'use strict';
const path = require('path');
const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  output: {
    path: path.resolve('./public_dev/'),
    filename: '[name].[contenthash].js'
  },
  mode: 'development',
  devtool: 'inline-source-map',
  optimization: {
    usedExports: true,
    splitChunks: {
      chunks: 'all',
    }
  }
});