# Changelog

## 1.3.9

Changes:
- Updated node dependencies

## 1.3.8  

Added:  
- Added AsyncLoad, which implements loadable components  

Changed:  
- export Placeholder  

Issues:  
- AsyncLoad does not work when used as a module import but works as a 
local project function, maybe issue with path resolution and dynamic imports  

## 1.3.7  

Changes:
- added props.onLoad to flow-interface  
- added props.appendComponent to flow-interface  
- added renderInterface to exports  
- added new project template to /template  
- added Popup to exports  
- TextField keydown handler will ignore editing keys  

Fixes: 
- ReCaptchaV2::cleanup will not throw errors if nodes have already been removed  

## 1.3.6

Changes:  
- html tag has lang=en
- renamed manifest.json to app.webmanifest and added content type to .htaccess 
- merged in updated service worker
- style adjustment to table 
- improved wrapper exports  
- catch resize promise error when the component has unmounted  

## 1.3.5  

Changes:  
- TextField: escaping of HTML unsafe characters is switchable via props.htmlEscape  

## 1.3.4  

Changes:  
- Guides are no longer built, they are now referenced from the repo.  
- Allow additional props to be passed to the stack content.  
- Unmount classList updates sometimes throw errors as the ref nodes are removed 
before the useEffect hook is called, we now call them conditionally. 

## 1.3.3  

Changes:  
- Style adjustment to <table>, height set to auto.  

## 1.3.2  

`Settings` content now a package export.  

## 1.3.1  

Changes: 
- Switched several `dependencies` to `devDependencies`  
- Added additional devDependencies removed from modules.  
- Images removed from preload assets.  

## 1.3.0  

Added form and input components. These are available to be imported.  
- Added `Form`.  
- Added `TextField`.  
- Added `InputFilePicker`. 
- Added `PaymentCard`.  
- Added `Codeblock`.  
- Added `Checkbox`.  
- Added `RadioGroup`.  
- Added `Toggle`.  
- Added `ReCaptchaV2`.  
- Added `ConditionalWrapper`.  
- Added `SelectBox`  

Changes:  
- Using `react-icons` for icons.  
- Adjusted Stack Modal width to 100vh on mobile.  
- Changed Stack close button to "arrow_back" and tweaked styling.  
- Refactored `FlowInterface`. Changed to functional component and events system.  
- `Menu` is scrollable on all devices.  

Fixes:  
- `ErrorBoundary`: the `hasError` state will reset if children are `null`.  
- Fixed: Stack content height cut off at the bottom.  
- Fixed: Removed ability to click stack overlay to close stack.  
- Fixed: `beforeunload` warning only shows when stack content has changed.  
- Fixed: `Textfield`, Feedback icon vertical alignment.  

Demo site updates:  
- Added content: Form and Data Input.  
- Added `DemoForm`.  
- Added `Settings`.  
- Split default menu options and content from demo site, to minimise build size 
when not using default content.  
- Added project docs.  

## 1.2.0  
- Added new default interface style as `mono`.  

## 1.1.0  
- Added Service Worker functionality.  

## 1.0.0  
- Initial commit  