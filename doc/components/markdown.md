# Markdown component  

- Renders markdown content using `react-markdown`.  
- Can take raw markdown or fetch remote content.  
- Implements `ServiceWorkerHandler` from `flow-serviceworker`. This allows 
remote markdown content to be cached, then loaded and updated using the 
cache-then-network policy for faster rendering.  

---  
## Usage  

````javascript
import { Markdown } from 'flow-interface';
````

---  
## Props  

Prop | Type | Description  
-- | -- | --  
`markdown` | String | The raw markdown text.  
`children` | String | Same as `props.markdown`.  
`source` | String | URI for remote content. Must have `Content-Type=text`.  

> If multiple props are set, `props.markdown` is given precedence.  
