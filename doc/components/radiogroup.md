# RadioGroup component  

- A group of styled radio buttons.  
- One value can be selected at any one time.  
- Can be used with the `<Form>` component.  

---  
## Usage  

````javascript
import { RadioGroup } from 'flow-interface';
````

---  
## Required Props  

Prop | Type | Description  
-- | -- | --  
`name` | String | Field name, similar to the html `name` attribute.  
`options` | Array | Array of `OptionObjects`, to render.  

---  
## Optional Props  

Prop | Type | Description  
-- | -- | --  
`minWidth` | String | CSS recognisable minimum width for grid layout.  
`maxWidth` | String | CSS recognisable maximum width for grid layout.  
`defaultValue` | String | The default value to apply on load. Match this to `props.options`.  
`callback` | Function | Invoked when field value changes, applying the `DataObject`.  

---  
## Custom Objects  

**OptionObject**  

Describes the radio options and their associated value.  

Property | Type | Description  
-- | -- | --  
`label` | String | Descriptive label to display.   
`value` | String | Value of the option.  

**DataObject**  

Property | Type  
-- | --  
`name` | String  
`valid` | Bool  
`value` | String  
