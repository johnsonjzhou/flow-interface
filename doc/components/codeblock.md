# Codeblock component  

- Displays a block of pre-formatted text.  

---  
## Usage  

````javascript
import { Codeblock } from 'flow-interface';
````

---  
## Required Props  

Prop | Type | Description  
-- | -- | --  
`data` | String | Pre-formatted text to display.  

---  
## Optional Props  

Prop | Type | Description  
-- | -- | --  
`language` | String | Indicating the code language.  

---  
## `props.language`  

Defaults to `Code Snippet` if not specified. 

Accepted values are:
- bash  
- shell  
- linux  
- code  
- data  
- javascript  
- php  
- apache  
- javascript  
- mysql  
- nosql  
- json  
