# InputFilePicker component  

- A styled element for file picker using input type=file.  
- Can be used with the `<Form>` component.  

---  
## Usage  

````javascript
import { InputFilePicker } from 'flow-interface';
````

---  
## Required Props  

Prop | Type | Description  
-- | -- | --  
`name` | String | Field name, similar to the html `name` attribute.  

---  
## Optional Props  

Prop | Type | Description  
-- | -- | --  
`accept` | String | Comma separated mime types, with wildcard, eg: `image/*,application/pdf`.  
`maxSize` | Int | Maximum size in MB, match this to what the server will accept.  
`maxQty` | Int | Maximum number of files that can be added.  
`capture` | String | What source to use for media capture, default is `environment`.  
`multiple` | Bool | Allow more than one file.  
`placeholder` | String | A custom placeholder message.  
`callback` | Function | Invoked when field loses focus, applying the `DataObject`.  

---  
## Custom Objects  

**DataObject**  

Property | Type  
-- | --  
`name` | String  
`valid` | Bool  
`value` | Array  

> The `value` property will be presented as an `array` of `File` interfaces.  
