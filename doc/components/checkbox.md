# Checkbox component  

- A styled checkbox.  
- Can be used with the `<Form>` component.  

---  
## Usage  

````javascript
import { Checkbox } from 'flow-interface';
````

---  
## Required Props  

Prop | Type | Description  
-- | -- | --  
`name` | String | Field name, similar to the html `name` attribute.  
`label` | String | Label description.  

---  
## Optional Props  

Prop | Type | Description  
-- | -- | --  
`intermediate` | Bool | Whether checkbox begins in an intermediate state.  
`defaultValue` | Bool | The default state for checkbox box, this takes precedence over `props.intermediate`.  
`callback` | Function | Invoked when field state changes, applying the `DataObject`.  
`onChange` | Function | Same as `props.callback`.  

> All other props are passed directly to the DOM.  

---  
## Custom Objects  

**DataObject**  

Property | Type  
-- | --  
`name` | String  
`valid` | Bool  
`value` | String  
