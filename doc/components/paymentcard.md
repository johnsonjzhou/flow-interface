# PaymentCard component  

- A credit card payment component that looks like a real credit card.  
- Uses [`react-payment-inputs`](https://github.com/medipass/react-payment-inputs).  
- Can be used with the `<Form>` component.  

---  
## Usage  

````javascript
import { PaymentCard } from 'flow-interface';
````

---  
## Required Props  

Prop | Type | Description  
-- | -- | --  
`name` | String | Field name, similar to the html `name` attribute.  

---  
## Optional Props  

Prop | Type | Description  
-- | -- | --  
`acceptOnly` | Array | Array of which cards to accept.  
`center` | Bool | Vertically center the component.  
`callback` | Function | Invoked when field loses focus, applying the `DataObject`.  

---  
## `props.acceptOnly`  

Card validation is conducted using `react-payment-inputs`. 
See [`cardTypes.js`](https://github.com/medipass/react-payment-inputs/blob/master/src/utils/cardTypes.js).  

Common accepted values:  
- `visa`  
- `mastercard`  
- `amex`  
- `dinersclub`  
- `jcb`  
- `unionpay`  
- `maestro`  
- `elo`  
- `hipercard`  

---  
## Custom Objects  

**DataObject**  

Property | Type  
-- | --  
`name` | String  
`valid` | Bool  
`value` | ValueObject  


> **Note:**  
> Value property will be empty if internal values do not pass validation.  


**ValueObject**  

Property | Type | Description  
-- | -- | --  
`name` | String | Name of the card holder.  
`number` | String | Card number.  
`expMM` | String | Expiry month in MM format.  
`expYYYY` | String | Expiry year in YYYY format.  
`cvc` | String | Card check code.  
