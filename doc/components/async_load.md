# AsyncLoad component  

Code splitting and lazy loading with `loadable-components`, 
implemented with dynamic import and and fallback.  

Uses `<Placeholder/>` for the fallback.  

---  
## Usage  

````javascript
import { AsyncLoad } from 'flow-interface';
````

> **Known issue**  
> Does not work as a module import but works as a local project module. 
> May be an issue with path resolution and dynamic imports in webpack.  
> 
> Work around is to copy the source and usually locally within the project.  
> View [source](https://bitbucket.org/johnsonjzhou/flow-interface/src/master/src/components/async_load.jsx)  

---  
## Required Props  

Prop | Type | Description  
-- | -- | --  
`path` | String | Path to the component to lazy load.  
