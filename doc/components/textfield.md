# TextField component  

- Renders a text field using either `<input>` or `<textarea`> tags based on 
preference (via the `props.multiline` flag).  
- Handles validation automatically based on `props.type` or use custom 
rules using `props.validation`.  
- Invokes `props.callback` when the text field loses focus.  
- Can be used with the `<Form>` component.  

---  
## Usage  

````javascript
import { TextField } from 'flow-interface';
````

---  
## Required Props  

Prop | Type | Description  
-- | -- | --  
`name` | String | Field name, similar to the html `name` attribute.  
`type` | String | Field type, similar to the html `type` attribute.  
`label` | String | Label description.  

---  
## Optional Props  

Prop | Type | Description  
-- | -- | --  
`multiline` | Bool | True: input=text. False: input=textarea.  
`defaultValue` | Mixed | The default value to apply on load.  
`required` | Bool | Whether this field can be left blank.  
`rejected` | Bool or String | False: normal behaviour, String: Rejection reason.  
`validation` | ValidationObject | Overrides default type based validation.  
`small` | Bool | For narrow fields, do not render the feedback text.  
`disabled` | Bool | Disables the field and show a disabled indicator.  
`maxlength` | Int | Limits the maximum length of the input.  
`htmlEscape` | Bool | Escape HTML unsafe characters in value. Default true.  
`callback` | Function | Invoked when field loses focus, applying the `DataObject`.  

> All other props are passed directly to the DOM.  

---  
## Custom Objects  

**ValidationObject**  

Property | Type  
-- | --  
`pattern` | RegEx  
`message` | String  

**DataObject**  

Property | Type  
-- | --  
`name` | String  
`valid` | Bool  
`value` | String  
