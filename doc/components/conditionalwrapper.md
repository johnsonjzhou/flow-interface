# ConditionalWrapper component  

- Wrap a component with another component when a given condition is met.  
- Credit: [blog.hackages.io](https://blog.hackages.io/conditionally-wrap-an-element-in-react-a8b9a47fab2).  

---  
## Usage and Example

````javascript
import { ConditionalWrapper } from 'flow-interface';

import MyComponent from './mycomponent';
import Wrapper from './wrapper';

const condition = (2 === 2);

return (
  <ConditionalWrapper condition={condition} wrapper={Wrapper} >
    <MyComponent/>
  </ConditionalWrapper>
);
````
