# ReCaptchaV2 component  

- Renders and manages Google reCaptcha V2.  
- Fetches Google assets on load and completely removes all scripts and nodes on dismount.  
- Fallback error state if Google assets do not load, eg. network connectivity.  
- Can be used with the `<Form>` component.  

---  
## Usage  

````javascript
import { ReCaptchaV2 } from 'flow-interface';
````

---  
## Required Props  

Prop | Type | Description  
-- | -- | --  
`apiKey` | String | The apikey for Google ReCaptcha V2.  

---  
## Optional Props  

Prop | Type | Description  
-- | -- | --  
`name` | String | Required if using with `<Form>`.  
`required` | Bool | Sets default DataObject as valid=false, useful with `<Form>`.  
`theme` | String | `light` (default) or `dark`.  
`size` | String | `normal` (default) or `compact`.  
`tabIndex` | Int | The tabindex attribute.  
`callback` | Function | Invoked when state changes, applying the `DataObject`.  

---  
## Custom Objects  

**DataObject**  

Property | Type  
-- | --  
`name` | String  
`valid` | Bool  
`value` | String  
