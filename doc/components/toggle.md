# Toggle component  

- A styled toggle switch.  
- Can be used with the `<Form>` component.  

---  
## Usage  

````javascript
import { Toggle } from 'flow-interface';
````

---  
## Required Props  

Prop | Type | Description  
-- | -- | --  
`name` | String | Field name, similar to the html `name` attribute.  
`label` | String | Label description.  

---  
## Optional Props  

Prop | Type | Description  
-- | -- | --  
`defaultValue` | Bool | The default state for toggle.  
`callback` | Function | Invoked when field state changes, applying the `DataObject`.  

---  
## Custom Objects  

**DataObject**  

Property | Type  
-- | --  
`name` | String  
`valid` | Bool  
`value` | String  
