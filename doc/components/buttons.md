# Button components 

Several pre-styled buttons are provided to keep a consistent interface experience:  
- FilledButton (*primitave*)  
- OpenButton  (*primitave*)    
- RoundButton  
- QuickActionButton  
- StackControlButton  

![Button style examples](https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/flow-interface-styled-buttons.gif)  
*Button style examples*  

All button components are styled using `styled-components`. 
**FilledButton** and **OpenButton** are style primatives to which other 
button styles are based.  

Type | Style sheet  
-- | --  
Primitave styles | [`src/styles/button.jsx`](https://bitbucket.org/johnsonjzhou/flow-interface/src/master/src/styles/button.jsx)  
Extended styles | [`src/components/buttons.jsx`](https://bitbucket.org/johnsonjzhou/flow-interface/src/master/src/components/buttons.jsx)  

---  
## Style modifications  

To create a custom button style, simply extend an existing style using the 
`styled` export of `styled-components` with overriding styles.  

````javascript
import styled from 'styled-comonents';
import { OpenButton } from 'flow-interface';

const NewButton = styled(OpenButton)`
  // new styles go here
`;
````

---  
## Usage  

````javascript
import {
  FilledButton, 
  OpenButton, 
  RoundButton. 
  QuickActionButton, 
  StackControlButton, 
} from 'flow-interface';

import { 
  RiExternalLinkLine, 
  RiAddLine, 
  RiArrowLeftLine
} from 'react-icons/ri';

<FilledButton onClick={() => { }}>Filled Button</FilledButton> 

// other style examples
<OpenButton>Open Button</OpenButton> 
<RoundButton><RiAddLine/></RoundButton> 
<QuickActionButton><RiExternalLinkLine/></QuickActionButton> 
<StackControlButton><RiArrowLeftLine/></StackControlButton> 
````
