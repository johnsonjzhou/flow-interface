# Form component  

- Works like a `<form>` but not using the `<form>` tag.  
- Handles and maintains child field data.  
- Identifies and keeps track of all child input fields as identified by the `name` prop.  
- Also works like a general wrapper component, so general components can also be used as children.  

---  
## Usage  

````javascript
import { Form } from 'flow-interface';

<Form>

  <TextField name='textfield'/>
  <SelectBox name='selectbox'/>

</Form>
````

---  
## Required Props  

There are no required props.  

---  
## Optional Props  

Prop | Type | Description  
-- | -- | --  
`callback` | Function | Invoked on first load and each time data changes in one of the fields. Applies the `FormDataObject` as the single argument.  

---  
## Child Props  

The following props need to be provided to the child field components for data maintenance.  

Prop | Type | Description  
-- | -- | --  
`name` | String | Unique identifier name for the field. Must contain this to initialise the field into the data array.  
`defaultValue` | Mixed | The initial value to which field data will be compared for changes.  


---  
## Custom Objects  

**FormDataObject**  

Property | Type | Description  
-- | -- | --  
`valid` | Bool | Whether all fields in the `data` array are valid.  
`changed` | Bool | Whether any field has deviated from it's `defaultValue`.  
`data` | Array | An array of `FieldDataObjects`, tracking all monitored fields.  

**FieldDataObject**  

Property | Type | Description  
-- | -- | --  
`name` | String | Unique identifier name for the field.  
`valid` | Bool | Whether the data is valid.  
`value` | Mixed | Current value of the field.  

---  
## Example of `FormDataObject`  

````json
{
  valid, 
  changed, 
  data: [ 
    { name, valid, value }, 
    ...
  ]
}
````