# Content Guide for Flow Interface

Working example: [`coming_soon.jsx`](https://bitbucket.org/johnsonjzhou/flow-interface/src/master/src/content/coming_soon.jsx).

---
## Content should be a React element  

Any valid React element (as evaluated by `React.isValidElement`) 
can be used as content.  

---
## Stack

A **stack** is an interactive container for content components rendered using 
the `Stack` component. Flow Interface handles multiple stacks via the 
`StackHandler` component. Only the upper most stack will be available for 
user interaction.  

![Multiple stacks](https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/flow-interface-stack-multiple.png)  
*Three stacks open*  

Each Stack has a designated area for **Content** as well as **Quick Actions**.  

![Stack areas](https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/flow-interface-stack.png)  

---
## Props  

All content components will be passed the following props via `StackHandler`:
- [openStack](##props.openstack)  
- [setQuickActions](##props.setquickactions)  
- [setChanged](##props.setchanged)  
- [setLoading](##props.setloading)  
- [closeStack](##props.closeStack)  

---
## Props.openStack `function`  

Opens a new stack with the specified content component.  

### Syntax  

````javascript
import MyComponent from './mycomponent';

props.openStack(<MyComponent/>, {...myComponentProps});
````

### Parameter(s)  

***component*** `object`  
Any valid React element.  

***props*** `object`  
Props to apply to the component.  

### Event  

This method fires `flow-openstack` custom event on `window`.  

---
## Props.setQuickActions `function`  

Sets Quick Actions available in the Stack.  

### Syntax  

````javascript

import { RiExternalLinkLine as IconOpenNew } from 'react-icons/ri';
import { IoMdHourglass as IconLoading } from 'react-icons/io';
import { FiEdit3 as IconChanged } from 'react-icons/fi';

props.setQuickActions([
  {icon: <IconOpenNew/>, action: () => {  }},
  {icon: <IconLoading/>, action: () => {  }}, 
  {icon: <IconChanged/>, action: () => {  }}
]);
````

### Parameter(s)  

***quickActions*** `array`  

An array of **QuickAction** `objects`. Each QuickAction object takes two 
properties.  

Property | Type | Description  
-- | -- | --  
`icon` | `React.Component` | An icon as imported from [`react-icons`](https://react-icons.github.io/react-icons/).  
`action` | `function` | The function that will be called the action is invoked.  

---
## Props.setChanged `function`  

Sets the Stack `changed` status. If this is *true*, then a warning message 
will be shown when the Stack is closed. This is used in situations such as 
form entry where the user will be prompted when closing a Stack that has 
unsubmitted content.  

![Changed prompt](https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/flow-interface-stack-changed.png)  

### Syntax  

````javascript
props.setChanged(bool);
````

### Parameter(s)  

***bool*** `bool`  
If this is *true*, then a warning message will be shown when the Stack is closed.  

---
## Props.setLoading `function`  

Activate or deactivate a loading animation. When the animation is active, 
the user will not be able to interact with the Content. This is useful 
for providing visual feedback for asynchronous functions.  

![Stack loading](https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/flow-interface-stack-loading.gif)  

### Syntax  

````javascript
props.setLoading(bool)  
````

### Parameter(s)  

***bool*** `bool`  
If this is *true*, loading animation will be activated.  

---  
## Props.closeStack `function`  

- Closes the current stack.  
- Check whether content has been flagged as 'changed', if so open a Popup.  
- Reset quickActions, loading style and changed status.  
- For exit transition to work properly, we must hold the modal height.  

### Syntax  

````javascript
props.closeStack()  
````