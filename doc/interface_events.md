# Flow Interface Events Guide  

`Flow Interface` core functions are events driven, allowing easy access 
to these functions from within and outside of the component tree. 
Unless specified otherwise, all events are mounted on `window`.  

---  
## Dispatching events 
````javascript
const event = new CustomEvent(eventName[, { detail }]);
window.dispatchEvent(event);
````

---
## Stack events 

### `flow-openstack`  

Opens a new stack with supplied content and props.  

````javascript
new CustomEvent('flow-openstack', { detail: { content, props } });
````

***content*** `React.Component`  
A React Component to render in the new Stack.  

***props*** `object`  
An object containing props that will be passed to the `content` component.  

### `flow-closestack`  

Closes a stack with supplied stack index.  

````javascript
new CustomEvent('flow-closestack', { detail: { index } });
````

***index*** `int`  
The Stack index number to close. This will close the Stack with the index 
number *and* all Stacks after it.  

---  
## UI events  

### `flow-set-color-scheme`  

Sets the color scheme.  

````javascript
new CustomEvent('flow-set-color-scheme', { detail: { scheme }});
````

***scheme*** `string`  
Takes value of `auto`, `light`, or `dark`.  

### `flow-get-current-color-scheme`  

Retrieves the currently active color scheme and applies it to the callback.  

````javascript
new CustomEvent('flow-get-current-color-scheme', { detail: { callback }});
````

***callback*** `function`  
Receives one parameter being the current `scheme`. This is usually either 
`light` or `dark`.  

### `flow-get-preferred-color-scheme`  

Retrieves the preferred color scheme and applies it to the callback.  

````javascript
new CustomEvent('flow-get-preferred-color-scheme', { detail: { callback }});
````

***callback*** `function`  
Receives one parameter being the preferred `scheme`. This is usually either 
`auto`, `light` or `dark`.  

### `flow-set-interface-style`  

Sets the interface style and store for future reference in localStorage.  

````javascript
new CustomEvent('flow-set-interface-style', { detail: { style } });
````

***style*** `string`  
The style value to set. Accepted values are `mono`, `stacks`, 
`stacks-performance`, or `stacks-perspective`.  

### `flow-get-interface-style`  

Retrieves the stored interface style value and applies it to the callback.  

````javascript
new CustomEvent('flow-get-interface-style', { detail: { callback }});
````

***callback*** `function`  
Receives one parameter being the current `style`. This is usually either 
`mono`, `stacks`, `stacks-performance`, or `stacks-perspective`.  

### `flow-set-background`  

Sets, or removes, a custom background. Typically is used when a new menu group 
is selected.  

````javascript
new CustomEvent('flow-set-background', { detail: { background } });
````

***background*** `string`  
Any value accepted by the css `background` property.  

### `flow-trigger-update`  

Tells the service worker handler that we want to update the app immediately by 'skipWaiting'.  

````javascript
new CustomEvent('flow-trigger-update');
````

### `flow-get-network-state`  

Retrieves the stored network value and applies it to the callback.  

````javascript 
new CustomEvent('flow-get-network-state', { detail: { callback }});
````

***callback*** `function`  
Receives one parameter being an object with properties `online` (bool) and 
the timestamp `updated` (string).
