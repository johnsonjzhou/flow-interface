# Issues and ToDo

## Service Worker and IndexedDB
- Use IndexedDB to handle POST requests.  

## Settings page
- Set color scheme  
- Set interface style

## Icons
- materialFilled  
- materialRegular  
- materialRounded
- materialSharp
- materialTwoTone