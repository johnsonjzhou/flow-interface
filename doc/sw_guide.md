# Content Guide for Flow Interface

Working example: [coming_soon.jsx](https://bitbucket.org/johnsonjzhou/flow-interface/src/master/src/content/coming_soon.jsx)  

---  
## Uses `flow-serviceworker`  

Flow Interface is designed around 
[`flow-serviceworker`](https://bitbucket.org/johnsonjzhou/flow-serviceworker/). 
Using your custom Service Worker script may yield unpredictable results. 
If you wish to do this, take note of the methods contained within the 
`FlowInterface` component.  

---  
## Network availability  

`flow-serviceworker` will notify when network connectivity is poor or offline. 
If the network state is *offline*, the interface will apply a greyscale 
filter to indicate that it is in offline operations.  

![Offline mode](https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/flow-interface-sw-offline.png)
*Offline mode is represented with a greyscale filter across the 
entire interface*  

---  
## App updates  

`ServiceWorkerHandler` will automatically check for changes to the service 
worker script based on the following schedule, based on build environment.  

Mode | Update check timer  
-- | --  
Development | 10 seconds  
Production | 60 minutes  

When an update is detected, the interface will be notified and a prompt 
displayed.  

![App update available](https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/flow-interface-sw-update.png)  
*App update available prompt*  
