/**
 * The Flow Interface
 * 
 * A minimalist user interface that promotes focus whilst maintaining
 * fluid workflows
 * 
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @export  FlowInterface
 * 
 * @param  {TaggedTemplateLiteral}  props.theme - theme declarations
 * @param  {array}  props.menu - menu choices
 * @param  {object}  props.logo - logo to display, {name, src}
 * @param  {object}  props.contents - a map of contents
 * @param  {object}  props.serviceworker - object encompassing parameters for 
 *   ServiceWorkerContainer.register  
 * @param  {function}  props.onLoad  
 *  - a function invoked in the useEffect phase 
 * @param  {React.Component}  props.appendComponent  
 *  - append a component as the last child of the interface stack  
 * @param  {string}  props.serviceworker.scriptURL  
 * @param  {object}  props.serviceworker.options  
 */

import React, { memo, useRef, useEffect, isValidElement } from 'react';

import ErrorBoundary from './components/errorboundary';
import Menu from './components/menu';
import StackHandler from './components/stack_handler';
import Popup from './components/pop_up';

import { Console } from 'flow-utils';
import Server from 'flow-fetch';

import ServiceWorkerHandler from 'flow-serviceworker';

import { GlobalStyles } from './styles/global_styles';
import { ui } from './styles/interface';
import { defaultMenuOptions } from './content/default_menu';
import { defaultContentMap } from './content/default_content_map';
import flowInterfaceLogo from './img/flow_interface_logo.svg';

import { useColorScheme, setColorScheme, watchSystemScheme, 
  getPreferredColorScheme } from 'flow-utils';
import { Perspective3D } from 'flow-utils';

const CustomEventInit = { detail: {} };

const FlowInterface = (props) => {
  const {
    theme, 
    menu = defaultMenuOptions, 
    logo = {name: 'Flow interface logo', src: flowInterfaceLogo},
    contents = defaultContentMap,
    onLoad, 
    appendComponent: AppendComponent, 
    serviceworker, 
  } = props;

  const menuProps = {
    menu, 
    logo, 
    contents 
  };

  const sw = useRef(new ServiceWorkerHandler());
  const network = useRef({ online: true, updated: Date.now() });
  const uiWrapper = useRef();
  const perspective3D = useRef();

  // initiate Console and Server modules into global namespace
  window.Console = new Console('>>>');
  window.Console.registerBugsnag();
  window.Server = new Server();

  /** SERVICE WORKER -------------------------------------------------------- */

  /**
   * Tells the service worker handler that we want to update the app
   * immediately by 'skipWaiting' 
   * @event  window#flow-trigger-update
   */
  const triggerUpdate = () => {
    window.Console && 
    window.Console.log('Triggering new version install', 'App Update');
    sw.current.isRegistered() && 
    sw.current.skipWaiting();
  };

  /**
   * Act as a callback from the service worker to indicate new version,
   * sets state.update to true
   * binds update handler to window.update()
   * opens a stack popup with option to update
   */
  const handleAppUpdateNotification = () => {
    window.Console && 
    window.Console.log('Update ready, use window.update()', 'App Update');

    // bind to window.update
    window.update = triggerUpdate;

    // open a popup
    window.dispatchEvent(new CustomEvent('flow-openstack', { detail: {
      content: (
        <Popup
          proceed={'Update now'}
          cancel={'Later'}
          onProceed={triggerUpdate} 
        >
          {`# An update is available. \n`+
          `We recommend that you update now. `+
          `You can update at a later time by refreshing the page.`}
        </Popup>
      ),
      props: { hideControls: true }
    }}));
  };

  /**
   * Read the online status when notified by the service worker
   * @param  {object}  data
   * @param  {bool}  data.online - whether we are online or offline
   */
  const handleNetworkStateChange = ({ online, updated }) => {
    if (online === undefined) return;

    // track in Ref
    network.current = { online, updated };

    // apply styling
    uiWrapper.current.setAttribute('data-online', online && 'true' || 'false');
    window.Console && window.Console.log(online, 'Network online');
  };

  /**
   * Registers the serviceworker
   */
  const registerServiceWorker = () => {
    // skip if no serviceworker defined in props
    if (!serviceworker) return;
    
    try {
      sw.current.setNewVersionCallback(handleAppUpdateNotification);
      sw.current.setMessageCallback(handleNetworkStateChange);
      sw.current.register(serviceworker);
    } catch (error) {
      window.Console && window.Console.handleError(error);
    }
  };

  /** COLOR SCHEME ---------------------------------------------------------- */

  /**
   * Applies color scheme styling 
   * @param  {string}  scheme -- light | dark
   */
  const applyColorScheme = (scheme = 'light') => {
    switch (scheme) {
      case 'light': 
      return uiWrapper.current.classList.remove('dark-theme');
      case 'dark':
      return uiWrapper.current.classList.add('dark-theme');
    }
  };

  /**
   * Sets the color scheme 
   * @event  window#flow-set-color-scheme 
   * @param  {CustomEvent.detail}  detail
   * @param  {string}  detail.scheme
   */
  const setColorSchemeEvent = ({ detail: { scheme = 'light' }} = CustomEventInit) => {
    setColorScheme(scheme, applyColorScheme);
  };

  /**
   * Retrieves the current color scheme and applies it to the callback 
   * @event  window#flow-get-current-color-scheme 
   * @param  {CustomEvent.detail}  detail
   * @param  {function}  detail.callback -- receives parameter 'scheme' 
   * 
   * @return  {string|null}
   */
  const getColorScheme = ({ detail: { callback }} = CustomEventInit) => {
    const scheme = useColorScheme();
    callback && callback(scheme);
    return scheme;
  };

  /**
   * Retrieves the preferred color scheme and applies it to the callback 
   * @event  window#flow-get-preferred-color-scheme 
   * @param  {CustomEvent.detail}  detail
   * @param  {function}  detail.callback -- receives parameter 'scheme' 
   * 
   * @return  {string}
   */
  const getPrefColorScheme = ({ detail: { callback }} = CustomEventInit) => {
    const scheme = getPreferredColorScheme() || 'auto';
    callback && callback(scheme);
    return scheme;
  };

  /** PERSPECTIVE 3D (ONLY IN STACKS STYLE) --------------------------------- */

  /**
   * Applies a perspective-origin inline style 
   * @event  window#perspective3d-change 
   * @param  {CustomEvent.detail}  detail 
   *  - any value that can be applied to the css property 'perspective-origin'
   */
  const applyPerspectiveOrigin = ({ detail } = CustomEventInit) => {
    // window.Console.log(`${x}% ${y}%`, 'Perspective');
    window.requestAnimationFrame(() => {
      detail && 
      uiWrapper.current.style.setProperty('perspective-origin', detail);
    });
  };

  /**
   * Whether to watch for perspective changes 
   * @param  {bool}  watch
   */
  const watchPerspectiveChange = (watch = true) => {
    switch (watch) {
      case true:
        // assign Perspective3D class 
        (!perspective3D.current) && 
        (perspective3D.current = new Perspective3D());

        // add event listener
        window.addEventListener('perspective3d-change', applyPerspectiveOrigin);
      break;

      case false: 
        // release Perspective3D class
        perspective3D.current && 
        (perspective3D.current.release()) && 
        (perspective3D.current = null);

        // remove event listener
        window.removeEventListener('perspective3d-change', applyPerspectiveOrigin);
      break;
    };
  };

  /** INTERFACE STYLES ------------------------------------------------------ */

  /**
   * Applies the interface style 
   * @param  {string}  style 
   */
  const applyInterfaceStyle = (newStyle) => {
    // check localStorage for preferences
    const userPref = getInterfaceStyle();

    let style = newStyle || userPref || 'mono';
    
    // handle style specifics
    switch (style) {
      case 'mono':
      case 'stacks':
      case 'stacks-performance':
        watchPerspectiveChange(false);
      break;
      case 'stacks-perspective':
        watchPerspectiveChange(true);
      break;
      default:
        // unsupported style, reset this to 'mono'
        style = 'mono';
        watchPerspectiveChange(false);
      break;
    }

    // apply style change
    uiWrapper.current.setAttribute('data-interface-style', style);
  };

  /**
   * Retrieves the stored interface style value and applies it to the callback 
   * @event  window#flow-get-interface-style
   * @param  {CustomEvent.detail}  detail
   * @param  {function}  detail.callback -- receives parameter 'style' 
   * 
   * @return  {string|null}
   */
  const getInterfaceStyle = ({ detail: { callback } } = CustomEventInit) => {
    const style = window.localStorage && 
      window.localStorage.getItem('flow-interface-style');
    callback && callback(style);
    return style;
  };

  /**
   * Sets the interface style and store for future reference in localStorage 
   * @event  window#flow-set-interface-style
   * @param  {CustomEvent.detail}  detail
   * @param  {string}  detail.style 
   */
  const setInterfaceStyle = ({ detail: { style } } = CustomEventInit) => {
    // save preference in localStorage
    window.localStorage && 
    window.localStorage.setItem('flow-interface-style', style);

    // apply the style 
    applyInterfaceStyle(style);
  }; 

  /**
   * Sets, or removes, a custom background 
   * @event  window#flow-set-background  
   * @param  {CustomEvent.detail}  detail
   * @param  {string}  detail.background 
   * - any value accepted by the css 'background' property 
   */
  const setBackground = ({ detail: { background = false }} = CustomEventInit) => {
    switch (background) {
      case false:
        uiWrapper.current.classList.remove('show-group');
        // wait 500ms to allow transition to complete
        // this is more reliable visually than ontransitionend event
        window.setTimeout(() => {
          uiWrapper.current.style.removeProperty('--menu-group-background');
        }, 500);
      break;
      default:
        uiWrapper.current.classList.add('show-group');
        uiWrapper.current.style.setProperty('--menu-group-background', background);
      break;
    }
  };

  /** NETWORK STATE --------------------------------------------------------- */

  /**
   * Also handleNetworkStateChange in Service Worker
   */

  /**
   * Retrieves the stored network value and applies it to the callback 
   * @event  window#flow-get-network-state
   * @param  {CustomEvent.detail}  detail
   * @param  {function}  detail.callback -- receives parameter { online, updated } 
   */
  const getNetworkState = ({ detail: { callback } } = CustomEventInit) => {
    callback && callback(network.current);
  };

  /** HOOKS & RENDER -------------------------------------------------------- */

  useEffect(() => {
    // register the service worker 
    registerServiceWorker();

    // apply the current scheme then watch for changes 
    applyColorScheme(useColorScheme());
    watchSystemScheme(applyColorScheme);

    // applies the interface style 
    applyInterfaceStyle();

    // event handlers 
    window.addEventListener('flow-set-color-scheme', setColorSchemeEvent);
    window.addEventListener('flow-get-current-color-scheme', getColorScheme);
    window.addEventListener('flow-get-preferred-color-scheme', getPrefColorScheme);
    window.addEventListener('flow-set-interface-style', setInterfaceStyle);
    window.addEventListener('flow-get-interface-style', getInterfaceStyle);
    window.addEventListener('flow-set-background', setBackground);
    window.addEventListener('flow-trigger-update', triggerUpdate);
    window.addEventListener('flow-get-network-state', getNetworkState);

    // onLoad callback  
    onLoad && onLoad();

    return () => {
      window.removeEventListener('flow-set-color-scheme', setColorSchemeEvent);
      window.removeEventListener('flow-get-color-scheme', getColorScheme);
      window.removeEventListener('flow-get-preferred-color-scheme', getPrefColorScheme);
      window.removeEventListener('flow-set-interface-style', setInterfaceStyle);
      window.removeEventListener('flow-get-interface-style', getInterfaceStyle);
      window.removeEventListener('flow-set-background', setBackground);
      window.removeEventListener('flow-trigger-update', triggerUpdate);
      window.removeEventListener('flow-get-network-state', getNetworkState);
    }
  }, []);

  return (
    <>
      <GlobalStyles theme={theme}/>
      <ui.wrapper
        id='flow-interface' 
        ref={uiWrapper} 
        data-online='true'
      >
        <ErrorBoundary message="Darn, something went awfully wrong!">
          <Menu {...menuProps} />
          <StackHandler/>
          { 
            AppendComponent && isValidElement(AppendComponent) &&
            <AppendComponent/>
          }
        </ErrorBoundary>
      </ui.wrapper>
    </>
  );
};

export default memo(FlowInterface);