import React from 'react';
import ReactDOM from 'react-dom';

import FlowInterface from './flow_interface';
import ErrorBoundary from './components/errorboundary';
import { demoMenuOptions } from './content/demo_menu';
import { demoContentMap } from './content/demo_content_map';

/**
 * Show identifier in the console
 */
window.console.log(
  '--------------------------------------------------- \n'+
  'Flow Interface Demonstration \n'+
  `Version ${___WEBPACK_VERSION___} ${process.env.NODE_ENV} \n`+
  'Webmaster: Johnson Zhou (johnson@simplyuseful.io) \n'+
  '---------------------------------------------------'
);

/**
 * Initiate FlowInterface on <main>
 */
ReactDOM.render(
  <ErrorBoundary message='Flow Interface failed to load'>
    <FlowInterface
      serviceworker={{
        scriptURL: '/sw.js',
        options: { scope: '/' }
      }}
      menu={demoMenuOptions} 
      contents={demoContentMap}
    />
  </ErrorBoundary>
, document.getElementsByTagName("main")[0]);