/**
 * A component that renders and manages Google reCAPTCHA v2.
 * @author  Johnson Zhou <johnson@simplyuseful.io> 
 * 
 * @export  ReCaptchaV2 as default 
 * 
 * @param  {string}  [props.name] - required if using with <Form/>
 * @param  {bool}  [props.required] - required if using with <Form/> to default valid=false
 * @param  {string}  props.apiKey - the apikey for Google Recaptcha V2 
 * @param  {function}  props.callback
 *  - invoked when value changes from the widget, applies { name, valid, value } 
 * @param  {string}  [props.theme='light'] - light | dark 
 * @param  {string}  [props.size='normal'] - normal | compact 
 * @param  {int}  [props.tabIndex]
 * 
 * @note  loading and cleanup behaviours 
 * the recaptcha widget is quite 'messy' as it injects additional scripts and 
 * div nodes to the body during operation. 
 * the widget ID seems to be tracked remotely by google. 
 * this way, each time this component is loaded, we will first perform clean up
 * which removes all of the injected scripts and nodes, then reloads the widget
 * from scratch 
 * we are assuming that for any given time, the user will only need to be 
 * submitting one form that requires recaptcha
 * we also have extensive fallback with error state should the widget not load 
 * due to network connectivity 
 */

import React, { memo, useEffect, useRef, useState } from 'react';
import Placeholder from './placeholder';
import { v2 } from '../styles/recaptcha';
import { useColorScheme } from 'flow-utils';

const { Wrapper } = v2;

const ReCaptchaV2 = (props) => {
  const { 
    name='recaptcha', 
    apiKey, 
    callback, 
    theme, 
    size = 'normal', 
    tabIndex
  } = props; 

  const passThruProps = {
    theme: theme || useColorScheme(), 
    size, 
    tabIndex
  };

  // warn if apiKey not provided
  !apiKey && console.warn('ReCaptchaV2', 'props.apiKey not provided');

  const [ error, setError ] = useState(!apiKey);
  const widgetWrapper = useRef();
  const widgetNode = useRef();
  const widgetID = useRef();
  const observer = useRef();

  const cleanup = () => {
    try {
      // remove any existing script tags 
      for (const script of document.scripts) {
        script.src && script.src.includes('recaptcha') && script.remove();
      };

      // delete global variables 
      delete window.grecaptcha;
      delete window.flowRecaptchaCallback;

      // delete and reset widgetNode 
      widgetNode.current && widgetWrapper.current.removeChild(widgetNode.current);
      widgetNode.current = undefined;

      // delete breakout nodes in the body 
      for (const node of document.body.childNodes) {
        if (node.nodeName !== 'DIV') continue;

        // find within the node any elements with class 'g-recaptcha-bubble-arrow'
        const isRecaptcha = node.getElementsByClassName('g-recaptcha-bubble-arrow');
        (isRecaptcha.length > 0) && node.parentNode.removeChild(node);
      };

      // stop watching for DOM mutations
      observer.current && observer.current.disconnect();
    } catch (error) {
      // don't need to do anything  
      // most errors are due to node being removed already  
    }
  };

  const loadScript = () => {
    (new Promise((resolve) => {
      
      cleanup();

      // assign onload callback
      window.flowRecaptchaCallback = resolve; 

      // assign timeout 
      window.setTimeout(resolve, 2000);

      // create the widget node 
      const node = document.createElement('div');
      node.setAttribute('class', 'widget');
      widgetNode.current = widgetWrapper.current.appendChild(node);

      // create the script tag and append it to body
      // @see https://developers.google.com/recaptcha/docs/display
      const scriptTag = document.createElement('script'); 
      scriptTag.setAttribute(
        'src', 
        'https://www.google.com/recaptcha/api.js?onload=flowRecaptchaCallback&render=explicit'
      );
      scriptTag.setAttribute('async', true); 
      scriptTag.setAttribute('defer', true); 
      document.body.appendChild(scriptTag); 
    }))
    .then(() => {
      setError(!window.grecaptcha);
      window.grecaptcha && renderWidget();
    });
  };

  const onWidgetError = () => {
    setError(true);
  };

  const onWidgetUpdate = () => {
    const response = window.grecaptcha.getResponse(widgetID.current);
    // response is either a g-recaptcha-response token or blank string
    // @see  https://developers.google.com/recaptcha/docs/display#js_api

    const data = {
      name, 
      valid: !!response, 
      value: !!response
    };

    callback && callback(data);
  };

  const renderWidget = () => {
    // render the widget 
    // @see  https://developers.google.com/recaptcha/docs/display#render_param
    widgetID.current = window.grecaptcha.render(widgetNode.current, {
      sitekey: apiKey,
      callback: onWidgetUpdate, 
      'expired-callback': onWidgetUpdate, 
      'error-callback': onWidgetError, 
      ...passThruProps
    });

    // we need to catch a situation whereby the widget does not load 
    // and set the error state 
    // as the widget runs in an cross-origin iframe, we have very limited
    // options to detect this. however, the widget does respond with a fallback  
    // after a timeout period (40s) 
    // we will use a MutationObserver to detect when this message loads 
    // and set the error state 
    const onWidgetChange = () => {
      const iframe = widgetNode.current.getElementsByTagName('iframe')[0];
      iframe && iframe.src.includes('fallback') && onWidgetError();
    };
    observer.current = new MutationObserver(onWidgetChange);
    observer.current.observe(widgetNode.current, { childList: true });
  };

  useEffect(() => {
    !error && loadScript();

    return () => {
      cleanup();
    }
  }, [ error ]);

  return (
    <Wrapper ref={widgetWrapper} className={size === 'compact' && 'compact'}>
      <Placeholder
        width='304px' 
        height='78px' 
        loadingMsg='Loading Challenge' 
        errorMsg='Error' 
        error={error}
        onRetry={loadScript}
      />
    </Wrapper>
  )
};

export default memo(ReCaptchaV2);
