/**
 * A styled Checkbox 
 * @author Johnson Zhou <johnson@simplepharmacy.io>
 * 
 * @export  Checkbox 
 * 
 * @param  {string}  props.name 
 * - the 'name' attribute 
 * @param  {string}  props.label 
 * - the description for the label 
 * @param  {bool}  [props.indeterminate] 
 * - whether checkbox begins in an indeterminate state 
 * @param  {bool}  [props.defaultValue] 
 * - default state for the check box, this takes precedence over indeterminate 
 * @param  {function}  [props.onChange] 
 * - a callback that gets invoked with the 'data' object 
 * @param  {function}  [props.callback] 
 * - a callback that gets invoked with the 'data' object 
 * 
 * @note  
 * props.onChange and props.callback gets invoked in an identical way, 
 * the reason for inclusion is to provide an additional callback in case 
 * the Form component is used for automating data 
 * 
 * @example  data
 * { name, valid=true, value }
 * 
 * all other props are directly passed to <input>
 * 
 * todo: spacebar to toggle
*/

import React, { memo, useState, useRef, useEffect } from 'react';
import { selector } from '../styles/selection';

import {
  RiCheckboxBlankLine as IconUnchecked, 
  RiCheckboxFill as IconChecked, 
  RiCheckboxIndeterminateLine as IconIndeterminate
} from 'react-icons/ri';

const { Wrapper } = selector;

const Checkbox = (props) => {

  const {
    name = 'checkbox', 
    label = 'This is a checkbox', 
    indeterminate, 
    defaultValue, 
    onChange, 
    callback, 
    ... restProps
  } = props;

  const [ checked, setChecked ] = useState(
    defaultValue || 
    indeterminate && 'indeterminate' || 
    false
  ); 
  const inputRef = useRef();

  const onClick = (event) => {
    const data = {
      name, 
      valid: true, 
      value: event.target.checked
    };
    callback && callback(data);
    onChange && onChange(data);
    setChecked(event.target.checked);
  };

  useEffect(() => {
    inputRef.current.indeterminate = !!indeterminate; 
    inputRef.current.checked = !!defaultValue; 
  }, []);

  return (
    <Wrapper>
      <input 
        ref={inputRef}
        type='checkbox' 
        id={name} 
        onClick={onClick} 
        aria-label={label} 
        {...restProps}
      />
      <label
        htmlFor={name} 
        aria-hidden={true} 
      >
        <div className='icon'>
        {
          (checked === 'indeterminate') && <IconIndeterminate/> || 
          checked && <IconChecked/> || 
          <IconUnchecked/>
        }
        </div>
        <span>{label}</span>
      </label>
    </Wrapper>
  );
};

export default memo(Checkbox);
