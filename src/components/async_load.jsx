/**
 * Code splitting and lazy loading with loadable components, 
 * implemented with dynamic import and and fallback  
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @export  AsyncLoad as default 
 */
import React from 'react';
import loadable from '@loadable/component'
import Placeholder from './placeholder';

/**
 * @see  https://loadable-components.com/docs/dynamic-import/
 * 
 * @param  {string}  props.path - path to the component to be lazy loaded 
 */
const AsyncLoad = loadable(props => import(`${props.path}`), {
  cacheKey: props => props.path, 
  fallback: <Placeholder/> 
});

export {
  AsyncLoad as default 
};