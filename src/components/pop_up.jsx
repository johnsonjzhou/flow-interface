/**
 * A simple pop up alert with option to proceed or cancel,
 * to be used as content for a Stack
 * @author  Johnson Zhou  <johnson@simplyuseful.io>
 * 
 * @param  {string}  props.proceed - a label for the proceed button
 * @param  {function}  props.onProceed - callback for proceed option
 * @param  {string}  props.cancel - a label for the cancel button
 * @param  {function}  props.closeStack
 *  - callback for the cancel option
 *  - this is usually applied by the Stack
 * @param  {string}  props.children - a message in markdown format
 */

import React, { memo, useEffect } from 'react';
import Markdown from './markdown';
import { FilledButton, OpenButton } from './buttons';
import styled from 'styled-components';
import { wrapper } from '../styles/layouts';

const SameRow = styled(wrapper.inline)`
  margin-bottom: 0 !important
`;

const Popup = (props) => {

  const {
    proceed = 'Ok', 
    onProceed, 
    cancel = 'Cancel', 
    closeStack, 
    children
  } = props;
  
  // activate onProceed when Enter key is pressed
  const onKeyDown = (event) => {
    event && event.key && event.key === 'Enter' && handleProceed();
  };

  // close self, then activate the proceed callback
  const handleProceed = () => {
    const closeSelf = new Promise(resolve => {
      closeStack && closeStack();
      resolve();
    });
    closeSelf.then(() => {onProceed && onProceed()});
  };

  useEffect(() => {
    // on mount
    window.addEventListener('keydown', onKeyDown);
    return () => {
      // on unmount
      window.removeEventListener('keydown', onKeyDown);
    }
  });

  const proceedBtnProps = {
    'aria-label': proceed,
    onClick: handleProceed,
  };

  const cancelBtnProps = {
    'aria-label': cancel,
    onClick: closeStack,
  };

  return(
    <>
      <Markdown>{children || '# Popup. \nThis is a popup alert.'}</Markdown>
      <SameRow>
        <FilledButton {...proceedBtnProps}>{proceed}</FilledButton>
        <OpenButton {...cancelBtnProps}>{cancel}</OpenButton>
      </SameRow>
    </>
  );
};

export default memo(Popup);
