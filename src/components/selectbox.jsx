/**
 * A styled <select> box. 
 * This does not actually use the <select> element due to challenges with 
 * styling. 
 * @author  Johnson Zhou  <johnson@simplyuseful.io>  
 * 
 * @export SelectBox   as default
 * 
 * @param  {string}  props.name 
 * - the 'name' attribute 
 * @param  {array}  props.options 
 * - array of objects describing the radio options to render
 * @param  {string}  [props.label] 
 * - label of the field 
 * @param  {function}  [props.callback] 
 * - a callback that gets invoked with the 'data' object 
 * @param  {string}  [props.defaultValue] 
 * - the default value for this field, match this to value in options 
 * 
 * @example  props.options
 * [
 *   {label: 'Non grouped option 1', value: 'option_1'},
 *   {label: 'Non grouped option 2', value: 'option_2'},
 *   {group: {
 *     label: 'Fruits',
 *     options: [
 *       {label: 'Banana', value: 'banana'},
 *       {label: 'Apple', value: 'apple'},
 *       {label: 'Orange', value: 'orange'},
 *     ]
 *   }}
 * ]
 * 
 * @example  data
 * { name, valid=true, value }
 * 
 * todo:  multiple && size attributes 
 */
import React, { memo, useEffect, useState, useRef } from 'react';
import { selectbox } from '../styles/selection';

import { 
  HiSelector as SelectIcon
} from 'react-icons/hi';

const { 
  Wrapper, 
  Select, 
  Label, 
  Button, 
  OptionList, 
  DefaultOption, 
  Option, 
  OptGroup, 
} = selectbox;

const SelectBox = (props) => {
  const {
    name = 'selectbox', 
    label, 
    options = [], 
    defaultValue = '', 
    callback
  } = props;

  const [ selected, setSelected ] = useState(defaultValue);
  const wrapperRef = useRef();

  /** Click handling -------------------------------------------------------- */

  const onClickSelect = () => {
    wrapperRef.current.classList.toggle('open');
  };

  const onClickOption = (event) => {
    const value = event && event.target.getAttribute('value');
    (value !== undefined) && setSelected(value);
  };

  /** Rendering ------------------------------------------------------------- */

  const renderPlaceholder = () => {
    let placeholder = 'Select an option';

    const matchLabel = option => {
      const {
        value, 
        label 
      } = option;

      // match value 
      label && value && value === selected && (placeholder = label);
    };
    
    selected && 
    options.forEach(option => {
      const { 
        group
      } = option;

      group && group.options.forEach(option => matchLabel(option))
        ||
      matchLabel(option);
    });

    return <DefaultOption>{placeholder}</DefaultOption>
  };

  const renderOption = (option) => {
    const {
      label = 'Undefined', 
      value = 'undefined'
    } = option;

    const optionProps = {
      key: value, 
      value, 
      className: value === selected && 'selected', 
      onClick: onClickOption
    };

    return (
      <Option {...optionProps}>
        {label}
      </Option>
    )
  };

  const renderGroup = (group) => {
    const {
      label = 'Undefined', 
      options = []
    } = group;

    const groupProps = {
      key: label, 
      label, 
    };

    return (
      <OptGroup {...groupProps}>
        {options.map(option => renderOption(option))}
      </OptGroup>
    )
  };

  useEffect(() => {
    const data = {
      name, 
      valid: true, 
      value: selected
    };

    callback && callback(data);

  }, [ selected ]);

  const selectProps = { 
    name, 
    ref: wrapperRef, 
    onClick: onClickSelect
  };

  return (
    <Wrapper>
      { label && <Label>{label}</Label> }
      <Select {...selectProps} > 
        { renderPlaceholder() }
        <Button><SelectIcon/></Button>
        <OptionList>{
          options.map((option) => {
            const { 
              group 
            } = option;
          
            return group && renderGroup(group) || renderOption(option);
          }) 
        } </OptionList>
      </Select>
    </Wrapper>
  );
};

export default memo(SelectBox);
