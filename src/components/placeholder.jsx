/**
 * A Placeholder for remotely fetched content with error styling and 
 * retry capability. Will retry automatically in 5 seconds or manually via 
 * a retry button.
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @param  {bool}  props.error - whether loading is in error state
 * @param  {function}  props.onRetry - a callback function when trying to retry
 * @param  {string}  [props.width] - css compatible width 
 * @param  {string}  [props.height] - css compatible height 
 * @param  {string}  [props.loadingMsg] - a custom loading message 
 * @param  {string}  [props.errorMsg] - a custom error message 
 * 
 * @export  Placeholder
 */
import React, { memo, useState, useEffect } from 'react';
import { placeholder } from '../styles/placeholder';
import { wrapper as layout } from '../styles/layouts';
import { OpenButton } from '../components/buttons';

const Placeholder = (props) => {

  const { 
    error, 
    onRetry,  
    width, 
    height, 
    loadingMsg = 'Fetching Content',
    errorMsg = 'Could not get content '
  } = props;

  const { 
    wrapper: Wrapper,
    status: Status
  } = placeholder;

  const {
    inline: Inline
  } = layout;

  // retry counter
  const [ counter, setCounter ] = useState(5);

  // retry message
  const retryMsg = onRetry && counter > 0 && `\nRetrying in ${counter}`;

  // count down timer
  useEffect(() => {
    error && onRetry && window.setTimeout(() => {
      counter > 0 && setCounter && setCounter(counter - 1);
      counter === 1 && onRetry();
    }, 1000);

    return () => {
      counter === 0 && setCounter(5);
    };
  });

  return (
    <Wrapper className={error && 'error'} width={width} height={height} >
      <Inline>
        <Status>
          {error && errorMsg || loadingMsg} {error && onRetry && retryMsg}
        </Status>
        {
          error && onRetry && 
          <OpenButton onClick={() => onRetry && onRetry()}>Retry</OpenButton>
        }
      </Inline>
    </Wrapper>
  );
};

export default memo(Placeholder);
