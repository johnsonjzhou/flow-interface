/**
 * Works like a Form, but not using the <form> tag. 
 * Identifies and keeps track of all user input elements in the children branch 
 * as identified by "name" prop. 
 * 
 * @author  Johnson Zhou <johnson@simplepharmacy.io> 
 * 
 * @export  Form  as default
 * 
 * @param  {function}  props.callback 
 *  - invoked on first load and each time data changes in one of the fields 
 *  - when invoked, applies the formData object as the single argument 
 * 
 * @example  usage  
 * <Form>{...}</Form>  
 * 
 * @example  field components must: 
 * - be identified by the 'name' prop  
 * - accept a 'callback' prop, which will then apply an object as the only 
 *   argument being { name, valid, value } 
 * 
 * @example  default values for field components 
 * - be specified by the 'defaultValue' prop  
 * 
 * @example  formData  
 * {
 *  valid: true | false, 
 *  changed: true | false, 
 *  data: [ 
 *    { name, valid, value }, 
 *    ...
 *  ]
 * }
*/

import React, { memo, cloneElement, useEffect, useRef } from 'react'; 
import { traverseChildBranch } from 'flow-utils';

const Form = (props) => {

  const { 
    children, 
    callback 
  } = props;

  const initialFormData = { 
    valid: false, 
    changed: false, 
    data: [], 
  };

  // keep track of formData as they get updated 
  // access via formData.current
  const formData = useRef(initialFormData);       // { name, valid, value }
  
  // keep track of defaultValues to compare if data has changed 
  const defaultValues = useRef([]);               // { name, defaultValue }

  /**
   * Receive and handle newValue from fields, 
   * this gets applied as the callback prop to field components during render. 
   * @param  {object}  newValue - { name, valid, value }
   */
  const updateValue = (newValue) => {
    
    const {
      name
    } = newValue;
  
    // update value if existing or create a new value 
    const existing = formData.current.data.findIndex(field => field.name === name);
    (existing >= 0) && (formData.current.data[existing] = newValue) 
      || 
    formData.current.data.push(newValue);
  
    // check whether all values within data are valid 
    formData.current.valid = formData.current.data.every(field => field.valid);

    // evaluate whether newValue has deviated from it's defaultValue 
    const unchanged = formData.current.data.every(field => {
      const {
        name, 
        value 
      } = field;

      const hasDefault = defaultValues.current.find(field => field.name === name);

      return (!hasDefault && value === '') 
        || 
      (hasDefault && hasDefault.defaultValue === value);
    });

    formData.current.changed = !unchanged;

    // apply the callback if there is one 
    callback && callback(formData.current);
  };

  /**
   * Initialise a field, 
   * check if it has an existing formData.current.data value, 
   * if not, load a blank value object 
   * if an element is required, the default valid state is false 
   * lastly merge in additional props as required: 
   *  - callback: updateValue 
   * @param  {string}  name - name of the field 
   */
  const initField = (child) => {

    const {
      props: { name, required, defaultValue }
    } = child;

    // return child unchanged if there are no name props 
    if (!name) return child;

    // check if the field is existing 
    const existing = formData.current.data.find(field => field.name === name);

    // if not existing, initiate data within formData 
    !existing && formData.current.data.push({
      name, 
      valid: !(required && !defaultValue), 
      value: defaultValue || '', 
    });

    // also, track it's defaultValue 
    !existing && defaultValue && defaultValues.current.push({
      name,
      defaultValue
    });

    return cloneElement(child, {
      key: name, 
      callback: updateValue, 
    });
    // React will warn if key not included 
    // React will warn if duplicate names are used 
  };

  useEffect(() => {
    callback && callback(formData.current);
  }, []);

  return (
    <> { 
      children.map((child, key) => traverseChildBranch(child, initField, key))
    } </>
  )
};

export default memo(Form);