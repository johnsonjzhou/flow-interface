/**
 * A credit card payment component that looks like a credit card 
 * @author  Johnson Zhou  <johnson@simplyuseful.io>
 * 
 * @export  PaymentCard (default)
 * 
 * @uses  react-payment-inputs
 * @see  https://github.com/medipass/react-payment-inputs
 * 
 * @param  {function}  props.callback 
 *  - callback({ name, value, valid })
 *  - value: Object({ name, number, expMM, expYYYY, cvc })
 * @param  {string}  props.name
 *  - the name used to form the 'name' part of the callback,
 *  - defaults to: payment_card
 * @param  {array}  props.acceptOnly
 *  - array of string defining which cards to accept
 *  - Array([
 *     'visa', 'mastercard', 'amex', 'dinersclub', 'jcb', 'unionpay',
 *     'maestro', 'elo', 'hipercard'
 *    ])
 * @param  {bool}  props.center 
 *  - vertically center the component 
 * 
 * @note  we will be referring to the card check code as "cvc"
 */
import React, { memo, useState, useEffect, useRef } from 'react';
import { usePaymentInputs } from 'react-payment-inputs'; 
import images from 'react-payment-inputs/images';
import { escape } from 'html-escaper';

import styled from 'styled-components';
import { payment } from '../styles/paymentcard';
import { flexbox } from '../styles/mixins';

import { wrapper } from '../styles/layouts';

const {
  Wrapper, ErrorMessage, Logo, Name, Number, Expiry, Cvc, CardName 
} = payment;

const Row = styled(wrapper.inline)`
  height: 1.2em;
  padding: 0.2em 0;
  min-height: initial;
  ${flexbox({
    direction: 'row', 
    xAlign: 'right', 
    yAlign: 'top'
  })}
`;

const RowCardName = styled(Row)`
  height: 3.2em; 
  padding: 0;
`;

const RowLast = styled(Row)`
  height: 2em; 
  justify-content: space-between;
`;

const PaymentCard = (props) => {

  const {
    name = 'payment_card', 
    acceptOnly, 
    center, 
    callback
  } = props;

  const paymentData = useRef({
    name: null,
    number: null,
    expMM: null,
    expYYYY: null,
    cvc: null,
  });

  // whether the user has touched this input 
  const touched = useRef({
    cardName: false, 
    cardNumber: false, 
    expiryDate: false, 
    cvc: false 
  });

  const [ changed, setChanged ] = useState(false);
  const [ nameError, setNameError ] = useState(false);

  const wrapperRef = useRef(); 

  const cardNumberValidator = ({ cardType }) => { 
    // if we have defined acceptable cards in acceptOnly 
    // and the card is not within acceptable cards 
    return acceptOnly && 
    Array.isArray(acceptOnly) && 
    !acceptOnly.includes(cardType.type) && 
    'Card type not accepted';
  };

  const onFocus = () => {
    wrapperRef.current.classList.add('focus');
  };

  const onBlur = (event) => {
    const { 
      target: { name, value }
    } = event;

    wrapperRef.current.classList.remove('focus');

    // update value 
    // @see https://github.com/medipass/react-payment-inputs/blob/master/src/utils/validator.js
    switch (name) {
      case 'cardName': 
        paymentData.current.name = escape(value);
        !nameError && !value && setNameError('Enter a card name');
        nameError && value && setNameError(false);
      break;
      case 'cardNumber': 
        const cardNumber = value.replace(/\s/g, '');
        paymentData.current.number = escape(cardNumber);
      break;
      case 'expiryDate': 
        const expiryDate = value.replace(' / ', '').replace('/', ''); 
        const month = expiryDate.slice(0, 2); 
        const year = `20${expiryDate.slice(2, 4)}`; 
        paymentData.current.expMM = escape(month); 
        paymentData.current.expYYYY = escape(year); 
      break;
      case 'cvc': 
        paymentData.current.cvc = escape(value);
      break;
    };

    // mark input as touched 
    !touched.current[name] && (touched.current[name] = true);
    
    // mark input as changed 
    setChanged(true);
  };

  // @see https://github.com/medipass/react-payment-inputs#data 
  // usePaymentInputs will trigger re-render due to internal useState calls 
  const {
    meta: { 
      cardType,                 // string
      error: paymentError,      // string || undefined
    },
    getCardImageProps, 
    getCardNumberProps, 
    getExpiryDateProps, 
    getCVCProps 
  } = usePaymentInputs({ 
    autoFocus: false, 
    cardNumberValidator, 
    onBlur, 
  });

  const validateAndCallback = () => {
    // evaluate validity 
    const valid = (!paymentError && !nameError); 

    const allTouched = (
      touched.current.cardNumber && 
      touched.current.cardNumber && 
      touched.current.expiryDate && 
      touched.current.cvc && 
      true || false 
    );

    // apply error styling 
    allTouched && valid && wrapperRef.current.classList.remove('error');
    allTouched && !valid && wrapperRef.current.classList.add('error');

    const allFilled = (
      paymentData.current.name && 
      paymentData.current.number && 
      paymentData.current.expMM && 
      paymentData.current.expYYYY && 
      paymentData.current.cvc && 
      true || false
    );

    const value = {
      name, 
      valid: (allFilled && valid), 
      value: (allFilled && valid) && paymentData.current || ''
    };

    callback && callback(value); 

    setChanged(false);
  };

  useEffect(() => { 

    changed && validateAndCallback();

  }, [ changed ]);

  return (
    <Wrapper 
      ref={wrapperRef} 
      className={center && 'center'} 
      data-card-type={cardType && cardType.type} 
    >
      <RowCardName>
        <CardName>{
          cardType && cardType.displayName || 'Card Payment'
        }</CardName>
      </RowCardName>
      <Row>
        <Name 
          ariaLabel='Card name' 
          autocomplete='cc-name' 
          name='cardName' 
          id='cardName' 
          placeholder='Card name' 
          type='text' 
          onFocus={onFocus}
          onBlur={onBlur}
        />
      </Row>
      <Row>
        <Number {...getCardNumberProps({ onFocus })} />
      </Row>
      <RowLast>
          <Expiry {...getExpiryDateProps({ onFocus })} />
          <Cvc {...getCVCProps({ onFocus })} />
          <Logo {...getCardImageProps({ images })} />
      </RowLast>
      <ErrorMessage>
        { nameError || paymentError }
      </ErrorMessage>
    </Wrapper>
  );
};

export default memo(PaymentCard);