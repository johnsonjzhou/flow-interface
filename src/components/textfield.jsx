/**
 * Renders a text field (input or textarea) 
 * Handles validation 
 * Can pass back values to parent using callback function 
 * @author  Johnson Zhou <johnson@simplepharmacy.io>
 * 
 * @note  React uses camelCase notation for some html attributes!
 * 
 * < Required params >
 * @param	{string}  props.name
 *  - field name, default: undefined
 * @param	{string}  props.type
 *  - html input types, default: text
 * @param	{string}  props.label
 *  - field label, default: Text Field
 * 
 * < Optional params >
 * @param  {boolean}  props.multiline
 *  - switches the field between a standard input=text and textarea
 *  - false: input, true: textarea, default: false
 * @param  {string}  props.defaultValue
 *  - use if there is data to display on load
 * @param	 {string}  props.required
 *  - whether this field can be blank
 * @param  {false|string}  props.rejected
 *  - false = valid field, behaves normally 
 *  - string = invalid field, the string is the reason 
 * @param  {object}  props.validation
 *  - overrides default type based validation { pattern: regex, message: string }
 * @param  {bool}  props.small
 *  - indicates field can be very narrow, so do not render the feedback text
 * @param  {bool}  props.disabled 
 *  - disables the field and show a disabled indicator 
 * @param  {int}  props.maxlength
 *  - limit the maximum length of the input 
 *  - this is handled to provide a consistent experience across browsers 
 * @param  {bool}  props.htmlEscape
 *  - whether or not to escape html unsafe characters in the value 
 * 
 * @param  {mixed}  ...props
 *  - pass through any other props directly to the DOM as needed 
 * 
 * < Callback >
 * @param  {function}  props.callback
 *  - applied when field loses focus
 *  - callback({ name, valid, value });
 */

import React, { memo, useState, useEffect, useRef } from 'react';
import { escape } from 'html-escaper';
import { validationPatterns } from 'flow-utils';
import { textfield } from '../styles/textfield'; 

import {
  RiLockFill as IconDisabled, 
  RiCloseFill as IconError, 
  RiStarFill as IconRequired, 
  RiPencilFill as IconChanged, 
} from 'react-icons/ri';

const { Wrapper, Feedback, Label, Input, Textarea } = textfield;

const TextField = (props) => { 
  
  const {
    multiline,                         // input or textarea 
    name = 'undefined',                // name attribute 
    type = 'text',                     // type attribute 
    label = 'Text Field',              // label of the field 
    defaultValue = '',                 // the default value 
    required,                          // is a required field 
    rejected: parentRejected,          // assigned rejection state, false or reason
    validation: parentValidation,      // assigned validation rules { pattern, message }
    maxlength,                         // maxlength, max character length 
    small,                             // narrow rendering, no feedback message
    disabled,                          // assigned disabled state 
    callback,                          // callback to apply data and validation 
    htmlEscape = true,                 // escape html unsafe characters 
    ... restProps                      // pass anything else directly to DOM 
  } = props;

  const [ rejected, setRejected ] = useState(false);  // reason or false
  const [ changed, setChanged ] = useState(false);    // bool
  const wrapperRef = useRef(null);
  const inputRef = useRef(null);
  const value = useRef(htmlEscape && escape(defaultValue) || defaultValue);

  /**
   * Rejection (validation) testing 
   * @param  {string}  value - value to test
   * @param  {string}  [type]  - input type to look up default validation pattern
   * 
   * @return  {false|string} - if rejected, a reason is provided
   */
  const checkRejected = (value, type) => {
    // default falsy
    let reject = true;

    // assign validation pattern
    // { pattern, message }
    const validation = 
      // from parent
      (
        typeof parentValidation === 'object' && 
        parentValidation.hasOwnProperty('pattern') 
      ) && parentValidation  
        || 
      // from default based on type
      (
        type && validationPatterns[type] && 
        validationPatterns[type].hasOwnProperty('pattern')
      ) && validationPatterns[type] 
        || 
      // match anything if not specified 
      { pattern: /[\s\S]*/ }
    ;

    // test for rejection
    reject = 
      (!validation.pattern.test(value)) && (validation.message || 'Not valid') 
        || 
      false
    ;

    // when there is no value, reject if it's a required field 
    (value === '') && 
    (reject = required && 'Required' || false);

    return reject;
  };

  /**
   * When the field gets focus
   */
  const onFocus = () => {
    wrapperRef.current.classList.add('focus');
  };

  /**
   * When the field loses focus, perform some validation
   * @param  {FocusEvent}  event
   */
  const onBlur = (event) => {
    const {
      target: { 
        value: newValue,        // HTMLDataElement.value
        type                    // HTMLAttribute.type
      }
    } = event;

    // update internal value tracker 
    value.current = newValue;

    // remove focus styling
    wrapperRef.current.classList.remove('focus');

    // run validation 
    const reject = checkRejected(newValue, type);

    // escape any unsafe HTML characters if required 
    const callbackValue = htmlEscape && escape(newValue) || newValue;

    // apply value object to callback 
    callback && callback({
      name, 
      valid: !reject, 
      value: !reject && newValue && callbackValue || '',
    });

    // set rejected state, 
    // we want to do this everytime as rejection reasons may change 
    setRejected(reject);

    // evaluate changed status 
    setChanged(value.current !== defaultValue);
  };

  /**
   * When the field value changes, 
   * truncate characters longer than specified in maxlength
   * @param  {Event}  event
   */
  const onChange = (event) => {
    const { 
      target: { value } 
    } = event;

    (typeof value === 'string') && (typeof maxlength === 'number') && 
    (event.target.value = value.substring(0, maxlength));
  };

  /**
   * When certain keys are pressed
   * @param  {KeyboardEvent}  event
   */
  const onKeyDown = (event) => {
    const {
      target: { tagName, type }, 
      key
    } = event;

    const {
      number 
    } = validationPatterns;

    event.stopPropagation();

    switch (key) {

      case 'Escape':
        event.target.value = value.current;
        return event.target.blur();

      case 'Enter':
        return (tagName === 'INPUT') && event.target.blur();

      case 'Backspace': 
      case 'Clear': 
      case 'Copy': 
      case 'CrSel': 
      case 'Cut': 
      case 'Delete': 
      case 'Del': 
      case 'EraseEof': 
      case 'ExSel': 
      case 'Insert': 
      case 'Paste': 
      case 'Redo': 
      case 'Undo': 
      case 'Tab': 
        // ignore editing keys 
        return;

      default:
        // sanitise character based input to have consistent cross-browser
        // experience when input type=number
        // on chrome, invalid characters do not register, 
        // on firefox, invalid characters register but value is null 
        (type === 'number') && 
        !number.pattern.test(key) && 
        event.preventDefault();
        return;
    }
  };

  /**
   * Handles height resizing of Textarea when lines are added or removed. 
   * Seems to work more reliably when bound to InputEvent rather than 
   * KeyBoardEvent
   */
  const resize = () => {
    (new Promise((resolve) => {
      // need to reset height presets in order to get accurate heights
      wrapperRef.current.style.removeProperty('--height-wrapper');
      wrapperRef.current.style.removeProperty('--height-textarea');
      resolve();
    }))
    .then(() => {
      const {
        scrollHeight: actualHeight,
        clientHeight: screenHeight,
      } = inputRef.current;

      let {
        offsetHeight: wrapperHeight         // note Wrapper must be border-box
      } = wrapperRef.current;

      // determine if there is a height difference between screen and actual
      const heightDiff = actualHeight - screenHeight;

      // skip following if no height difference 
      if (heightDiff === 0) return;

      // adjust height to actual
      // note actualHeight includes padding
      wrapperHeight = wrapperHeight + heightDiff;
      wrapperRef.current.style.setProperty(
        '--height-wrapper', `${wrapperHeight}px`
      );
      wrapperRef.current.style.setProperty(
        '--height-textarea', 
        `calc(${actualHeight}px - (var(--padding-y) * 2))`
      );
    })
    .catch(error => {
      // on unmount, it is possible that inputRef and wrapperRef 
      // are unmounted before the promise resolves, leading to an uncaught
      // error when setting heights, we don't need to do anything with this
      // apart from catching it 
    });
  };

  /**
   * Attributes that we are handling that will be passed to the DOM 
   */
  const managedAttrs = {
    name, 
    type, 
    'aria-label': label, 
    disabled, 
    onBlur, 
    onChange, 
    onFocus,
    ref: inputRef
  };

  useEffect(() => {
    // add error styling if not valid
    (rejected || parentRejected) && 
    wrapperRef.current.classList.add('error');

    // add InputEvent listener to resize a Textarea
    (inputRef.current.tagName === 'TEXTAREA') && 
    inputRef.current.addEventListener('input', resize);

    // add KeyboardEvent listener 
    inputRef.current.addEventListener('keydown', onKeyDown);

    // if values mismatch, assign the internal value 
    // also run resize if the input is a textarea
    if (inputRef.current.value !== value.current) {
      inputRef.current.value = value.current;
      (inputRef.current.tagName === 'TEXTAREA') && resize();
    }

    // add filled styling if value exists 
    (inputRef.current.value) && inputRef.current.classList.add('filled');

    return () => {
      // remove status styling 
      if (wrapperRef.current) {
        wrapperRef.current.classList.remove('error');
        wrapperRef.current.classList.remove('focus');
        wrapperRef.current.classList.remove('filled');
      }

      // remove listeners
      if (inputRef.current) {
        inputRef.current.removeEventListener('input', resize);
        inputRef.current.removeEventListener('keydown', onKeyDown);
      }
    }
  });

  return (
    <Wrapper ref={wrapperRef}>
      {
        multiline && 
        <Textarea {...managedAttrs} {...restProps}/> 
          ||
        <Input {...managedAttrs} {...restProps}/>
      }
      <Label aria-hidden='true'>
        <Feedback>
          {
            (rejected || parentRejected) && <IconError/> || 
            (disabled) && <IconDisabled/> || 
            (changed) && <IconChanged/> || 
            (required) && <IconRequired/>
          }
          { !small && ( parentRejected || rejected ) }
        </Feedback>
        { label }
      </Label>
    </Wrapper>
  );
}

export default memo(TextField);