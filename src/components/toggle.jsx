/**
 * A toggle switch
 * @author  Johnson Zhou <johnson@simplyuseful.io> 
 * 
 * @export  Toggle as default
 * 
 * @param  {string}  props.name  
 * - the 'name' attribute  
 * @param  {string}  props.label  
 * - the description for the label  
 * @param  {bool}  [props.defaultValue]  
 * - default state for the toggle  
 * @param  {function}  [props.callback]  
 * - a callback that gets invoked with the 'data' object 
 */

import React, { memo, useState } from 'react';
import { toggle } from '../styles/selection';

const {
  Wrapper, 
  Label, 
  Switch 
} = toggle;

const Toggle = (props) => {
  const {
    name, 
    label, 
    defaultValue, 
    callback, 
  } = props;

  const [ active, setActive ] = useState(defaultValue || false);

  const onClick = () => {
    const data = {
      name, 
      valid: true, 
      value: !active,
    };
    callback && callback(data);
    setActive(!active);
  };

  return (
    <Wrapper 
      className={active && 'active'} 
      onClick={onClick}
    >
      <Switch/>
      <Label>{label}</Label>
    </Wrapper>
  );
};

export default memo(Toggle);