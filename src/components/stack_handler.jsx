/**
 * Handler for multiple instances of Stacks
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 */
import React, { memo, useEffect, useReducer, isValidElement } from 'react';
import Stack from './stack';
import ComingSoon from '../content/coming_soon';

/**
 * Template for a blank stack
 * call this using the clone getter
 */
const blankStack = { 
  content: null, 
  get clone() {return Object.create(this)}
};

/**
 * Reducer function for stacks
 * @param  {array}  stacks - the stacks array
 * @param  {object}  action 
 * @param  {string}  action.type - 'open' || 'close'
 * @param  {object}  action.payload 
 * @param  {React.Component}  action.payload.content - the content to open
 * @param  {object}  [action.payload.props] - additional props to apply
 * @param  {integer}  action.payload.index - the stack index to close
 */
const stackManager = (stacks, {type, payload}) => {
  let newStacks = [];
  switch (type) {
    case 'open':
      /**
       * Open a new stack by:
       * 1. assign content and props to the top most stack (should be free)
       * 2. push a blank stack above the content stack
       */
      let { content = null, props = {} } = payload;

      // limit to 9 stacks, as we have only defined css for 10 stacks
      // need one spare for pop ups
      if (stacks.length >= 9) {
        window.Console && window.Console
        .notify(`Maximum stack limit of 10 reached.`, 'Open Stack');
        return stacks;
      }

      // copy current stacks, as we need to return a different array
      // as compared by Object.is to trigger a render
      newStacks = [...stacks];

      //todo -- rename to 'focusedStack'
      // get the index of the focusedStack
      const focusedStack = newStacks.length - 1;

      // warn if content is not set or not a valid React component
      if (content === null || !isValidElement(content)) {
        window.Console && window.Console
        .notify(`Content not supplied or not valid React component,\n`+
          `deferring to default content.`, 'Open Stack');
        content = <ComingSoon/>;
      }

      // the focusedStack should be blank, warn if not
      if (newStacks[focusedStack].content !== null) {
        window.Console && window.Console
        .notify('No blank stacks available', 'Open Stack');
        return stacks;
      }

      // assign the new content and props to focusedStack
      newStacks[focusedStack].content = content;
      newStacks[focusedStack].props = props;

      // insert another stack to the end
      newStacks.push(blankStack.clone);

    return newStacks;

    case 'close':
      /**
       * Close a stack for a given index,
       * 1. Remove all stacks beyond the index
       * 2. Reset the indexed stack a blankStack
       */
      const { index = 0 } = payload;
      newStacks = stacks.filter((stack, idx) => (idx < index));
      newStacks.push(blankStack.clone);

    return newStacks;
  }
};

const StackHandler = (props) => {

  // handle array of stacks via reducer function
  // 'handleStacks' -> calls 'stackManager'
  const [ stacks, handleStacks ] = useReducer(stackManager, [ blankStack.clone ])

  /**
   * Opens a new stack with supplied content and props
   * @param  {event}  event
   * @param  {object}  event.detail
   * @param  {React.Component}  event.detail.content
   * @param  {object}  event.detail.props
   */
  const openStackEvent = ({ detail }) => {
    const { content, props } = detail;
    handleStacks({type: 'open', payload: { content, props }});
  };
  window.addEventListener('flow-openstack', openStackEvent);

  /**
   * Closes a stack with supplied stack index
   * @param  {event}  event
   * @param  {object}  event.detail
   * @param  {int}  event.detail.index
   */
  const closeStackEvent = ({ detail }) => {
    const { index } = detail;
    if (typeof index !== 'number') {
      window.Console && 
      window.Console.notify('Stack index not provided', 'Close Stack');
      return;
    }
    handleStacks({type: 'close', payload: { index }});
  };
  window.addEventListener('flow-closestack', closeStackEvent);

  // calculate focusedLayer
  const focusedLayer = stacks.length - 2;

  useEffect(() => {
    // on DOM update

    // watch for 3D perspective when there are two or more stacks open
    (focusedLayer > 0) && 
      window.dispatchEvent(new CustomEvent('perspective3d-watch'));
    (focusedLayer <= 0) && 
      window.dispatchEvent(new CustomEvent('perspective3d-disconnect'));

    return (() => {
      // on unmount
      window.dispatchEvent(new CustomEvent('perspective3d-disconnect'));
      window.removeEventListener('flow-openstack', openStackEvent);
      window.removeEventListener('flow-closestack', closeStackEvent);
    });

  }, [ focusedLayer ]);

  // render the Stack(s)
  // window.Console.log(stacks, 'Render Stacks');
  return stacks
  .map((stack, index) => {
    const { content, props: otherProps } = stack;

    const stackProps = {
      index, 
      focused: (index === focusedLayer), 
      content: (typeof content === 'function' && content()) || content
    };
    
    return (
      <Stack key={`stack-${index}`} {...stackProps} {...otherProps} />
    );
  });
}

export default memo(StackHandler);