/**
 * Displays a block of pre-formatted text
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @export  Codeblock
 * 
 * @param  {string}  props.language - indicating the code language  
 * @param  {string}  props.data - pre-formatted text to display  
 */

import React, { memo } from 'react';
import styled from 'styled-components';
import { text } from '../styles/text';

const Block = styled.div`${text.codeblock}`;

const Codeblock = (props) => {
  const { 
    language = 'code', 
    data
  } = props;

  return (
    <Block>
      <pre>
      <code className={`language-${language}`}>
        { data && data || 'Nothing to display' }
      </code>
      </pre>
    </Block>
  );
};

export default memo(Codeblock);