/**
 * A conditional wrapper that renders a wrapper when a condition is met 
 * @see  https://blog.hackages.io/conditionally-wrap-an-element-in-react-a8b9a47fab2
 */

const ConditionalWrapper = ({ condition, wrapper, children }) => {
  return condition ? wrapper(children) : children;
};

export default ConditionalWrapper;