/**
 * Renders some markdown content,
 * can be given a source to fetch remote content
 * @author  Johnson Zhou  <johnson@simplyuseful.io>
 *
 * @param  {string}  props.markdown
 *  - mark down text, if this is set, will avoid fetching from source
 * @param  {string}  props.children
 *  - behaves same as props.markdown, the child must be a string
 * @param  {string}  props.source
 *  - link to markdown content to fetch
 *
 * @note  <a>
 * All links will have target=_blank applied
 */
import React, { memo, useState, useEffect } from 'react';
import ReactMarkdown from 'react-markdown';
import Placeholder from './placeholder';

import { md } from '../styles/markdown';
import ServiceWorkerHandler from 'flow-serviceworker';

const Markdown = (props) => {

  const {
    markdown,
    children,
    source,
  } = props;

  let sw;

  const [ error, setError ] = useState(false);
  const [ remoteContent, setRemoteContent ] = useState(null);

  // fetch content from url
  // subscribe to service worker messages in case remote content is updated
  // only do this if markdown or children is missing
  useEffect(() => {

    /**
     * Handles fetching of content from the source
     * @param  {string}  source - the source url
     * @param  {string}  method - fetch method, GET | REFRESH
     */
    const fetchFromSource = ({
      source,
      method = 'GET',
    }) => {
      fetch(source, { method })
      .then(response => {
        const {
          ok,
          headers
        } = response;

        const contentType = headers.get('Content-Type');

        return (
          ok && contentType.startsWith('text') && !contentType.includes('html')
          && response.text()
           ||
          null
        );
      })
      .then(text => {
        // set the text to state to render
        // only if text has changed
        text && (text !== remoteContent) && setRemoteContent(text);

        // set error state if no content and we are not using REFRESH method
        (text === null) && (method !== 'REFRESH') &&
        setError(true);
      })
      .catch(error => {
        // TypeError = loss of connectivity
        setError(true);
      });
    }

    // handle incoming messages from service worker
    const handleSWMessages = ({url}) => {
      source && url &&
      url.includes(source) &&
      fetchFromSource({ source, method: 'REFRESH' });
    };

    // fetch and assign serviceworker if markdown or children is not defined
    if (
      !(markdown || children) && source && !error
    ) {
      // register service worker handler
      sw = new ServiceWorkerHandler();
      sw.setMessageCallback(handleSWMessages);

      // initiate fetch
      fetchFromSource({ source });
    }

    return () => {
      // on unmount
      sw && sw.unsetMessageCallback();
    }
  });

  return (
    <md.wrapper>
      {
        ((markdown || children || remoteContent) &&
          <ReactMarkdown
            children={ markdown || children || remoteContent }
            linkTarget={'_blank'}
          />
        )
        ||
        (error && <Placeholder error onRetry={() => setError(false)}/>)
        ||
        <Placeholder/>
      }
    </md.wrapper>
  );

}

export default memo(Markdown);