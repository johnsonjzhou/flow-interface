/**
 * The Stack modal to display content
 * @author Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @param  {int}  props.index
 *  - applied when using StackHandler, refers to the stack index
 * @param  {bool}  props.focused 
 *  - whether this stack has focus
 * @param  {bool}  [props.hideControls]
 *  - optional, render the modal without modal controls
 * @param  {React.Component}  props.content
 *  - the content component to display
 * 
 * @note  applies the following props to content component (children)
 * setLoading, setChanged, setQuickActions, openStack, closeStack
*/
import React, { cloneElement, useState, useRef, memo, useEffect } from 'react';
import { stack } from '../styles/stack'; 
import ErrorBoundary from './errorboundary';
import { beforeUnload } from 'flow-utils';

import { divider } from '../styles/layouts'; 
import { 
  StackControlButton as ControlButton, 
  QuickActionButton 
} from './buttons';

import { Dots } from '../styles/loading_dots'; 

import Popup from './pop_up';
import { 
  RiArrowLeftLine as IconBack, 
  RiErrorWarningLine as IconError
} from 'react-icons/ri';

const Divider = divider.horizontal;

const Controls = (props) => {
  const {
    quickActions = [], 
    closeStack 
  } = props;

  // quickActions is an array of objects,
  // {React.Component} quickActions[].icon - from 'react-icons' 
  // {function}  quickActions[].action - callback to apply when clicked

  return (
    <stack.control>
      <ControlButton onClick={closeStack} >
        <IconBack/>
      </ControlButton>
      {quickActions.length > 0 && (
        <Divider/>
      )}
      {quickActions.length > 0 && quickActions
      .map(({
        icon = <IconError/>, 
        action
      } = {}, index) => 
        <QuickActionButton onClick={action} key={`quickAction-${index}`}>
          {icon}
        </QuickActionButton>
      )}
    </stack.control>
  );
};

const ControlsMemo = memo(Controls);

const Stack = (props) => {

  const {
    index, 
    focused = true,
    hideControls, 
    content,  
    ...restProps
  } = props;

  const [ quickActions, setQuickActions ] = useState([]);

  const wrapperRef = useRef();
  const modalRef = useRef();

  const wrapperEl = wrapperRef.current;
  const modalEl = modalRef.current;

  // whether the content has been changed
  // access with changed.current
  const changed = useRef(false);
  const setChanged = (bool) => {
    // update changed status
    changed.current = bool; 

    // show a warning before navigating away if the content has changed
    // eg. using the back button
    (bool) && 
    window.addEventListener('beforeunload', beforeUnload);
    (!bool) && 
    window.removeEventListener('beforeunload', beforeUnload);
  };

  // sets the loading style on the modal wrapper
  const setLoading = (loading = true) => {
    (loading) && wrapperEl.classList.add('loading');
    (!loading) && wrapperEl.classList.remove('loading');
  };

  /**
   * Sets the quick actions by the content, via callback
   * @param  {array}  newQuickActions
   * @param  {string}  newQuickActions[].icon - material icon name
   * @param  {function}  newQuickActions[].action - function to call
   */
  const setQuickActionsByContent = (newQuickActions) => {
    // skip if we are hiding controls
    if (hideControls) return;

    // only continue if quickActions have not already been set
    // do a deep comparison with JSON.stringify but only with the action
    // as we are using a React.Component for the icon and this will cause
    // a TypeError due to circular reference
    //? -- can this be done in a better way
    if (
      JSON.stringify(quickActions.map(qa => qa.action)) 
      === JSON.stringify(newQuickActions.map(qa => qa.action))
    ) return;

    // use a Promise to not block the UI transitions
    (new Promise((resolve) => {
      // resolve on transitionend
      wrapperEl && wrapperEl.
      addEventListener('transitionend', resolve, { once: true });

      // or after 200ms as fallback
      window.setTimeout(resolve, 200); 
    }))
    .then(() => setQuickActions(newQuickActions));
  };

  /**
   * Closes the current stack
   * 1. check whether content has been flagged as 'changed', if so open a Popup
   * 2. reset quickActions, loading style and chnaged status
   * 3. for exit transition to work properly, we must hold the modal height
   */
  const closeStack = () => {
    // the close procedure 
    const close = () => {
      // reset state
      setQuickActions([]);
      setLoading(false);
      setChanged(false);

      // Dispatch the closestack event
      window.dispatchEvent(new CustomEvent('flow-closestack', { detail: {
        index: (typeof index === 'number' && index) || 0
      }}));
    }

    // if content has been flagged as changed,
    // open a Popup stack, which can also apply the close procedure
    if (changed.current) {
      window.dispatchEvent(new CustomEvent('flow-openstack', { detail: {
        content: (
          <Popup proceed='Discard' onProceed={close} cancel={'Go Back'} >
            {`# Content may not be saved \n`+
            `Did you mean to close and discard changes?`}
          </Popup>
        ), 
        props: {hideControls: true}
      }}));
      return;
    }

    // when closing,
    // hold the modal height for 200ms whilst the exit animation is played
    // match the time to transition duration
    (new Promise((resolve) => {
      const height = modalEl.clientHeight;
      modalEl.style.setProperty('--height', `${height}px`);
      window.setTimeout(resolve, 200, modalEl);
    }))
    .then(modal => modal.style.removeProperty('--height'));

    // call the close procedure
    close();
  }

  /**
   * Stack opening functiont to pass to content, which triggers the
   * flow-openstack event
   */
  const openStack = (content, props = {}) => {
    window.dispatchEvent(new CustomEvent('flow-openstack', { detail: {
      content, props
    }}));
  };

  const onKeydown = (event) => { 
    event && event.key === 'Escape' && closeStack();
  };

  useEffect(() => { 
    (focused) && window.addEventListener('keydown', onKeydown);
    (!focused) && window.removeEventListener('keydown', onKeydown);

    return () => {
      // on unmount 
      window.removeEventListener('keydown', onKeydown);
      window.removeEventListener('beforeunload', beforeUnload);
    }
  }, [ focused, onKeydown ]);

  //? -- can we do hideControls via styled prop?
  let classList = [];
  hideControls && classList.push('no-controls');

  const wrapperProps = {
    ref: wrapperRef, 
    className: classList.join(' '),
    id: `flow-layer-${index}`
  };

  const modalProps = {
    ref: modalRef,
  };

  //* This is the set of props available to all "content" components
  const contentProps = {
    setLoading,
    setChanged,
    setQuickActions: setQuickActionsByContent,
    openStack,
    closeStack, 
    ...restProps
  };

  const controlProps = {
    quickActions,
    closeStack
  };
  
  return (
    <stack.wrapper {...wrapperProps}>
      <stack.modal {...modalProps}>
        <stack.content>
          <ErrorBoundary message='Error in loading content'>
            {content && cloneElement(content, contentProps)}
          </ErrorBoundary>
        </stack.content>
        <stack.ajax>{<Dots size={'16px'}/>}</stack.ajax>
        {(!hideControls) && <ControlsMemo {...controlProps}/>}
      </stack.modal>
    </stack.wrapper>
  );
}

export default memo(Stack);