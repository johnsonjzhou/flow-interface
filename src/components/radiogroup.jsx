/**
 * A styled Radio Group
 * @author Johnson Zhou <johnson@simplepharmacy.io>
 * 
 * @export  RadioGroup 
 * 
 * @param  {string}  props.name 
 * - the 'name' attribute 
 * @param  {array}  props.options 
 * - array of objects describing the radio options to render
 * @param  {string}  [props.minWidth] 
 * - css recognisable minimum width for grid layout 
 * @param  {string}  [props.maxWidth] 
 * - css recognisable maximum width for grid layout 
 * @param  {function}  [props.callback] 
 * - a callback that gets invoked with the 'data' object 
 * @param  {string}  [props.defaultValue] 
 * - the default value for this field, match this to value in options 
 * 
 * @example  props.options
 * [
 *  { label: 'Radio option 1', value: 'option_1' }, 
 *  { label: 'Default option', value: 'option_2', checked: true }, 
 *  { label: 'Radio option 3', value: 'option_3' }
 * ]
 * 
 * @example  data
 * { name, valid=true, value }
 * 
*/

import React, { memo, useState, useEffect } from 'react';
import { selector } from '../styles/selection';
import { wrapper } from '../styles/layouts';

import {
  IoRadioButtonOff as IconUnchecked, 
  IoRadioButtonOnOutline as IconChecked, 
} from 'react-icons/io5';

const { Wrapper } = selector;
const { Grid } = wrapper;

const RadioOption = memo((props) => {

  const {
    id, 
    name = 'radiogroup', 
    label = 'Radio option', 
    value = 'undefined_value', 
    checked, 
    onClick, 
    ...restProps
  } = props;

  const onChange = (event) => {
    onClick && onClick(event.target.value);
  };

  return (
    <Wrapper>
      <input 
        type='radio' 
        id={id} 
        name={name} 
        value={value}
        onChange={onChange} 
        checked={checked}
        aria-label={label} 
        {...restProps}
      />
      <label
        htmlFor={id} 
        aria-hidden={true} 
      >
        <div className='icon'>
        {
          checked && <IconChecked/> || 
          <IconUnchecked/>
        }
        </div>
        <span>{label}</span>
      </label>
    </Wrapper>
  );
});

const RadioGroup = (props) => {

  const {
    name = 'radiogroup', 
    options = [], 
    minWidth,
    maxWidth, 
    defaultValue, 
    callback, 
  } = props;

  const [ checked, setChecked ] = useState(defaultValue || undefined);

  const updateChecked = (value) => {
    const data = {
      name, 
      valid: true, 
      value
    };
    callback && callback(data);
    setChecked(value);
  };

  useEffect(() => {
    // in case defaultValue is provided after first render 
    checked !== defaultValue && 
    setChecked(defaultValue);
  }, [ defaultValue ]);

  return (
    <Grid min={minWidth} max={maxWidth}> {
      options.map((option, index) => {
        const {
          label = `Radio option ${index}`, 
          value = `option_${index}`, 
          ...restProps
        } = option;

        return (
          <RadioOption 
            key={`${name}-${index}`} 
            id={`${name}-${index}`} 
            name={name} 
            label={label} 
            value={value} 
            checked={checked === value} 
            onClick={updateChecked} 
            {...restProps}
          />
        );
      })
    } </Grid>
  );
};

export default memo(RadioGroup);