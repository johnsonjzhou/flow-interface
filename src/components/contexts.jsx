/**
 * React contexts being used
 * @author Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @export ShellHandler
*/
import { createContext } from 'react';

/**
 * Provides methods for handling shell state methods, such as
 * stack opening, layer handling, 
 * Provider: MonoShell
 */
const ShellHandler = createContext();

export { 
  ShellHandler 
};