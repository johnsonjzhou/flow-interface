/**
 * Styled element for file picker using input type=file
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @see  https://devdocs.io/html/element/input/file
 * 
 * @export  InputFilePicker - as default
 * 
 * @param  {string}  [props.name='filepicker'] 
 *  - name of the field, necessary for the data callback 
 *    and to be recognised by Form see (./form.jsx)
 * @param  {string}  props.accept 
 *  - comma separated mime file type, with wildcard, eg: 'image/*,application/pdf'
 * @param  {int}  props.maxSize
 *  - maximum size in MB, match this to what the server will accept
 * @param  {int}  props.maxQty
 *  - maximum number of files that can be added
 * @param  {string}  [props.capture='environment'] 
 *  - what source to use for media capture
 * @param  {function}  props.callback 
 *  - callback that gets invoked when files are added to removed 
 * @param  {bool}  props.multiple 
 *  - allows more than one file
 * @param  {string}  [props.placeholder='Click to attach files']
 *  - placeholder text to display when no files are selected
 * @param  {bool}  props.reset
 *  - instruction from the parent to reset data within this component
 *  - cancelling the reset should be handled from parent.callback
 * 
 * @callback  props.callback
 * @param  {object}  object - { name, valid, value }
 * - if valid = false, value will be an empty array 
 */
import React, { memo, useEffect, useReducer, useRef } from 'react';
import styled from 'styled-components';

import { filepicker } from '../styles/filepicker';
import { wrapper } from '../styles/layouts';
import { RoundButton, QuickActionButton } from './buttons';

import {
  CgAttachment as AttachIcon, 
  CgPlayListRemove as ResetIcon, 
  CgClose as RemoveIcon
} from 'react-icons/cg';

const { Wrapper, Filelist, File, Name, Action } = filepicker; 

const ResetButton = styled(RoundButton)`
  height: 2rem;
  line-height: 2rem;
  width: 2rem;
  border-radius: 2rem;
  font-size: 1.4rem;
`;

const Inline = styled(wrapper.inline)`
  position: absolute;
  justify-content: flex-start;
  bottom: 0;
  transform: translateY(50%);
  & > * {
    margin-left: 1em;
  }
`;

const InputFilePicker = (props) => {

  const {
    name = 'filepicker', 
    accept, 
    maxSize, 
    maxQty, 
    capture = 'environment', 
    multiple, 
    callback, 
    placeholder = 'Click to attach files', 
  } = props;

  const pickerRef = useRef();
  const wrapperRef = useRef();
  const error = useRef(false);

  /**
   * Applies error styling to the Wrapper 
   */
  const renderError = () => {
    switch (error.current) {
      case true:
        return (wrapperRef.current.classList.add('error'));
      case false:
        return (wrapperRef.current.classList.remove('error'));
    }
  };

  /**
   * Reducer function for handling fileList updates 
   * @param  {array}  fileList - state
   * @param  {object}  object  
   * @param  {string}  object.action 
   * @param  {mixed}   object.payload 
   * 
   * @return  {array}  updated fileList state
   */
  const fileListHandler = (fileList, { action, payload }) => {
    let newFileList = [...fileList];

    switch(action) {
      case 'add':
        // payload is an array of files
        newFileList = newFileList.concat(payload);
      break;
      case 'remove':
        // payload is an integer 
        newFileList.splice(payload, 1);
      break;
      case 'removeAll': 
        newFileList = [];
        renderError(false);
      break;
      default:
        // on render only with no actionable handling 
      return fileList;
    }

    // validate each file, based on accept types and maxSize
    newFileList
    .map(file => {
      (
        (
          maxSize && 
          (file.size > (maxSize*1000000))
        )
         ||
        (
          accept &&
          file.type.match(
            // convert accept string to RegExp
            // eg: 'image/*,application/pdf'
            accept
            .replace(',', '|')
            .replace('/*', '.*')
          ) === null
        )
      ) 
      && (file.invalid = true) || (delete file.invalid);
      return file;
    });

    // validate overall newFileList
    // check that all the files in the newFileList are valid, 
    // check that number of files is below as specified by maxQty 
    const allValid = newFileList.every(file => !file.invalid);

    const tooMany = (maxQty && (newFileList.length > maxQty));

    error.current = (!allValid || tooMany);

    // apply error styling 
    renderError();

    // update fileList
    return newFileList;
  };

  const [ fileList, updateFileList ] = useReducer(fileListHandler, []);

  /**
   * Adds a file to the fileList, 
   * by activating the click event of the file picker input
   * @param  {MouseEvent}  event
   */
  const onClickAttach = (event) => {
    event && event.preventDefault();
    pickerRef.current.click();
  };

  /**
   * Handles the change event of the type=file input, 
   * transform FileList to an array, 
   * clear the input field, 
   * add the array of Files to fileList
   * @param  {Event}  event 
   * @param  {FileList}  event.target.files
   */
  const handleAddFiles = (event) => {
    event && event.preventDefault();
    const picker = event.target;

    // transform FileList to an array of newFiles
    let newFiles = [];
    for (const file of picker.files) {
      newFiles.push(file);
    }

    // clear the input field 
    picker.file = null;
    picker.value = null;

    // add the newFiles array to fileList 
    updateFileList({ action: 'add', payload: newFiles });
  };

  const handleResetFiles = (event) => {
    event && event.preventDefault();
    updateFileList({ action: 'removeAll' });
  };

  useEffect(() => {
    // invoke the callback 
    // this is done here rather than within the reducer fileListHandler 
    // the reducer seems to be called twice when the first file is added 
    (fileList.length > 0) && callback && callback({
      name, 
      valid: !error.current, 
      value: error.current && [] || fileList
    });
  }, [ fileList ]);

  return (
    <Wrapper ref={wrapperRef} >

      <Filelist>{
        (fileList.length < 1) && 
        // placeholder 
        (
          <File className='placeholder'>
            <Name onClick={onClickAttach}>{placeholder}</Name>
          </File>
        )
          || 
        // or, list of files   
        (fileList.map((file, fileIndex) => {

          const {
            name, 
            invalid, 
          } = file;

          const onClickRemove = (event) => {
            event && event.preventDefault();
            updateFileList({ action: 'remove', payload: fileIndex });
          };

          return (
            <File key={`file-${fileIndex}`} className={invalid && 'invalid'}>
              <Name>{name}</Name>
              <Action>
                <QuickActionButton onClick={onClickRemove}>
                  <RemoveIcon/>
                </QuickActionButton>
              </Action>
            </File>
          )
        }))
      }</Filelist>

      <input 
        ref={pickerRef}
        name={name}
        type='file' 
        accept={accept} 
        capture={capture}
        multiple={multiple}
        onChange={handleAddFiles}
      />

      <Inline>
        <RoundButton onClick={onClickAttach}><AttachIcon/></RoundButton>
        { 
          fileList && 
          <ResetButton onClick={handleResetFiles}><ResetIcon/></ResetButton>
        }
      </Inline>

    </Wrapper>
  )
};

export default memo(InputFilePicker);