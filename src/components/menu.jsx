/**
 * The Menu element
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @export Menu - as memo
 * 
 * @note  menu groups are set via callback to FlowInterface 
*/
import React, { memo, useState, useContext } from 'react';
import { menu } from '../styles/menu';
import { ShellHandler } from './contexts';

/**
 * Render a logo,
 * or just the name if a file src is not specified
 * @param  {object}  data
 * @param  {string}  data.name - App name or "alt" tag if src not specified
 * @param  {string}  data.src - src of the logo image
 */
const Logo = (props) => {
  const {
    data: {
      name = 'My App',
      src = null,
    }
  } = props;

  return (
    (src && <menu.logo src={src} alt={name}/>) || 
    <menu.name>{name}</menu.name>
  );
};

const LogoMemo = memo(Logo);

/**
 * Render a menu block with grouped items
 * @param  {array}  items - array of menu items
 * @param  {function}  click - onClick call back for menu item
 */
const MenuBlock = (props) => {
  const {
    items = [], 
    contents
  } = props; 

  //! const sH = useContext(ShellHandler);

  // selected group as state
  const [ currentGroup, setGroup ] = useState(undefined);

  const selectGroup = ({ group, background }) => {
    // group and background can be undefined
    (group !== currentGroup) && setGroup(group);
    //! sH.setMenuGroup({ group, background });
    const setBackground = new CustomEvent('flow-set-background', {
      detail: { background }
    })
    window.dispatchEvent(setBackground);
  };

  const openStack = (content) => {
    window.dispatchEvent(new CustomEvent(
      'flow-openstack', {
        detail: { content }
      }
    ));
  }

  return (
    <menu.menu>
      {(currentGroup) && (
        <menu.item 
          key={`item-back`}
          onClick={() => selectGroup({ group: undefined })}
        >
          Back
        </menu.item>
      )}
      {items.map((item, index) => { 
        const {
          label,
          hide,
          setGroup,
          group,
          background,
          content: target
        } = item;

        // do not render an item if hidden
        // cannot use filter as will mutate array index
        if (hide) return;

        // do not render if not belonging to current group
        // ensure default group is 'undefined', so it becomes
        // optional to specify a group property
        if (group !== currentGroup) return;

        return (
          <menu.item 
            key={`item-${index}`}
            onClick={() => {
              try {
                if (setGroup) return selectGroup({ group: setGroup, background }); 
                openStack(contents[target]);
              } catch (error) {
                window.Console && window.Console.handleError(error);
              }
            }}
          >
            {label}
          </menu.item>
        );
      })}
    </menu.menu>
  );
};

const MenuBlockMemo = memo(MenuBlock);

const Menu = (props) => {

  const {
    menu: items = [],
    logo = {name: 'My App', src: null},
    contents = {}, 
  } = props;

  return(
    <menu.wrapper id={'flow-layer-menu'}>
      <LogoMemo data={logo}/>
      <MenuBlockMemo 
        items={items} 
        contents={contents} 
      />
    </menu.wrapper>
  );
}

export default memo(Menu);