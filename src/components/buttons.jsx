/**
 * Some preformed button types, extending the Button element
 * @author  Johnson Zhou  <johnson@simplyuseful.io>
 * 
 * @export OpenButton
 * @export FilledButton
 * @export StackControlButton
 * @export QuickActionButton
 * @export RoundButton
 */
import styled from 'styled-components';
import { OpenButton, FilledButton } from '../styles/button';

/**
 * StackClose design: Round button designed for a single icon
 * eg (x)
 * @var  {css='black'}  --theme-color-foreground
 */
const StackControlButton = styled(OpenButton)`
  height: 2rem;
  line-height: 2rem;
  width: 2rem;
  border-radius: 2rem;
  padding: 0;
  font-size: 1.2rem;
  text-transform: none;
  color: var(--theme-color-foreground, black);
  &:before {
    border-radius: 2rem;
  }

  // react-icons
  & svg {
    display: block;
    margin: auto;
  }
`;

/**
 * QuickAction design: like open design but smaller and inline with control
 * eg:   Button  
 * @var  {css='grey'}  --theme-color-grey
 * @var  {css='black'}  --theme-color-foreground
 */
const QuickActionButton = styled(OpenButton)`
  height: 2rem;
  line-height: 2rem;
  width: 2rem;
  padding: 0;
  font-size: 0.9rem;
  text-transform: none;
  //color: var(--theme-color-darkgrey, grey);
  &:hover, &:focus {
    color: var(--theme-color-foreground, black);
  }

  // react-icons
  & svg {
    display: block;
    margin: auto;
  }
`;

/**
 * Round design: a filled button with coloured background and white text
 * For key action items, usually with an icon
 * eg: ( + )
 */
const RoundButton = styled(FilledButton)`
  height: 3rem;
  line-height: 3rem;
  width: 3rem;
  border-radius: 3rem;
  padding: 0;
  font-size: 2rem;
  text-transform: none;
  &:before {
    border-radius: 3rem;
  }

  // react-icons
  & svg {
    display: block;
    margin: auto;
  }
`;

export {
  OpenButton, 
  FilledButton, 
  StackControlButton, 
  QuickActionButton, 
  RoundButton
};