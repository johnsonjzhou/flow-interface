 /**
 * A date input that allows date and time to be typed as well as selected.
 * Looks and behaves like a SelectBox.
 * @author  Johnson Zhou  <johnson@simplyuseful.io>
 */
import React, { memo, useState, useRef, useEffect } from 'react';
import { selectbox } from '../styles/selection';
import { datepicker } from '../styles/datepicker';

import {
  RiCalendar2Fill as DateIcon
} from 'react-icons/ri';

const {
  Wrapper,
  ComboBox,
  Label,
  PickerWrapper,
  TimePicker,
} = selectbox;

const {
  Input,
  PickDate,
} = datepicker;

/**
 * ! -- latest @ flow-utils
 * @method  Date.parse
 * @method  toString
 * @method  toJSON
 * @method  getVerboseMonth
 * @method  getVerboseDay
 */
class DateTime extends Date {

  constructor(input) {
    const {
      year, month, date, hour, minutes, seconds
    } = DateTime.parse(input);
    super(year, month-1, date, hour, minutes, seconds);
  }

  /**
   * Replaces Date.parse() that can take a variety of date and time
   * representations that are commonly used by humans. Note, date and month
   * positions are mostly based on the British format.
   * @param  {string}  string  the input string
   *
   * @return  {object}  { year, month, date, hour, minutes, seconds }
   *
   * @example  dates
   * 2020-03-01 || 2020/03/01
   * 01-03-2020 || 01/03/2020
   * 1-3-20 || 1-3 || 1 Mar || 1 March
   *
   * @example  times
   * 14:30:23 || 14:30 || 2:30pm || 2pm || 2 pm
   */
  static parse = (string = '') => {

    /**
     * @see  https://javascript.info/regexp-groups#capturing-groups-in-replacement
     */

    const now = new Date();

    const datetime = {
      year: now.getFullYear(),
      month: now.getMonth() + 1, // returns 0-11, need 1-12
      date: now.getDate(),
      hour: now.getHours(),
      minutes: now.getMinutes(),
      seconds: now.getSeconds(),
    };

    let parsedDate = {} , parsedTime = {};

    const matchAndParseJson = (string, pattern, output) => {
      const matched = string.replace(pattern, output);
      return JSON.parse(matched.slice(matched.indexOf('{'), matched.indexOf('}')+1));
    };

    // handle date portion

    const datePatterns = [
      // 2020-03-01 || 2020/03/01
      /(?<year>[0-9]{4})(?:[-/\s]{1})(?<month>[0-9]{1,2})(?:[-/\s]{1})(?<date>[0-9]{1,2})/,

      // 01-03-2020 || 01/03/2020 || 1-3-20 || 1-3 || 1 Mar || 1 March
      /(?<date>[0-9]{1,2})(?:[-/\s]{1})(?<month>[0-9]{1,2}|[A-Za-z]{3,})(?:[-/\s]{1})?(?<year>[0-9]{2,4})?/,

      // Mar 1 || March 1 || Mar 1 20 || Mar 1 2020
      /(?<month>[0-9]{1,2}|[A-Za-z]{3,})(?<date>^[0-9]{1,2})(?:[-./\s]{1})?(?<year>[0-9]{2,4})?/,

      // 01032020
      /(?<date>[0-9]{2})(?<month>[0-9]{2})(?<year>[0-9]{4})/,

      // 010320
      /(?<date>[0-9]{2})(?<month>[0-9]{2})(?<year>[0-9]{2})/,
    ];
  
    for (const pattern of datePatterns) {
      if (pattern.test(string)) {
        const output = '{"year":"$<year>","month":"$<month>","date":"$<date>"}';
        parsedDate = matchAndParseJson(string, pattern, output);

        // check and manage parsed date object
        Object.keys(parsedDate).forEach((property) => {
          switch (property) {
            case 'year':
              // year is a two digit number, add the first two digits from current
              parsedDate[property].length === 2 && Number(parsedDate[property] !== 'NaN') &&
              (parsedDate[property] = `${String(datetime.year).substr(0,2)}${parsedDate[property]}`);
              break;

            case 'month':
              switch (isNaN(parsedDate[property])) {
                // month is a named string rather than number
                case true:
                  const names = [ 'jan', 'feb', 'mar', 'apr', 'may', 'jun',
                    'jul', 'aug', 'sep', 'oct', 'nov', 'dec'
                  ];

                  parsedDate[property] = names.findIndex((month) => {
                    return String(parsedDate[property])
                    .toLowerCase()
                    .startsWith(month);
                  }) + 1;
                  // 0, 1-12
                  // 0 will be removed later
                  break;

                // month is a number, check for invalid entry
                case false:
                  const number = Number(parsedDate[property]);

                  ((number < 1) || (number > 12)) &&
                  (parsedDate[property] = 0);
                  // this will be removed later
                  break;
              }
              break;

            case 'date':
              switch (isNaN(parsedDate[property])) {
                // date is a named string rather than number
                case true:
                  parsedDate[property] = 0;
                  break;

                // date is a number, check for invalid entry
                case false:
                  const number = Number(parsedDate[property]);

                  ((number < 1) || (number > 31)) &&
                  (parsedDate[property] = 0);
                  // this will be removed later
                  break;
              }
              break;
          }

          // remove empty or invalid properties
          if (!parsedDate[property]) {
            delete parsedDate[property];
            return;
          }
          
          // cast to number to number
          parsedDate[property] = Number(parsedDate[property]);
        });

        break;
      }
    }

    // handle time portion

    const timePatterns = [
      // 14:30:23 || 14:30 || 2:30pm
      /(?<hour>[0-9]{1,2})(?:[.:]{1})(?<minutes>[0-9]{1,2})(?:[.:]{1})?(?<seconds>[0-9]{1,2})?(?:[/s]{1})?(?<meridiem>am|pm|AM|PM{2})?/,

      // 2pm || 2 pm
      /(?<hour>[0-9]{1,2})(?:[/s]{1})?(?<meridiem>am|pm|AM|PM{2})/,
    ];

    for (const pattern of timePatterns) {
      if (pattern.test(string)) {
        const output = '{"hour":"$<hour>","minutes":"$<minutes>","seconds":"$<seconds>","meridiem":"$<meridiem>"}';
        parsedTime = matchAndParseJson(string, pattern, output);

        // check and manage parsed time object
        Object.keys(parsedTime).forEach((property) => {
          switch (property) {
            case 'hour':
              switch (isNaN(parsedTime[property])) {
                // hour is a named string rather than number
                case true:
                  parsedDate[property] = 0;
                  break;

                // hours is a number, check for invalid entry
                case false:
                  const number = Number(parsedTime[property]);

                  if ((number < 0) || (number > 23)) {
                    (parsedTime[property] = 0);
                    // this will be removed later
                    break;
                  }

                  if (parsedTime.hasOwnProperty('meridiem')) {
                    switch(parsedTime.meridiem.toLowerCase()) {
                      case 'am':
                        (number > 12) && (parsedTime[property] = (number - 12));
                        break;

                      case 'pm':
                        (number < 12) && (parsedTime[property] = (number + 12));
                        break;
                    };
                  }

                  break;
              }
              break;

            case 'minutes':
            case 'seconds':
              switch (isNaN(parsedTime[property])) {
                // minutes or seconds is a named string rather than number
                case true:
                  parsedTime[property] = 0;
                  break;

                // minutes or seconds is a number, check for invalid entry
                case false:
                  const number = Number(parsedTime[property]);

                  ((number < 0) || (number > 59)) &&
                  (parsedTime[property] = 0);
                  // this will be removed later
                  break;
              }
              break;
          }
          
          // cast to number to number
          parsedTime[property] = Number(parsedTime[property]);
        });

        break;
      }
    }

    return {...datetime, ...parsedDate, ...parsedTime};
  };

  /**
   * Replaces Date.toString() method
   * @return  {string}  yyyy-mm-dd hh:mm
   */
  toString = () => {
    const date= this.getDate();
    const month = this.getMonth();
    const year = this.getFullYear();
    const hour = this.getHours();
    const minutes = this.getMinutes();

    const pad = int => String(int).padStart(2, '0');

    return `${year}-${pad(month)}-${pad(date)} ${pad(hour)}:${pad(minutes)}`;
  }

  /**
   * Replaces Date.toJSON() method
   * @return  {string}
   */
  toJSON = () => this.toString();

  /**
   * Returns the English text of the month
   * @param  {bool=false}  [long]  full text or shortened to 3 characters
   *
   * @return  {string}
   */
  getVerboseMonth = (long = false) => {
    const names = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ];

    const month = names[this.getMonth()];

    return long && month || month.slice(0, 3);
  }

  /**
   * Returns the English text of the day of the week
   * @param  {bool=false}  [long]  full text or shortened to 3 characters
   *
   * @return  {string}
   */
  getVerboseDay = (long = false) => {
    const names = [
      'Sunday',
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday',
    ];

    const day = names[this.getDay()];

    return long && day || day.slice(0, 3);
  }
}

const DatePicker = memo((props) => {
  const {
    // operational props
    name = 'datepicker',
    label,
    defaultValue,
    callback,

    // functional props
    setTime = false,
  } = props;

  /** States and ref -------------------------------------------------------- */
  const [ datetime, setDateTime ] = useState(new Date(defaultValue));
  const value = useRef(defaultValue);

  const wrapperRef = useRef();

  /** Callback handling ----------------------------------------------------- */

  const invokeCallback = () => {
    const data = {
      name,
      value: datetime.current.getTime(),  // milliseconds since epoch
      valid: true,
    };

    callback && callback(data);
  };

  /** Click handling -------------------------------------------------------- */

  const onClickDateIcon = () => {
    wrapperRef.current.classList.toggle('open');
  };

  /** Key entry handling ---------------------------------------------------- */

  /**
   * @param  {Event}  event
   */
  const onBlur = (event) => {
    const {
      target: { value: newValue }
    } = event;

    setDateTime(new Date(newValue));

    invokeCallback();
  };

  /** Rendering ------------------------------------------------------------- */

  const renderDateTimeString = () => {
    // check width of element which will determine how we are rendering
    const width = wrapperRef.current.offsetWidth;
    //!
    console.log('datetime width', width);

    const d = datetime.current.getDate();
    const m = datetime.current.getMonth();
    const yyyy = datetime.current.getFullYear();

    const hour = datetime.current.getHours();
    //! -- here

    const string = ``;
  };

  const comboProps = {
    name,
    ref: wrapperRef,
    onClick: onClickSelect
  };

  const inputProps = {
    onBlur,
    onChange,
  };

  return (
    <Wrapper>
      { label && <Label>{label}</Label> }
      <ComboBox {...comboProps} >
        <Input/>
        <Button onClick={ onClickDateIcon } ><DateIcon/></Button>
        <PickerWrapper>
          <PickDate/>
          <TimePicker/>
        </PickerWrapper>
      </ComboBox>
    </Wrapper>
  );
});

export {
  DatePicker as default
};