/**
 * Styling for PaymentCard
 * Takes the appearance of an actual credit card
 * 
 * @export  payment  
 * { Wrapper, Label, ErrorMessage, Logo, Name, Number, Expiry, Cvc, CardName }
 */
import styled, { css } from 'styled-components';
import { responsive } from './mixins';

export const payment = {};

/* payment.Wrapper is at the bottom */

payment.ErrorMessage = styled.div`
  visibility: hidden;
  font-family: var(--theme-font-paymentcard-message);
  font-weight: 500;
  font-size: 0.64rem;
  text-transform: none;
  white-space: nowrap;
  text-overflow: ellipsis;
  
  color: var(--theme-color-paymentcard-error);
  position: absolute;
  bottom: -1.2rem;
  height: 1em;
  line-height: 1em;
`;

payment.Logo = styled.svg`
  display: block;
  height: 2em;
  width: auto;
`;

payment.Name = styled.input`
  display: block;
  width: 100%;
`;

payment.Number = styled.input`
  display: block;
  width: 100%;
`;

payment.Expiry = styled.input`
  display: block;
  width: 8ch;
`;

payment.Cvc = styled.input`
  display: block;
  width: 5ch;
`;

payment.CardName = styled.div`
  display: block;
  width: 100%;
  font-family: inherit;
  font-size: 1.2em;
  height: 1.6em;
  line-height: 1.6em;
  color: inherit;
`;

payment.Wrapper = styled.div`
  --font-size: 16px; 
  --border-color: transparent;
  --card-color: var(--theme-color-paymentcard-base);

  font-size: var(--font-size);
  border-radius: 0.8em;
  height: 8.8em;
  width: 16em;
  padding: 0.6em;
  box-sizing: content-box;
  margin-top: 1.5em;
  margin-bottom: 2em;
  position: relative;
  transition: all 0.2s ease;

  color: var(--theme-color-paymentcard-foreground);
  border: 2px var(--border-color) solid;
  background-color: var(--card-color);

  &[data-card-type="visa"] {
    --border-color: transparent;
    --card-color: var(--theme-color-paymentcard-visa);
  }

  &[data-card-type="mastercard"] {
    --border-color: transparent;
    --card-color: var(--theme-color-paymentcard-mastercard);
  }

  &[data-card-type="amex"] {
    --border-color: transparent;
    --card-color: var(--theme-color-paymentcard-amex);
  }

  &[data-card-type="dinersclub"] {
    --border-color: transparent;
    --card-color: var(--theme-color-paymentcard-dinersclub);
  }

  &[data-card-type="unionpay"] {
    --border-color: transparent;
    --card-color: var(--theme-color-paymentcard-unionpay);
  }

  &.center {
    margin-left: auto;
    margin-right: auto;
  }

  &.focus {
    --border-color: var(--theme-color-paymentcard-accent);
  }

  &.error {
    --border-color: var(--theme-color-paymentcard-error);

    ${payment.ErrorMessage} {
      visibility: visible;
    }
  }

  & input {
    font-family: var(--theme-font-paymentcard);
    font-size: inherit;

    height: 1.2em;
    line-height: 1.2em;
    border: 0;
    transition: background-color 0.2s ease;
    color: var(--theme-color-paymentcard-input-foreground);
    background-color: var(--theme-color-paymentcard-input-background);
    border-radius: 3px;

    &:focus {
      border: 0;
      outline: 0;
      background-color: var(--theme-color-paymentcard-input-background-focused);
    }

    &::placeholder {
      color: var(--theme-color-paymentcard-input-placeholder-foreground);
    }
  }

  ${responsive.tablet(css`
    --font-size: 20px;
  `)}
`;