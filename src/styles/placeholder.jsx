/**
 * Styles for Placeholder
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @export  placeholder  
 * { wrapper, status }
 */
import styled, { keyframes } from 'styled-components';

const placeholder = {};

const shimmer = keyframes`
  0% {
    background-position: 90% 90%;
  }
  40% {
    background-position: 10% 10%;
  }
  100% {
    background-position: 10% 10%;
  }
`;

const loading = keyframes`
  0%    { content: ''; }
  25%   { content: '·'; }
  50%   { content: '··'; }
  75%   { content: '···'; }
  100%  { content: '···'; }
`;

placeholder.status = styled.div`
  font-family: var(--theme-font-markdown-loading);
  font-size: 0.8em;
  line-height: 1.2em;
  letter-spacing: 0.3ch;
  text-transform: uppercase;
  width: auto;
  flex-grow: 1;
  white-space: pre-wrap;

  &:after {
    content: '·';
    display: inline-block;
    padding-left: 1em;
    animation: ${loading} 2s linear infinite;
  }
`;

placeholder.wrapper = styled.div`
  width: ${props => props.width || '100%'};
  height: ${props => props.height || 'auto'};
  border-radius: 6px;
  animation: ${shimmer} 2s linear infinite;
  background: linear-gradient(
    135deg, 
    transparent 0%, 
    transparent 40%, 
    var(--theme-color-placeholder-loading-pulse) 50%, 
    transparent 60%, 
    transparent 100%
  );
  background-size: 300% 300%;

  box-sizing: border-box;
  padding: 1.2em 1em;

  &.error {
    animation: initial;
    background: var(--theme-color-placeholder-loading-error);
    background-size: 100%;

    ${placeholder.status} {
      color: var(--theme-color-red); 

      &:after {
        content: '';
        animation: initial;
      }
    }
  }
`;

export {
  placeholder
};