/**
 * Styling the ComingSoon content
 * @author Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @export comingSoon 
 * { wrapper, logo, message }
*/
import styled, { css } from 'styled-components';
import { flexbox } from './mixins';

export const comingSoon = {};

/**
 * @param  {css}  --control-height
 */
comingSoon.wrapper = styled.div`
  display: block;
  position: relative;
  ${flexbox({direction: 'column', xAlign: 'center'})}
  overflow: hidden;
  margin-bottom: calc(
    var(--control-height, 3em) - var(--padding-y, 1em)
  ) !important;
`;

comingSoon.logo = styled.div`
  overflow: hidden;
  font-size: 8vh;
  line-height: 8vh;
  color: var(--theme-color-foreground);
  padding: 0 0 2vh 0;
  width: initial;

  // react-icons
  & svg {
    display: block;
    margin: auto;
  }
`;

comingSoon.message = styled.div`
  font-size: 0.6rem;
  font-family: var(--theme-font-monospace);
  text-overflow: ellipsis;
  white-space: pre-wrap;
  overflow: hidden;
  text-transform: uppercase;
  text-align: center;
  color: var(--theme-color-foreground);
  padding: 0;
  background-color: transparent;
  width: initial;
  max-width: 80%;
`;