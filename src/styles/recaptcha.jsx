/**
 * Styles for reCAPTCHA elements 
 * @author  Johnson Zhou  <johnson@simplyuseful.io> 
 * 
 * @export v2  
 * {  }
 */

import styled from 'styled-components';
import { placeholder } from './placeholder';

const v2 = {};

v2.Wrapper = styled.div`
  --width: 304px;
  --height: 78px;
  width: var(--width);
  height: var(--height);
  position: relative;
  overflow: hidden;

  ${placeholder.wrapper} {
    position: absolute;
    top: 0;
    left: 0;
  }

  & .widget {
    width: auto;
    height: auto;
  }

  &.compact {
    --width: 158px;
    --height: 138px;
  }
`;

export {
  v2
};