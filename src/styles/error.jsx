/**
 * Styling for ErrorBoundary
 * 
 * @author Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @export error { wrapper, message }
*/
import styled from 'styled-components';

export const error = {};

error.wrapper = styled.div`
  display: inline-block;
  height: 100%;
  width: 100%;
  background: repeating-linear-gradient(
    45deg,
    var(--theme-color-red, red),
    var(--theme-color-red, red) 2px,
    var(--theme-color-background, white) 2px,
    var(--theme-color-background, white) 18px 
  );
  position: relative;
  min-height: 1.6rem;
`;

error.message = styled.div`
  font-size: 0.6rem;
  font-family: var(--theme-font-monospace, monospace);
  text-overflow: ellipsis;
  white-space: pre-wrap;
  overflow: hidden;
  text-transform: uppercase;
  text-align: center;
  display: block;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translateY(-50%) translateX(-50%);
  overflow: hidden;
  color: var(--theme-color-red, red);
  padding: 0.2rem;
  background-color: var(--theme-color-background, white);
  width: initial;
`;