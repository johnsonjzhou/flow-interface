/**
 * Styles for TextField
 * @author Johnson Zhou <johnson@simplepharmacy.io>
 * 
 * @export textfield 
 * { wrapper, input, textarea, label, feedback }
*/

import styled from 'styled-components';
import { flexbox } from './mixins';

export const textfield = {};

// textfield.Wrapper is at the bottom as it references other styles

textfield.Input = styled.input`
  /* common input/textarea styling in textfield.Wrapper */
  --line-height: var(--height);
  height: var(--height-input);
  white-space: nowrap;
  text-overflow: ellipsis;
`;

textfield.Textarea = styled.textarea`
  /* common input/textarea styling in textfield.Wrapper */
  --line-height: 1.3em;
  --padding-y-adjust: calc(0.5 * (var(--height) - var(--line-height)));
  height: var(--height-textarea);
  resize: none;
  white-space: pre-wrap;
`;

textfield.Label = styled.div`
  position: absolute;
  width: auto;
  padding: 0 0.3em;
  height: var(--height-label);
  line-height: var(--height-label);
  top: 0.4em;
  left: 0.75em;

  font-family: var(--theme-font-input-label);
  font-weight: 600;
  font-size: 0.8em;
  letter-spacing: 0.3ch;
  text-transform: uppercase;
  white-space: nowrap;
  text-overflow: ellipsis;
  color: var(--label-color-foreground);
  background-color: var(--label-color-background);
  border: 0;
  border-radius: 3px 3px 0 0;
  transition: all 0.1s ease;
`;

textfield.Feedback = styled.div`
  height: 1em;
  line-height: 1em;
  color: var(--feedback-color);
  position: absolute;
  left: calc(100% + 0.3em);
  top: 50%;
  transform: translateY(-50%);
  ${flexbox({
    direction: 'row', 
    xAlign: 'left', 
    yAlign: 'center'
  })}

  font-family: var(--theme-font-input-label);
  font-weight: 500;
  font-size: 0.8em;
  letter-spacing: 0;
  text-transform: none;
  white-space: nowrap;
  text-overflow: ellipsis;

  & svg {
    display: block;
    margin-right: 0.3em;
    flex-shrink: 0;
  }
`;

/**
 * @param  {string}  className - focus, error
 * @param  {css}  --height-wrapper - set by component when resize
 * @param  {css}  --height-textarea - set by component when resize
 */
textfield.Wrapper = styled.div`
  /* settings */
  --height: 2em;
  --height-label: 1.5em;
  --height-wrapper: auto;                                  // settable when resize
  --height-textarea: calc(var(--height) - var(--border));  // settable when resize
  --height-input: calc(var(--height) - var(--border));
  --width: calc(100% - (2 * (var(--padding-x) + var(--border))));
  --padding-top: 1.5em;
  --padding-y: 0.5em;
  --padding-x: 0.75em;
  --border: 2px; 
  --border-color: var(--theme-color-input-accent);
  --font-size: 1em; 
  --label-color-foreground: var(--theme-color-input-label-foreground);
  --label-color-background: var(--theme-color-input-label-background);
  --feedback-color: var(--theme-color-darkgrey);

  position: relative;
  box-sizing: border-box;
  padding: var(--padding-top) 0 0 0;
  margin: 0 0 2em 0;
  width: 100%;
  height: var(--height-wrapper);
  overflow: visible;
  letter-spacing: 0;
  font-size: var(--font-size);

  &.focus {
    --border-color: var(--theme-color-input-accent);
    --label-color-foreground: var(--theme-color-input-label-foreground-focused);
    --label-color-background: var(--theme-color-input-label-background-focused);
  }

  &.error {
    --border-color: var(--theme-color-input-error);
    --label-color-foreground: var(--theme-color-input-label-foreground-error);
    --label-color-background: var(--theme-color-input-label-background-error);
    --theme-color-input-background: 
      var(--theme-color-input-background-error);

    ${textfield.Feedback} {
      --feedback-color: var(--theme-color-input-error);
    }
  }

  ${textfield.Input}, ${textfield.Textarea} {
    color: var(--theme-color-input-foreground);
    background-color: var(--theme-color-input-background);
    border-radius: 6px;
    width: var(--width);
    transition: border 0.1s ease;
    border: solid var(--border) transparent;
    margin: 0;

    font-family: var(--theme-font-input);
    font-size: 1em;
    letter-spacing: 0;
    text-transform: none;

    line-height: var(--line-height);
    padding: 
      calc(var(--padding-y) + var(--padding-y-adjust, 0px)) 
      var(--padding-x) 
      calc(var(--padding-y) - (0.4 * var(--padding-y-adjust, 0px))) 
      var(--padding-x)
    ;

    /* user agent */
    box-shadow: none; /* FF */

    &:focus {
      background-color: var(--theme-color-input-background-focused);
      border: solid var(--border) var(--border-color);
      outline: 0;
    }
    
    &:disabled {
      cursor: default;
      color: var(--theme-color-input-disabled);
    }
  }
`;