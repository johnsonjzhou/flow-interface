/**
 * Styles for Flow Interface
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 */
import styled, { css } from 'styled-components';
import { responsive } from './mixins';
import { menu } from './menu';
import { stack } from './stack';

export const ui = {};

/**
 * @note  overwrite the perspective-origin property with the Perspective3D
 * module to create perspective changes
 * 
 * @note  class show-group - whether or not to show the group background
 * @note  var(--menu-group-background) - describes group background to apply
 */
ui.wrapper = styled.section`
  width: 100%;
  height: 100%;
  background: var(--theme-interface-background);
  transition: filter 0.2s ease;

  /* menu group background and transition */
  :before {
    content: '';
    display: block;
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    transform: translateX(-100%);
    transform-origin: left;
    background: var(--menu-group-background, transparent);
    transition: transform 0.3s cubic-bezier(0,.67,.05,.78);
  }

  &.show-group:before {
    transform: translateX(0);
  }

  /** layer depth (--d) */

  > :nth-last-child(11) { --d: 9; }
  > :nth-last-child(10) { --d: 8; }
  > :nth-last-child(9) { --d: 7; }
  > :nth-last-child(8) { --d: 6; }
  > :nth-last-child(7) { --d: 5; }
  > :nth-last-child(6) { --d: 4; }
  > :nth-last-child(5) { --d: 3; }
  > :nth-last-child(4) { --d: 2; }
  > :nth-last-child(3) { --d: 1; }
  > :nth-last-child(2) { --d: 0; }
  > :nth-last-child(1) { --d: -1; }

  /* experimental stacks style that shifts the stacks perspective */
  /* WebKit only, does not work very well on Firefox */
  &[data-interface-style="stacks-perspective"] {

    ${menu.wrapper} {
      --menu-translate-z: calc(-5px * var(--d, 0));
      --menu-blur: calc(0.5px * var(--d, 0));

      --t: 0.08s;
      transition: 
        transform var(--t) linear, 
        filter var(--t) linear
      ;
      transform: translate3d(0, 0, var(--menu-translate-z));
      filter: blur(var(--menu-blur));
      transform-style: flat;
    }

    ${stack.modal} {
      --theme-color-modal-background: var(--theme-color-background-stacks);
    }

    ${stack.wrapper} {
      --t: 0.1s;
      --stack-shift: calc(-12px * var(--d));
      --filter-blur: calc(0.25px * var(--d));

      transform: 
        translate3d(var(--stack-shift), 0, var(--stack-shift)) 
        scale3d(1, 1, 1)
      ;
      transition: transform var(--t) linear;
      filter: blur(var(--stack-blur));
    }

    transform-style: flat;

    perspective: 500px;
    perspective-origin: 0% 30%;

    ${responsive.tablet(css`
      perspective-origin: 0% 30%;
    `)}

    ${responsive.desktop(css`
      perspective-origin: 30% 40%;
    `)}

    ${responsive.ultrawide(css`
      perspective-origin: 40% 40%;
    `)}
  }

  /* default stacks style */
  &[data-interface-style="stacks"] {
    
    ${menu.wrapper} {
      --t: 0.08s;
      --menu-shift: calc(-6px * var(--d));
      --menu-scale: calc(1 - (0.006 * var(--d)));
      --menu-blur: calc(0.5px * var(--d, 0));
      transition: 
        transform var(--t) linear, 
        filter var(--t) linear
      ;
      transform: 
        translate3d(var(--menu-shift), 0, 0) 
        scale3d(var(--menu-scale), var(--menu-scale), 1)
      ;
      filter: blur(var(--menu-blur));
      transform-style: flat;
    }

    ${stack.modal} {
      --theme-color-modal-background: var(--theme-color-background-stacks);
    }

    ${stack.wrapper} {
      --t: 0.1s;
      --stack-shift: calc(-18px * var(--d));
      --stack-scale: calc(1 - (0.02 * var(--d)));
      transform: 
        translate3d(var(--stack-shift), 0, 0) 
        scale3d(var(--stack-scale), var(--stack-scale), 1)
      ;
      transition: transform var(--t) linear;
    }
  }

  /* stacks style without transitions */
  /* for older systems that struggle with GPU transitions */
  &[data-interface-style="stacks-performance"] {
    
    ${menu.wrapper} {
      --menu-shift: calc(-6px * var(--d));
      --menu-scale: calc(1 - (0.006 * var(--d)));
      --menu-blur: calc(4px * (var(--d) / var(--d)));
      transform: 
        translate(var(--menu-shift), 0) 
        scale(var(--menu-scale), var(--menu-scale))
      ;
      filter: blur(var(--menu-blur));
    }

    ${stack.modal} {
      --theme-color-modal-background: var(--theme-color-background-stacks);
    }

    ${stack.wrapper} {
      --stack-shift: calc(-18px * var(--d));
      --stack-scale: calc(1 - (0.02 * var(--d)));
      transform: 
        translate(var(--stack-shift), 0) 
        scale(var(--stack-scale), var(--stack-scale))
      ;
    }
  }

  /* monochrome style inspired by the TV show "3%" */
  /* this is the default style */
  &[data-interface-style="mono"] {
    
    ${menu.wrapper} {
      --t: 0.08s;
      --menu-shift: -24px;
      --menu-blur: 6px;
      --menu-overlay-bg: var(--theme-color-background-mono);
      --menu-overlay-opacity: 1;

      &:nth-last-child(2), &:nth-last-child(1) { 
        --menu-shift: 0px;
        --menu-blur: 0px;
        --menu-overlay-opacity: 0;
        &:after {
          display: none;
        }
      }

      > * {
        transform: translate3d(var(--menu-shift), 0, 0);
        filter: blur(var(--menu-blur));
        transition: 
          transform var(--t) linear, 
          filter var(--t) linear
        ;
        transform-style: flat;
      }

      &:after {
        content: '';
        display: block;
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
      }

      &:after {
        background-color: var(--menu-overlay-bg);
        opacity: var(--menu-overlay-opacity);
        transition: opacity var(--t) linear;
      }

      @supports (backdrop-filter: blur(6px)) {
        > * {
          filter: initial;
        }

        &:after {
          backdrop-filter: blur(var(--menu-blur));
          transition: backdrop-filter var(--t) linear;
        }
      }
    }

    ${stack.wrapper} {
      --t: 0.1s;
      --stack-shift: -72px;
      --stack-opacity: 0;

      &:nth-last-child(2) { 
        --stack-shift: 0px;
        --stack-opacity: 1;
      }

      &:nth-last-child(1) { 
        --stack-shift: 72px;
      }

      transform: translate3d(var(--stack-shift), 0, 0);
      opacity: var(--stack-opacity);
      
      transition: 
        transform var(--t) linear, 
        opacity var(--t) linear
      ;

      &.no-controls {
        & ${stack.modal} {
          height: var(--height, auto);
        }
      }
    }

    ${stack.modal} {
      --theme-color-modal-background: transparent;
      box-shadow: initial;
    }
  }

  /* apply a greyscale filter if we are offline */
  &[data-online="false"] {
    filter: grayscale(0.8);
  }
`;