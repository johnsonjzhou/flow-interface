/**
 * Styling for selectors (checkbox and radio), toggles and select box
 * @author Johnson Zhou <johnson@simplepharmacy.io>
 * 
 * @export selector - serves both Checkbox and RadioGroup
 * { Wrapper } 
 * 
 * @export toggle {} - ( )== -> ==( )
 * 
 * @export selectbox 
 * { Wrapper }
*/

import styled, { css } from 'styled-components';
import { flexbox } from './mixins';
import { textfield } from './textfield';

export const selector = {};
export const toggle = {};
export const selectbox = {};

// ----- selector (checkbox or radio depends on Icon) ----- /
// <Wrapper>
//    <input/>
//    <label>
//      <div class='icon'><Icon (svg)/></div>
//      <span>Text</span>
//    </label>
// </Wrapper>

selector.Wrapper = styled.div`
  --font-size: 1em;
  --icon-size: 1.4em;
  --hover-size: 3em;

  padding: 0;
  overflow: visible;

  font-size: var(--font-size);
  font-family: inherit;

  & input {
    display: none;

    &:checked, &:indeterminate {
      & + label {
        --color: var(--theme-color-selector-active);
      }
    }

    &:disabled {
      & + label {
        --color: var(--theme-color-selector-inactive);
        cursor: not-allowed;
      }
    }
  }

  & label {
    --color: var(--theme-color-selector-inactive);

    text-align: left;
    margin-bottom: 0;
    position: relative;
    font-size: inherit;
    padding: calc(var(--hover-size) / 3) 0;
    width: 100%;
    color: var(--color);
    cursor: pointer;
    ${flexbox({
      direction: 'row', 
      xAlign: 'left', 
      yAlign: 'center'
    })}

    & div.icon {
      display: block;
      height: auto;
      width: auto;
      margin-left: calc((var(--hover-size) - var(--icon-size)) / 2);
      margin-right: 0.3em;
      align-self: flex-start;
      flex-shrink: 0;

      &:before {
        content: '';
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%) scale(0, 0);
        width: var(--hover-size);
        height: var(--hover-size);
        border-radius: var(--hover-size);
        border: 0;
        background-color: var(--theme-color-selector-hover-background);
        filter: opacity(0.1);
        transition: all 0.2s ease;
      }

      & svg {
        display: block;
        flex-shrink: 0;
        height: var(--icon-size);
        width: var(--icon-size);
      }
    }

    & span {
      display: block;
      flex-grow: 1;
      letter-spacing: 0;
      text-transform: none;
      white-space: wrap;
      line-height: 1.1em;
    }

    &:hover {
      & div.icon {
        &:before {
          transform: translate(-50%, -50%) scale(1, 1);
        }
      }
    }
  }
`;

// ----- toggle ----- /
// <Wrapper>
//   <Label>
//     text 
//   </Label>
//   <Switch>
//     ::before - track
//     ::after - toggle slider
//   </Switch>
// </Wrapper>

toggle.Label = styled.span`
  display: block;
  text-align: left; 
  margin-bottom: 0; 
  font-size: inherit; 
  color: var(--label-color); 
  white-space: nowrap; 
  text-overflow: ellipsis; 
  height: 100%;
  flex-grow: 1;
  transition: color 0.3s ease;
`;

/**
 * div - wrapper for the toggle
 * ::after - the actual toggle switch
 * ::before - the track for the sliding toggle
 */
toggle.Switch = styled.div`
  display: block;
  position: relative;
  width: var(--track-width);
  height: 100%;
  min-height: var(--toggle-size);
  flex-shrink: 0;
  margin-left: calc((var(--hover-size) - var(--track-width)) / 2);
  margin-right: 0.6em;
  filter: opacity(0.3);
  transition: filter 0.3s ease;

  &:before, &:after {
    display: block;
    content: "";
    position: absolute;
    top: 50%;
    transition: 
      left 0.3s ease, 
      background-color 0.3s ease, 
      filter 0.3s ease,
      box-shadow 0.1s ease;
    transform: translateY(-50%);
  }

  /* toggle switch */
  &:after {
    height: var(--toggle-size);
    width: var(--toggle-size);
    border-radius: var(--toggle-size);
    border: 0;
    background-color: var(--toggle-color);
    left: var(--toggle-pos);
  }

  /* sliding track */
  &:before {
    width: 100%;
    height: var(--track-height);
    border-radius: var(--track-height);
    background-color: var(--toggle-color);
    filter: opacity(0.5);
  }
  &:focus {
    outline: none;
  }
`;

toggle.Wrapper = styled.div`
  --font-size: 1em;
  --toggle-size: 1.4em;
  --track-width: 2.2em;
  --track-height: 0.8em;
  --height: 3em;
  --toggle-color: var(--theme-color-toggle-switch-inactive);
  --toggle-pos: 0;
  --label-color: var(--theme-color-toggle-label-inactive);
  --hover-size: 3em;

  text-align: left;
  font-size: var(--font-size);
  padding: calc(var(--hover-size) / 3) 0;
  width: 100%;
  cursor: pointer;
  ${flexbox({
    direction: 'row', 
    xAlign: 'left', 
    yAlign: 'center'
  })}

  &:before {
    content: '';
    position: absolute;
    top: 50%;
    left: calc(var(--hover-size) / 2);
    transform: translate(-50%, -50%) scale(0, 0);
    width: var(--hover-size);
    height: var(--hover-size);
    border-radius: var(--hover-size);
    border: 0;
    background-color: var(--theme-color-toggle-hover-background);
    filter: opacity(0.1);
    transition: all 0.2s ease;
  }

  &:hover {
    &:before {
      transform: translate(-50%, -50%) scale(1, 1);
    }
  }

  &.active {
    --toggle-color: var(--theme-color-toggle-switch-active);
    --toggle-pos: calc(var(--track-width) - var(--toggle-size));
    --label-color: var(--theme-color-toggle-label-active);

    ${toggle.Switch} {
      filter: opacity(1);
    }
  }
`;

// ----- selectbox ----- /

selectbox.OptLabel = css`
  font-family: var(--theme-font-sans-serif);
  font-size: 0.8em;
  font-weight: 700;
  font-style: normal;
  text-transform: uppercase;
  letter-spacing: 0.3ch;
  overflow: hidden;
  word-wrap: nowrap;
  text-overflow: ellipsis;
`;

selectbox.Option = styled.div`
  position: relative;
  font-family: var(--theme-font-selectbox);
  font-size: 1em;
  background-color: transparent;
  padding: 0.25em 0;
  overflow: hidden;
  word-wrap: nowrap;
  text-overflow: ellipsis;

  &.label {
    ${selectbox.OptLabel}
  }

  &.hide {
    display: none;
  }

  &:hover, &.selected {
    color: var(--theme-color-selectbox-accent-contrast);
    background-color: var(--theme-color-selectbox-accent);
  }
`;

selectbox.OptGroup = styled.optgroup`
  font-size: 1em;
  padding: 0;

  &:before {
    ${selectbox.OptLabel}
    padding: 0.5em 0 0.25em 0;
  }
`;

selectbox.Button = styled.div`
  position: absolute;
  right: 0;
  top: 0;
  height: 100%;
  width: var(--width-button);
  color: var(--theme-color-selectbox-accent);
  background-color: transparent;
  transition: background-color 0.2s ease;
  font-size: 1em;
  ${flexbox({ direction: 'row', xAlign: 'center' })}

  & svg {
    display: block;
  }
`;

selectbox.DefaultOption = styled.span`
  display: inline;
  font-size: 1em;
  cursor: pointer;
`;

selectbox.OptionList = styled.div`
  width: 100%;
  height: auto;
  max-height: var(--height-option-list);
  overflow-x: hidden;
  overflow-y: auto;
  padding-top: calc(2 * var(--padding-y)); 
  position: relative; 

  :before {
    display: block;
    content: '';
    width: 100%;
    height: 1px;
    border-top: solid 1px var(--theme-color-selectbox-foreground);
    opacity: 30%;

    position: absolute;
    left: 0;
    top: var(--padding-y);
  }
`;

selectbox.Label = styled(textfield.Label)`
  font-family: var(--theme-font-selectbox-label);
`;

selectbox.Select = styled.div`
  position: relative;
  min-height: var(--height-selector);
  max-height: var(--height-selector);
  line-height: var(--height);
  width: var(--width);
  padding: 
    var(--padding-y) 
    calc(var(--padding-x) + var(--width-button)) 
    var(--padding-y) 
    var(--padding-x) 
  ;
  border: solid var(--border) transparent;
  border-radius: 6px;
  font-family: var(--font);
  font-size: var(--font-size);
  font-weight: 400;
  color: var(--foreground);
  background-color: var(--background);
  appearance: none;
  overflow: hidden;
  cursor: pointer;
  transition: 
    border 0.2s ease, 
    background-color 0.2s ease, 
    max-height 0.2s ease
  ;

  &:hover, &:focus, &.open {
    border: solid var(--border) var(--border-color);

    ${selectbox.Button} {
      color: var(--theme-color-selectbox-accent-contrast);
      background-color: var(--theme-color-selectbox-accent);
    }
  }

  &:focus, &.open {
    background-color: var(--theme-color-selectbox-background-open);
  }

  &.open {
    max-height: calc(var(--height-selector) + var(--height-option-list));
  }
`;

selectbox.Wrapper = styled.div`
  --height: 2em;
  --height-label: 1.5em;
  --padding-y: 0.5em;
  --padding-x: 0.75em;
  --border: 2px; 
  --border-color: var(--theme-color-selectbox-accent);
  --width-button: 1.5em;
  --width: calc(100% - (2 * (var(--padding-x) + var(--border))) - var(--width-button));
  --height-selector: calc(var(--height) - var(--border));
  --height-option-list: 50vh;
  --font-size: 1em; 
  --font: var(--theme-font-selectbox);
  --foreground: var(--theme-color-selectbox-foreground);
  --background: var(--theme-color-selectbox-background);
  --label-color-foreground: var(--theme-color-input-label-foreground);
  --label-color-background: var(--theme-color-input-label-background);

  padding: 1.5em 0 0 0;
  margin: 0 0 2em 0;
`;
