/**
 * Styling for the Stack modal
 * @author Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @export stack
 * { wrapper, overlay, control, modal, content, ajax }
 * 
*/
import styled, { css } from 'styled-components';
import { flexbox, responsive } from './mixins';

const stack = {};

/**
 * @note  stack.wrapper is declared at the end,
 * to capture references to other styles
 */

/**
 * ! Deprecated 
 * Overlay to cover the screen directly beneath the content
 */
stack.overlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  background-color: transparent;
  border: 0;
`;

/**
 * Control bar for close button, quick action buttons, etc
 * @var  {string}  --control-height
 *  - height of the control element, set in content.wrapper
 */
stack.control = styled.div`
  position: absolute;
  width: 100%;
  height: var(--control-height, 3em);
  left: 0;
  top: 0;
  border: 0;
  ${flexbox({
    direction: 'row',
    xAlign: 'left',
    yAlign: 'center',
    wrap: 'nowrap'
  })}
  background-color: transparent;

  /** quick action buttons */
  & > * {
    margin-right: 0.3em;

    /** modal control */
    &:first-child {
      margin-left: 0.4em;
      margin-right: 0.2em;
    }

    /** separator line */
    &:nth-child(2) {
      margin-right: 0.5em;
    }
  }

`;

/**
 * @var  {int}  --height - fix the height in desktop
 */
stack.modal = styled.div`
  /* control height, used by stack.content and stack.control */
  --control-height: 3em;

  position: relative;
  background-color: var(--theme-color-modal-background, white);
  padding: 0;
  box-sizing: border-box;
  margin-left: auto;
  border: 0;
  width: calc(100vw + var(--mobile-padding-right,0px));
  border-radius: 6px 0 0 6px;
  overflow: hidden;
  height: 100%;
  box-shadow: -6px 0px 8px 0 var(--theme-color-shadow, darkgrey);

  ${responsive.mobileLandscape(css`
    max-width: calc(700px + var(--mobile-padding-right, 0px));
  `)}

  ${responsive.tablet(css`
    max-width: calc(700px + var(--mobile-padding-right, 0px));
  `)}

  ${responsive.desktop(css`
    border-radius: 6px;
    width: 800px;
    margin-right: calc(50vw - 420px);
    height: var(--height, 96%);
    box-shadow: 0px 20px 32px -8px var(--theme-color-shadow, darkgrey);
  `)}

  ${responsive.ultrawide(css`
    margin-right: calc(50vw - 420px);
  `)}
`;

/**
 * Main content area for the modal
 * todo: implement <Scrollable> in the future
 * but it's not working well right now
 * @var  {string}  --control-height
 *  - height of the top margin, set by content.wrapper
 */
stack.content = styled.div`
  font-size: 1em;
  line-height: 1.1em;
  height: calc(100% - var(--control-height, 3em));
  width: 100%;
  overflow-y: auto;
  overflow-x: hidden;
  position: relative;
  filter: none;
  transition: filter var(--theme-global-transition-speed, 0.2s) ease;
  box-sizing: border-box;
  color: var(--theme-color-foreground);

  --padding-y: 1em;
  --padding-x: 1.2em;
  --padding-r: calc(var(--padding-x) + var(--mobile-padding-right, 0px));
  padding: 
    0 
    var(--padding-r) 
    var(--padding-y) 
    var(--padding-x) 
  ;
  margin-top: var(--control-height, 3em);

  /* only make contents visible when it has a depth of 0 */
  visibility: hidden;

  & *::selection {
    color: var(--theme-color-modal-selection-foreground);
    background: var(--theme-color-modal-selection-background);
  }

  & > :last-child {
    margin-bottom: 6vh;
  }

  ${responsive.desktop(css`
    --desktop-max-height: calc(95vh - var(--control-height, 3em));
    max-height: var(--desktop-max-height);
  `)}
`;

stack.ajax = styled.div`
  position: absolute;
  height: 100%;
  width: 100%;
  display: none;
  top: 0;
  left: 0;
`;

/**
 * @var  {int}  --d - layer depth as set by interface.wrapper
 */
stack.wrapper = styled.section`
  /* 
    on mobiles, we want to add an extra padding space on the right,
    so that stacking transitions will not show a gap
  */
  --mobile-padding-right: 20vw;

  height: 100%;
  width: calc(100% + var(--mobile-padding-right));
  position: absolute;
  top: 0;
  left: 0;
  border: 0;
  display: flex;
  align-items: center;

  font-size: 16px;
  line-height: 1.1em;

  &.no-controls {
    & ${stack.modal} {
      height: var(--height, auto);
    }

    & ${stack.content} {
      height: auto;
      max-height: 100%;
      margin-top: var(--padding-y);
    }
  }

  &.loading {
    & ${stack.content} {
      filter: blur(5px);
    }

    & ${stack.ajax} {
      display: block;
    }
  }

  &:nth-last-of-type(2) { 
    ${stack.content} { visibility: visible; }
  };

  &:last-of-type { 
    visibility: hidden;
  };

  ${responsive.desktop(css`
    --mobile-padding-right: 0px;
  `)}
`;

export {
  stack 
};