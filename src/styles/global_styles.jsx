/**
 * Global App Styles
 * @author Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @export  GlobalStyles
*/

import { createGlobalStyle } from 'styled-components';
import { text } from './text';
import leagueMonoVariable from '../fonts/league-mono-variable.woff2';
import leagueSpartanVariable from '../fonts/league-spartan-variable.woff2'
import fanwoodRegular from '../fonts/fanwood-regular.woff';
import fanwoodItalic from '../fonts/fanwood-italic.woff';

/**
 * @param  {bool}  props.dark - dark mode
 * @param  {TaggedTemplateLiteral}  props.theme - override theme variables
 */
export const GlobalStyles = createGlobalStyle`

  :root {
    --theme-font-size-global: 16px;
  }

  html {
    font-size: var(--theme-font-size-global);
  }

  div, input, textarea, button {
    -webkit-font-smoothing: subpixel-antialiased !important;
    text-rendering: optimizeLegibility !important;
    -moz-osx-font-smoothing: grayscale !important;
    position: relative;
  }

  body {
    position: relative;
    overflow: hidden;
    margin: 0;
    box-sizing: border-box;
  }

  main {
    display: block;
    position: fixed;
    overflow: hidden;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
  }

  div {
    display: block;
    width: 100%;
    padding: 0;
  }

  img {
    display: block;
  }

  /* theme assignments for components */
  /* default is light color scheme */
  /* use 'dark-theme' class to override with dark color scheme */
  #flow-interface {

    /* >>> fonts styles >>> */

    @font-face {
      font-family: 'League Spartan Variable';
      src: url('${leagueSpartanVariable}') format('woff2-variations');
      font-weight: 200 900;
      font-display: swap;
      /* 
        @see https://github.com/theleagueof/league-spartan
        200 Extralight
        300 Light
        400 Regular
        500 Medium
        600 Semibold
        700 Bold
        800 Extrabold
        900 Black

        For best rendering, font-variation-settings: "wght" 300
        rather than font-weight: 300.
      */
    }

    @font-face {
      font-family: 'League Mono';
      src: url('${leagueMonoVariable}') format('woff2');
      font-weight: 100 800;
      font-variation-settings: "wdth" 100;
      font-style: normal;
      font-display: swap;
      font-feature-settings: 'salt' on;

      /*
        @see https://tylerfinck.com/leaguemonovariable/
        font-variation-settings: "wdth" <50-200>;
        We are using a global weight value of 500, so 80 of width is 'normal'
      */
    }

    @font-face {
      font-family: 'Fanwood';
      src: url('${fanwoodRegular}') format('woff');
      font-weight: normal;
      font-style: normal;
      font-display: swap;
    }

    @font-face {
      font-family: 'Fanwood';
      src: url('${fanwoodItalic}') format('woff');
      font-weight: normal;
      font-style: italic;
      font-display: swap;
    }

    --theme-font-sans-serif: 'League Spartan Variable', sans-serif;
    --theme-font-monospace: 'League Mono', monospace;
    --theme-font-serif: 'Fanwood', serif;
    --theme-font-icons-material: 'Material Icons';

    font-family: var(--theme-font-sans-serif, sans-serif);
    font-weight: 500;
    letter-spacing: 0.05ch;
    font-size: 1rem;

    /* >>> color palette >>> */

    /* default palette is based on Mozilla Dot Design
      @see https://mozilla.design/firefox/color/
     */
    --theme-color-accent: #121212;
    --theme-color-accent-contrast: #ffffff;
    --theme-color-blue: #48a1d9;
    --theme-color-cyan: #63d1de;
    --theme-color-teal: #91d3a3;
    --theme-color-green: #20a185;
    --theme-color-yellow: #ffd95c;
    --theme-color-orange: #f5855b;
    --theme-color-red: #da4646;
    --theme-color-pink: #ffa3b2;
    --theme-color-purple: #9966e8;
    --theme-color-midnight: #1a1a1a;
    --theme-color-darkgrey: #666666;
    --theme-color-grey: #a6a6a6;
    --theme-color-lightgrey: #e6e6e6;
    --theme-color-white: #ffffff;
    --theme-color-black: #000000;

    --theme-color-text-light: #ffffff;
    --theme-color-text-dark: #121212;

    --theme-color-background: #FAFAFA; 
    --theme-color-foreground: #121212; 
    --theme-color-background-mono: #ebf0ebdb;
    --theme-color-background-stacks: #ebf0eb;
    --theme-color-background-transparent-light: #FAFAFA66;
    --theme-color-background-transparent-dark: #1212121A;
    --theme-color-shadow: #1e2e2235; /* #1e2e22 */

    /* other */
    --theme-global-transition-speed: 0.2s;

    /* interface */
    --theme-interface-background: 
      linear-gradient(120deg, #20a185 0%, #91d3a3 100%); 
    --menu-group-background:
      linear-gradient(120deg, #2989c5 0%, #63d1de 100%); 

    /* notification element */
    --theme-color-notification-background: #263238;
    --theme-color-notification-foreground: #ffffff;
    --theme-font-size-notification: var(--theme-font-size-global);

    /* modal element */
    --theme-color-modal-overlay: transparent;
    --theme-color-modal-background: var(--theme-color-background);
    --theme-color-modal-selection-background: var(--theme-color-foreground);
    --theme-color-modal-selection-foreground: var(--theme-color-background);

    /* misc layout elements */
    --theme-color-divider-horizontal: var(--theme-color-darkgrey);

    /* menu element */
    --theme-color-menu-base: var(--theme-color-white);
    --theme-color-menu-first: var(--theme-color-purple);
    --theme-color-menu-second: var(--theme-color-cyan);
    --theme-color-menu-third: var(--theme-color-orange);
    --theme-color-menu-fourth: var(--theme-color-yellow);
    --theme-color-menu-fifth: var(--theme-color-pink);

    /* ajax loading dots */
    --theme-color-ajax-dots-base: var(--theme-color-foreground);
    --theme-color-ajax-dots-first: var(--theme-color-cyan);
    --theme-color-ajax-dots-second: var(--theme-color-yellow);
    --theme-color-ajax-dots-third: var(--theme-color-pink);

    /* info block */
    --theme-font-infoblock-heading: var(--theme-font-sans-serif);
    --theme-font-infoblock-content: var(--theme-font-sans-serif);

    /* buttons */
    --theme-color-button-filled-foreground: var(--theme-color-white);
    --theme-color-button-filled-background: var(--theme-color-accent);
    --theme-color-button-open-foreground: var(--theme-color-darkgrey);
    --theme-color-button-open-background: var(--theme-color-accent);

    /* input */
    --theme-color-input-foreground: var(--theme-color-foreground);
    --theme-color-input-background: 
      var(--theme-color-background-transparent-light);
    --theme-color-input-background-focused: var(--theme-color-white);
    --theme-color-input-background-error: #E029501A;
    --theme-color-input-accent: var(--theme-color-accent);
    --theme-color-input-accent-contrast: var(--theme-color-accent-contrast);
    --theme-color-input-error: var(--theme-color-red);
    --theme-clor-input-disabled: var(--theme-color-darkgrey);
    --theme-color-input-label-background: transparent;
    --theme-color-input-label-foreground: var(--theme-color-input-accent);
    --theme-color-input-label-background-focused: transparent;
    --theme-color-input-label-foreground-focused: var(--theme-color-input-accent);
    --theme-color-input-label-background-error: transparent;
    --theme-color-input-label-foreground-error: var(--theme-color-input-error);
    --theme-font-input: var(--theme-font-monospace);
    --theme-font-input-label: var(--theme-font-sans-serif);

    /* filepicker */
    --theme-color-filepicker-button-background: 
      var(--theme-color-button-filled-background);
    --theme-color-filepicker-button-foreground: 
      var(--theme-color-button-filled-foreground);
    --theme-color-filepicker-foreground: 
      var(--theme-color-input-foreground);
    --theme-color-filepicker-background: var(--theme-color-input-background);
    --theme-color-filepicker-background-error: 
      var(--theme-color-input-background-error);
    --theme-color-filepicker-accent: var(--theme-color-input-accent);
    --theme-color-filepicker-hover-background: 
      var(--theme-color-input-background-focused);
    --theme-color-filepicker-hover-background-error: 
      var(--theme-color-input-background-focused);
    --theme-color-filepicker-error: var(--theme-color-red);
    --theme-font-filepicker: var(--theme-font-monospace);

    /* markdown */
    --theme-color-markdown-foreground: var(--theme-color-foreground); 
    --theme-color-markdown-background: inherit; 
    --theme-font-markdown-loading: var(--theme-font-monospace);

    /* placeholder */
    --theme-color-placeholder-loading-pulse: #FAFAFA4D;
    --theme-color-placeholder-loading-error: #E029501A;

    /* payment card */
    --theme-color-paymentcard-foreground: var(--theme-color-input-foreground);
    --theme-color-paymentcard-error: var(--theme-color-red);
    --theme-color-paymentcard-accent: var(--theme-color-input-accent);
    --theme-color-paymentcard-base: #FAFAFA3A;
    --theme-color-paymentcard-visa: #1A1F713A;
    --theme-color-paymentcard-mastercard: #231F203A;
    --theme-color-paymentcard-amex: #006FCF3A;
    --theme-color-paymentcard-dinersclub: #1E34413A;
    --theme-color-paymentcard-unionpay: #B7C2D03A;
    --theme-color-paymentcard-input-background: 
      var(--theme-color-input-background);
    --theme-color-paymentcard-input-foreground: 
      var(--theme-color-input-foreground);
    --theme-color-paymentcard-input-background-focused: 
      var(--theme-color-input-background-focused);
    --theme-color-paymentcard-input-placeholder-foreground: 
      var(--theme-color-input-foreground);
    --theme-font-paymentcard: var(--theme-font-monospace);
    --theme-font-paymentcard-message: var(--theme-font-sans-serif);

    /* scrollable */
    --theme-color-scrollable-track: #cccccc50;
    --theme-color-scrollable-thumb: #7f7f7f50;
    --theme-color-scrollable-theme-hover: #a6a6a6;

    /* selector (checkbox, radio) */
    --theme-color-selector-active: var(--theme-color-accent);
    --theme-color-selector-inactive: var(--theme-color-darkgrey);
    --theme-color-selector-hover-background: var(--theme-color-accent);

    /* selector box */
    --theme-color-selectbox-foreground: var(--theme-color-input-foreground);
    --theme-color-selectbox-background: var(--theme-color-input-background);
    --theme-color-selectbox-background-open: var(--theme-color-background);
    --theme-color-selectbox-accent: var(--theme-color-accent);
    --theme-color-selectbox-accent-contrast: var(--theme-color-accent-contrast);
    --theme-font-selectbox: var(--theme-font-monospace);
    --theme-font-selectbox-label: var(--theme-font-input-label);

    /* datepicker */
    --theme-color-datepicker-foreground: var(--theme-color-input-foreground);
    --theme-color-datepicker-background: var(--theme-color-input-background);
    --theme-color-datepicker-background-open: var(--theme-color-background);
    --theme-color-datepicker-accent: var(--theme-color-accent);
    --theme-color-datepicker-accent-contrast: var(--theme-color-accent-contrast);
    --theme-font-datepicker: var(--theme-font-monospace);
    --theme-font-datepicker-label: var(--theme-font-input-label);

    /* toggle */
    --theme-color-toggle-label-active: var(--theme-color-body-foreground);
    --theme-color-toggle-label-inactive: var(--theme-color-darkgrey);
    --theme-color-toggle-switch-active: var(--theme-color-accent);
    --theme-color-toggle-switch-inactive: var(--theme-color-accent);
    --theme-color-toggle-hover-background: var(--theme-color-accent);

    /* text */
    --theme-color-text-table-background: 
      var(--theme-color-background-transparent-light);
    --theme-color-text-table-tr-hover: #FFFFFF;
    --theme-color-text-link: var(--theme-color-foreground);
    --theme-color-text-link-visited: var(--theme-color-foreground);
    --theme-color-text-blockquote-background: 
      var(--theme-color-background-transparent-dark);
    --theme-color-text-blockquote-accent: 
      var(--theme-color-foreground);
    --theme-color-text-codeblock: var(--theme-color-background-transparent-dark);
    --theme-color-text-list-marker: var(--theme-color-foreground);
    --theme-font-text-table-th: var(--theme-font-sans-serif);
    --theme-font-text-table-td: var(--theme-font-sans-serif);
    --theme-font-text: var(--theme-font-sans-serif);
    --theme-font-text-h1: var(--theme-font-sans-serif);
    --theme-font-text-h2: var(--theme-font-sans-serif);
    --theme-font-text-strong: var(--theme-font-sans-serif);
    --theme-font-text-em: var(--theme-font-serif);
    --theme-font-text-link: var(--theme-font-sans-serif);
    --theme-font-text-blockquote: var(--theme-font-sans-serif);
    --theme-font-text-code: var(--theme-font-monospace);
    --theme-font-text-codeblock-label: var(--theme-font-sans-serif);
    --theme-font-text-codeblock: var(--theme-font-monospace);
    --theme-font-text-list-marker: var(--theme-font-serif);
    --theme-style-text-list-marker-size: 3px;

    &.dark-theme {

      --theme-color-accent: #ffffff;
      --theme-color-accent-contrast: #121212;
      --theme-color-shadow: #66666635;

      --theme-color-background: #263238;
      --theme-color-foreground: #ffffff;
      --theme-color-background-mono: #263238DB;
      --theme-color-background-stacks: #263238;
      --theme-color-background-transparent-light: #12121299;
      --theme-color-background-transparent-dark:#FAFAFA32;

      --theme-interface-background: 
        linear-gradient(120deg, #187662 0%, #6dc485 100%);
      --menu-group-background: 
        linear-gradient(120deg, #206c9b 0%, #39c5d5 100%);

      --theme-color-divider-horizontal: var(--theme-color-lightgrey);

      --theme-color-notification-background: #fafafa;
      --theme-color-notification-foreground: var(--theme-color-text-dark);

      --theme-color-button-filled-foreground: var(--theme-color-text-dark);
      --theme-color-button-filled-background: var(--theme-color-lightgrey);
      --theme-color-button-open-foreground: var(--theme-color-lightgrey);

      --theme-color-selectbox-popup-background: #121212B0;
      --theme-color-input-background-focused: var(--theme-color-black);

      --theme-color-placeholder-loading-pulse: #FAFAFA10;

      --theme-color-paymentcard-base: #1212124A;
      --theme-color-paymentcard-visa: #8085D73A;
      --theme-color-paymentcard-mastercard: #8985863A;
      --theme-color-paymentcard-amex: #66D5FF3A;
      --theme-color-paymentcard-dinersclub: #849AA73A;
      --theme-color-paymentcard-unionpay: #D1DCEA3A;

      --theme-color-selector-inactive: var(--theme-color-lightgrey);

      --theme-color-selectbox-popup-background: var(--theme-color-black);

      --theme-color-toggle-label-inactive: var(--theme-color-lightgrey);

      --theme-color-text-codeblock: var(--theme-color-background-transparent-light);

      --theme-color-table-tr-hover: #000000;
      --theme-color-table-background: var(--theme-color-background-transparent-dark);
    }
  }

  /* >>> common text elements >>> */
  ${text.h1}
  ${text.h2}
  ${text.p}
  ${text.strong}
  ${text.em}
  ${text.link}
  ${text.hr}
  ${text.code}
  ${text.table}
  ${text.ul}
  ${text.ol}
  ${text.blockquote}

  /* >>> theme overrides >>> */
  ${props => props.theme}
`;