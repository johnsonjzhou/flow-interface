/**
 * Styles for common text elements
 * @author  Johnson Zhou  <johnson@simplyuseful.io>
 * 
 * @export  text  
 * { 
 *  h1, h2, p, strong, em, link, blockquote, hr, code, codeblock, table, 
 *  ul, ol 
 * }
 */
import { css } from 'styled-components';
// import materialIconsOutlined from '../fonts/material-icons-outlined.woff2';

const text = {};

text.h1 = css`
  h1 {
    margin: 0 0 1.4em 0;

    font-family: sans-serif;
    font-family: var(--theme-font-text-h1);
    font-weight: 600;
    letter-spacing: 0;
    font-size: 2.4em;
    line-height: 1.2em;
    text-overflow: ellipsis;
    white-space: pre-wrap;
    overflow: hidden;
  }
`;

text.h2 = css`
  h2 {
    margin: 4em 0 2em 0;

    font-family: sans-serif;
    font-family: var(--theme-font-text-h2);
    font-weight: 700;
    letter-spacing: 0.3ch;
    font-size: 1.2em;
    line-height: 1em;
    text-overflow: ellipsis;
    white-space: pre-wrap;
    overflow: hidden;
    text-transform: uppercase;

    &:first-child {
      margin: 0 0 2em 0;
    }
  }
`;

text.p = css`
  p {
    margin: 0 0 1.5em 0;

    font-family: inherit;
    font-weight: inherit;
    letter-spacing: 0.1ch;
    font-size: inherit;
    line-height: 1.5em;
    text-overflow: ellipsis;
    white-space: normal;
    overflow: hidden;
  }
`;

text.strong = css`
  strong {
    font-family: sans-serif;
    font-family: var(--theme-font-text-strong);
    font-weight: 700;
    letter-spacing: inherit;
    font-size: inherit;
    line-height: inherit;
  }
`;

text.em = css`
  em {
    font-family: serif;
    font-family: var(--theme-font-text-em);
    font-style: italic;
    font-size: 1.1em;
    letter-spacing: 0;
    font-weight: 400;
  }
`;

text.link = css`
  a {
    display: inline-box;
    text-decoration: none;
    position: relative;

    font-family: inherit;
    font-family: var(--theme-font-text-link);
    font-weight: inherit;
    letter-spacing: inherit;
    font-size: inherit;
    line-height: inherit;

    &[target="_blank"] {
      /* &:before {

        @font-face {
          font-family: 'Material Icon Outline';
          src: url('${materialIconsOutlined}') format('woff2');
          font-style: normal;
          font-weight: 400;
          font-display: swap;
        }

        font-family: 'Material Icon Outline';
        content: 'launch';
        vertical-align: bottom;
        font-size: 0.8em;
        padding: 0 0.1em 0 0;
        line-height: inherit;

        font-weight: normal;
        font-style: normal;
        display: inline-block;
        text-transform: none;
        letter-spacing: normal;
        word-wrap: normal;
        white-space: nowrap;
        direction: ltr;
        -webkit-font-smoothing: antialiased;
        text-rendering: optimizeLegibility;
        -moz-osx-font-smoothing: grayscale;
        font-feature-settings: 'liga';
      } */
    }

    &:after {
      content: ' ';
      display: block;
      width: 100%;
      height: 2px;
      bottom: 0px;
      background: black;
      background: var(--theme-color-text-link);
      position: absolute;
      opacity: 1;
      transition: all 0.3s ease;
    }

    &:link, &:visited {
      color: black;
      color: var(--theme-color-text-link-visited);
    }

    &:hover {
      &:after{
        opacity: 0.1;
        height: 100%;
        bottom: 0;
      }
    }

    &:active {
      &:after{
        opacity: 0.1;
        height: 100%;
        bottom: 0;
      }
      
    }
  }
`

text.blockquote = css`
  blockquote {
    display: block;
    position: relative;
    background-color: transparent;
    font-size: 0.9em;
    border-left: 3px solid;
    border-color: black;
    border-color: var(--theme-color-text-blockquote-accent);
    background-color: lightgrey;
    background-color: var(--theme-color-text-blockquote-background);
    margin: 0 0 1.5em 0;
    padding: 1em 1em 1.2em 1em;

    font-family: inherit;
    font-family: var(--theme-font-text-blockquote);
    font-weight: inherit;
    letter-spacing: inherit;
    font-size: 0.9em;
    line-height: inherit;

    & > :last-child {
      margin-bottom: 0;
    }
  }
`;

text.hr = css`
  hr {
    border: solid 1px;
    margin: 2em 0 2em 0;

    & + h1, + h2 {
      margin-top: 0;
    }
  }
`;

text.code = css`
  code {
    font-family: var(--theme-font-text-code);
    font-size: 0.9em;
  }
`;

text.codeblock = css`
  pre {
    border: 0;
    margin: 0;
    position: relative;
    display: block;
    background-color: lightgrey;
    background-color: var(--theme-color-text-codeblock);
    border-radius: 6px;

    & code {
      display: block;
      position: relative;
      background-color: transparent;
      font-size: 0.9em;
      margin: 0 0 1.5em 0;
      padding: 2.2em 1em 1.2em 1em;
      overflow-x: auto;

      font-family: inherit;
      font-family: var(--theme-font-text-codeblock);
      font-weight: inherit;
      letter-spacing: inherit;
      font-size: 0.9em;
      line-height: inherit;
      tab-size: 2;

      & > :last-child {
        margin-bottom: 0;
      }

      &:after {
        content: 'Code Snippet';
        display: block;
        position: absolute;
        top: 0.8em;

        font-family: var(--theme-font-text-codeblock-label);
        font-weight: 600;
        font-size: 0.8em;
        line-height: 1em;
        height: 1em;
        text-overflow: ellipsis;
        white-space: pre-wrap;
        overflow: hidden;
        text-transform: uppercase;
        letter-spacing: 0.3ch;
      }

      &.language-bash:after,
      &.language-shell:after,
      &.language-linux:after {
        content: 'Shell';
      }

      &.language-code:after {
        content: 'Code';
      }

      &.language-data:after {
        content: 'Data';
      }

      &.language-javascript:after {
        content: 'Javascript';
      }

      &.language-php:after {
        content: 'PHP';
      }

      &.language-apache:after {
        content: 'Apache';
      }

      &.language-javascript:after {
        content: 'Javascript';
      }

      &.language-mysql:after {
        content: 'MySQL';
      }

      &.language-nosql:after {
        content: 'NoSQL';
      }

      &.language-json:after {
        content: 'JSON';
      }
    }
  }
`;

text.table = css`
  table {
    margin: 0 0 1.5em 0;
    padding: 0.5em 0;
    border: 0;
    position: relative;
    display: inline-block;
    max-width: 100%;
    height: auto;
    background-color: #FFFFFFFF;
    background-color: var(--theme-color-text-table-background);
    border-radius: 6px;
    overflow-x: auto;
    font-size: 1em;
    border-collapse: collapse;

    & th, & td {
      padding: 0 0.5em;
      white-space: pre;
    }

    & thead {
      & tr {
        & th {
          font-family: var(--theme-font-text-table-th);
          font-weight: 700;
          font-size: 0.9em;
          letter-spacing: 0;
          text-align: left;
          text-transform: capitalize;
        }
      }
    }

    & tbody {
      & tr {
        transition: background-color 0.2s ease;

        &:hover {
          background-color: var(--theme-color-text-table-tr-hover);
        }

        & td {
          font-family: var(--theme-font-text-table-td);
          font-weight: 500;
          text-align: left;
          height: 2.5em;
          vertical-align: middle;
        }
      }
    }
  }
`;

text.ul = css`
  & ul {
    padding: 0 0 0 1.6em;

    & li {
      --line-height: 1.5em;

      list-style: none;
      position: relative;

      margin: 0 0 0.5em 0;

      font-family: inherit;
      font-weight: inherit;
      letter-spacing: 0.1ch;
      font-size: inherit;
      line-height: var(--line-height);
      text-overflow: ellipsis;
      white-space: normal;

      &:marker {
        display: none;
      }

      &:before {
        --size: var(--theme-style-text-list-marker-size, 2px);

        content: '';
        display: block;
        position: absolute;
        width: var(--size);
        height: var(--size);
        top: calc((var(--line-height) - var(--size)) / 2);
        left: -0.8em;
        background-color: var(--theme-color-text-list-marker);
        border: 0;
        border-radius: var(--size);
      }
    }
  }
`;

text.ol = css`
  & ol {
    padding: 0 0 0 1.6em;
    counter-reset: numbered-list;

    & li {
      --line-height: 1.5em;

      list-style: none;
      position: relative;

      margin: 0 0 0.5em 0;

      font-family: inherit;
      font-weight: inherit;
      letter-spacing: 0.1ch;
      font-size: inherit;
      line-height: var(--line-height);
      text-overflow: ellipsis;
      white-space: normal;

      &:before {
        font-family: var(--theme-font-text-list-marker);
        font-style: italic;
        counter-increment: numbered-list;
        content: counters(numbered-list, ".") ". ";
        display: block;
        position: absolute;
        left: -1.2em;
        border: 0;
        border-radius: var(--size);
      }
    }
  }
`;

export {
  text
};