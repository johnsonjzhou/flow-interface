/**
 * Styles for the background menu
 * @author  Johnson Zhou  <johnson@simplyuseful.io>
 * 
 * @export  menu
 * { wrapper, logo, name, menu, item }
 */
import styled, { css } from 'styled-components';
import { flexbox, responsive, vhFontSizeLimits } from './mixins'; 

export const menu = {};

menu.wrapper = styled.nav`
  ${flexbox({
    direction: 'column',
    xAlign: 'left',
    yAlign: 'top'
  })}
  height: 100%;
  width: 100%;
  box-sizing: border-box;
  padding: 0 4vw;
  margin: 0 auto;
  color: var(--theme-color-white, white);
  line-height: 1.1em;
  overflow-x: hidden;
  overflow-y: auto;

  font-size: 4.2vh;
  ${vhFontSizeLimits({
    size: 4.2, 
    minSize: '34px', 
    maxVertical: '1000px'
  })}

  ${responsive.tablet(css`
    // overflow-y: hidden;
    padding: 0 6vw;
  `)}

  ${responsive.desktop(css`
    width: initial;
    max-width: 1440px;
  `)}
`;

menu.logo = styled.img`
  max-width: 100%;
  height: 1.5em;
  margin: 0 auto 0 0;
  box-sizing: content-box;
  padding: 1.5em 0 0 0;
  overflow: visible;
`;

menu.name = styled.div`
  width: 100%;
  font-size: 1.5em;
  height: 1.5em;
  line-height: 1.5em;
  padding: 1.5em 0 0 0;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  font-family: var(--theme-font-sans-serif, sans-serif);
  font-weight: 700;
  letter-spacing: 0.05ch;
`;

menu.menu = styled.div`
  padding-top: 3em;
  padding-bottom: 2em;
  flex-grow: 1;
  ${flexbox({
    direction: 'column',
    xAlign: 'left',
    yAlign: 'top'
  })}
  max-width: 320px;
  opacity: 1;
  transition: all 0.2s ease;
`;

menu.item = styled.div`
  font-family: var(--theme-font-sans-serif, sans-serif);
  font-weight: 700;
  letter-spacing: 0.05ch;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  
  width: initial;
  padding: 0.3em 0;
  outline: none;
  cursor: pointer;
  transition: color 0.3s ease;
  flex-shrink: 0;

  color: var(--theme-color-menu-base, white);

  &:nth-of-type(5n) {
    &:hover {
      color: var(--theme-color-menu-first, black);
    }
  }

  &:nth-of-type(5n+1) {
    &:hover {
      color: var(--theme-color-menu-second, black);
    }
  }

  &:nth-of-type(5n+2) {
    &:hover {
      color: var(--theme-color-menu-third, black);
    }
  }

  &:nth-of-type(5n+3) {
    &:hover {
      color: var(--theme-color-menu-fourth, black);
    }
  }

  &:nth-of-type(5n+4) {
    &:hover {
      color: var(--theme-color-menu-fifth, black);
    }
  }
`;