/**
 * Styles for icon fonts
 * @author Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @export MaterialIconOutline
 * 
 * todo:  icon.materialFilled
 * todo:  icon.materialRegular
 * todo:  icon.materialRounded
 * todo:  icon.materialSharp
 * todo:  icon.materialTwoTone
 * 
*/
import styled from 'styled-components';
import materialIconsOutlined from '../fonts/material-icons-outlined.woff2';


/**
 * @param  {boolean}  props.inline
 */
const iconFontBase = styled.i`
  /* font-family by extension */
  font-weight: normal;
  font-style: normal;
  display: inline-block;
  text-transform: none;
  letter-spacing: normal;
  word-wrap: normal;
  white-space: nowrap;
  direction: ltr;
  -webkit-font-smoothing: antialiased;
  text-rendering: optimizeLegibility;
  -moz-osx-font-smoothing: grayscale;
  font-feature-settings: 'liga';
  vertical-align: ${({inline}) => (inline && `bottom`) || `initial`};
`;

const MaterialIconOutline = styled(iconFontBase)`
  @font-face {
    font-family: 'Material Icon Outline';
    src: url('${materialIconsOutlined}') format('woff2');
    font-style: normal;
    font-weight: 400;
    font-display: swap;
  }

  font-family: 'Material Icon Outline';
`;

export {
  MaterialIconOutline, 
};
