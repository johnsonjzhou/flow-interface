/**
 * Styling for loading dots used in ajax events
 * @author Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @export  Dots
 * @export  Wrapper
*/
import styled, { css, keyframes } from 'styled-components';
import { positionAbsoluteCenter } from './mixins';

/**
 * Dot animations
 * eg. () () () -> (x) (x) (x)
 * 
 * @param  {string}  props.size- Size of the dot specified in rem
 * @param  {object}  theme
 */
const Dots = styled.div(({
  size = '1rem',
  overrides,
}) => {
  
  // dot color scheme
  const dot = {
    on1: 'var(--theme-color-ajax-dots-first, lightgrey)',
    on2: 'var(--theme-color-ajax-dots-second, grey)',
    on3: 'var(--theme-color-ajax-dots-third, darkgrey)',
    off: `transparent`,
    def: 'var(--theme-color-ajax-dots-base, black)',
  }

  // dot animation keyframes, dot1, dot2, dot3
  const dot1 = keyframes`
    0%		{background-color: ${dot.def};}
    10% 	{background-color: ${dot.off};}
    20% 	{background-color: ${dot.on1};}
    50% 	{background-color: ${dot.on1};}
    60% 	{background-color: ${dot.off};}
    70% 	{background-color: ${dot.def};}
    100%	{background-color: ${dot.def};}
  `;

  const dot2 = keyframes`
    0%		{background-color: ${dot.def};}
    3%		{background-color: ${dot.def};}
    13% 	{background-color: ${dot.off};}
    23% 	{background-color: ${dot.on2};}
    53% 	{background-color: ${dot.on2};}
    63% 	{background-color: ${dot.off};}
    73% 	{background-color: ${dot.def};}
    100%	{background-color: ${dot.def};}
  `;

  const dot3 = keyframes`
    0%		{background-color: ${dot.def};}
    9%		{background-color: ${dot.def};}
    19% 	{background-color: ${dot.off};}
    29% 	{background-color: ${dot.on3};}
    59% 	{background-color: ${dot.on3};}
    69% 	{background-color: ${dot.off};}
    79% 	{background-color: ${dot.def};}
    100%	{background-color: ${dot.def};}
  `;

  // draw the dot
  const draw_dot = (size = `1rem`) => {
    return css`
      position: absolute;
      width: ${size};
      height: ${size};
      border-radius: ${size};
      background-color: var(--theme-color-ajax-dots-base, black);
    `;
  }
  
  return css`
    animation: ${dot2} 2s infinite;
    ${draw_dot(size)}
    ${positionAbsoluteCenter}

    &:before {
      display: block;
      content: '';
      left: -1.5em;
      animation: ${dot1} 2s infinite;
      ${draw_dot(size)}
    }

    &:after {
      display: block;
      content: '';
      right: -1.5em;
      animation: ${dot3} 2s infinite;
      ${draw_dot(size)}
    }

    ${overrides}
`});

/**
 * For wrapping the dots when used in a button
 */

const Wrapper = styled.div`
  height: 100%;
  width: 100%;
  overflow: visible;
  border-radius: 6px;
  background-color: var(--theme-color-lightgrey, lightgrey);
  z-index: 100;
`;

export {
  Dots,
  Wrapper
};