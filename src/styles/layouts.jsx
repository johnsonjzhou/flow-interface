/**
 * Styles for layout wrappers
 * @author Johnson Zhou  <johnson@simplyuseful.io>
 * 
 * @export wrapper { inline }
 * @export spacer { vertical, horizontal }
 * @export divider { horizontal }
*/
import styled, {css} from 'styled-components';
import { flexbox, responsive } from './mixins';

const wrapper = {};
const spacer = {};
const divider = {};

/**
 * Wrap elements into a single row using flexbox,
 * defaults to space-between
 */
wrapper.inline = styled.div`
  display: block;
	position: relative;
	width: 100%;
	min-height: 2em;
  ${flexbox({direction: 'row', xAlign: 'spaceBetween', yAlign: 'center'})}

  & > * {
    flex: 0 0 auto;
  }
`;

wrapper.inlineShrink = styled(wrapper.inline)`
  ${flexbox({direction: 'column'})} 

  & > * {
    margin-right: 1em !important;
    flex: 0 1 auto;
  }
  
  & > *:last-child {
    margin-right: inherit !important;
  }

  ${responsive.tablet(css`
    ${flexbox({direction: 'row', xAlign: 'spaceBetween', yAlign: 'center'})}
  `)}
`;

wrapper.Grid = styled.div`
  --min: ${props => props.min || '250px'};
  --max: ${props => props.max || '1fr'};
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(var(--min), var(--max)));
  grid-gap: 1em;
`;

wrapper.Inline = wrapper.inline;
wrapper.InlineShrink = wrapper.InlineShrink;

/**
 * A vertical spacer spanning entire width,
 * default height is 1em
 */
spacer.vertical = styled.div`
  display: block;
	position: relative;
	width: 100%;
  height: 1em;
`;

/**
 * A horizontal spacer spanning entier height for a settable width
 * @param  {float}  props.width - in em, default 0.6em
 * @param  {null|string}  props.line
 *  - string describing color from theme, eg, red, yellow, etc
 * @param  {css}  props.overrides
 */
spacer.horizontal = styled.div`
  display: block;
  position: relative;
  height: 100%;
  width: 0.6em;
`;

/**
 * Draws a vertical line to act as a horizontal divider, using
 * spacer.horizontal as a starting point
 * set color by overriding the :after { background-color } property
 * default color is --theme-color-divider-horizontal
 */
divider.horizontal = styled(spacer.horizontal)`
  &:after {
    display: block;
    position: absolute;
    content: '';
    width: 1px;
    height: 60%;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    background-color: var(--theme-color-divider-horizontal);
  }
`;

export {
  wrapper, 
  spacer, 
  divider 
};