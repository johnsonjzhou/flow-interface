/**
 * Styles for DatePicker component 
 * @author  Johnson Zhou  <johnson@simplyuseful.io>  
 * 
 * @export  datepicker 
 * {  }
 */

import styled from 'styled-components';
import { textfield } from './textfield';

export const datepicker = {};

datepicker.Button = styled.div`
  position: absolute;
  right: 0;
  top: 0;
  height: 100%;
  width: var(--width-button);
  color: var(--theme-color-datepicker-accent);
  background-color: transparent;
  transition: background-color 0.2s ease;
  font-size: 1em;
  ${flexbox({ direction: 'row', xAlign: 'center' })}

  & svg {
    display: block;
  }
`;

datepicker.Label = styled(textfield.Label)`
  font-family: var(--theme-font-datepicker-label);
`;

datepicker.Input = styled.input`
  display: block;
  font-size: 1em;
  height: var(--height);
  line-height: var(--height);
  width: 100%;
`;

datepicker.PickerWrapper = styled.div`
  width: 100%;
  height: auto;
  max-height: var(--height-date-selector);
  overflow-x: hidden;
  overflow-y: auto;
  padding-top: calc(2 * var(--padding-y)); 
  position: relative; 

  :before {
    display: block;
    content: '';
    width: 100%;
    height: 1px;
    border-top: solid 1px var(--theme-color-datepicker-foreground);
    opacity: 30%;

    position: absolute;
    left: 0;
    top: var(--padding-y);
  }
`;

datepicker.PickDate = styled.div``;

datepicker.TimePicker = styled.div``;

datepicker.ComboBox = styled.div`
  position: relative;
  min-height: var(--height-selector);
  max-height: var(--height-selector);
  line-height: var(--height);
  width: var(--width);
  padding: 
    var(--padding-y) 
    calc(var(--padding-x) + var(--width-button)) 
    var(--padding-y) 
    var(--padding-x) 
  ;
  border: solid var(--border) transparent;
  border-radius: 6px;
  font-family: var(--font);
  font-size: var(--font-size);
  font-weight: 400;
  color: var(--foreground);
  background-color: var(--background);
  appearance: none;
  overflow: hidden;
  cursor: pointer;
  transition: 
    border 0.2s ease, 
    background-color 0.2s ease, 
    max-height 0.2s ease
  ;

  &:hover, &:focus, &.open {
    border: solid var(--border) var(--border-color);

    ${datepicker.Button} {
      color: var(--theme-color-datepicker-accent-contrast);
      background-color: var(--theme-color-datepicker-accent);
    }
  }

  &:focus, &.open {
    background-color: var(--theme-color-datepicker-background-open);
  }

  &.open {
    max-height: calc(var(--height-selector) + var(--height-option-list));
  }
`;

datepicker.Wrapper = styled.div`
  --height: 2em;
  --height-label: 1.5em;
  --padding-y: 0.5em;
  --padding-x: 0.75em;
  --border: 2px; 
  --border-color: var(--theme-color-datepicker-accent);
  --width-button: 1.5em;
  --width: calc(100% - (2 * (var(--padding-x) + var(--border))) - var(--width-button));
  --height-selector: calc(var(--height) - var(--border));
  --height-option-list: 50vh; // todo  match this to selector height 
  --font-size: 1em; 
  --font: var(--theme-font-datepicker);
  --foreground: var(--theme-color-datepicker-foreground);
  --background: var(--theme-color-datepicker-background);
  --label-color-foreground: var(--theme-color-input-label-foreground);
  --label-color-background: var(--theme-color-input-label-background);

  padding: 1.5em 0 0 0;
  margin: 0 0 2em 0;
`;