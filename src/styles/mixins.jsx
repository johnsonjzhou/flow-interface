/**
 * CSS mixins
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @export  flexbox - flexbox styles
 * @export  responsive - media queries for devices
 * @export  positionAbsoluteCenter - absolutely position in the center
 * @export  vhFontSizeLimits - set maximum font size limit when using vh
 */
import { css } from 'styled-components';

/**
 * Provides standardised flexbox styles
 * @param  {object}  
 * @param  {string}  direction - flex direction
 * @param  {string}  xAlign - alignment along x-axis
 * @param  {string}  yAlign - alignment along y-axis
 * @param  {string}  wrap - wrapping mode
 */
function flexbox({
  direction = `column`, 
  xAlign = `left`, 
  yAlign = `center`, 
  wrap = `nowrap`
}) {

  const xAlignOpt = {
    left: `flex-start`,
    top: `flex-start`,
    center: `center`,
    right: `flex-end`,
    bottom: `flex-end`,
    spaceAround: `space-around`,
    spaceBetween: `space-between`,
    spaceEvenly: `space-evenly`,
    unset: `initial`
  }

  const yAlignOpt =  {
    top: `flex-start`,
    center: `center`,
    bottom: `flex-end`,
    stretch: `stretch`,
    baseline: `baseline`,
    unset: `initial`
  }

  let flexOpts;

  if ((direction === `row`) || (direction ===`row-reverse`)) {
    flexOpts = `
      justify-content: ${xAlignOpt[xAlign]};
      align-items: ${yAlignOpt[yAlign]};
    `;
  } else {
    flexOpts = `
      justify-content: ${yAlignOpt[yAlign]};
      align-items: ${xAlignOpt[xAlign]};
    `;
  }

  return css`
    display: flex;
    flex-direction: ${direction};
    flex-wrap: ${wrap};
    ${flexOpts}
  `;
}

/**
 * Responsive media queries for different device types,
 * use of this assumes base styles are for mobile devices.
 * 
 * Breakpoints are:
 * mobileLandscape
 * tablet
 * desktop
 * ultrawide
 * 
 * @param  {css}  styles - styles defined using styled-components css helper
 */

const responsive = {}

responsive.mobileLandscape = (styles) => css`
  @media only screen 
  and (max-height: 599px)
  and (orientation: landscape) {
    ${styles}
  }
`;

responsive.tablet = (styles) => css`
  @media only screen 
  and (min-width: 600px) 
  and (min-height: 600px) {
    ${styles}
  }
`;

responsive.desktop = (styles) => css`
  @media only screen and (min-width: 992px) {
    ${styles}
  }
`;

responsive.ultrawide = (styles) => css`
  @media only screen and (min-width: 2000px) {
    ${styles}
  }
`;

/**
 * for use when font size is specified in vh
 * provides media queries to define maximum and minimum font sizes
 * @param  {object}
 * @param  {float}  size - in vh
 * @param  {float|string}  minSize - minimum font size acceptable in px
 * @param  {float|string}  maxVertical - maximum vertical height can be in px
 * @return  {css}
 */
function vhFontSizeLimits({
  size,
  minSize = '16px', 
  maxVertical = '1000px'
}) {
  size = parseFloat(size);
  minSize = parseFloat(minSize);
  maxVertical = parseFloat(maxVertical);

  const vhToPx = (size, vertical = 1000) => {
    size = parseFloat(size);
    vertical = parseFloat(vertical);
    return `${parseInt((size/100)*vertical)}px`;
  }

  const minSizeBreakpoint = (size, minSize = '16px') => {
    size = parseFloat(size);
    minSize = parseFloat(minSize);
    return `${parseInt((minSize * 100) / size)}px`;
  }

  return css`
    @media only screen and (max-height: ${minSizeBreakpoint(size, minSize)}) {
      font-size: ${minSize}px;
    }
    
    @media only screen and (min-height: ${maxVertical}px) {
      font-size: ${vhToPx(size, 1000)};
    }
  `
}

const positionAbsoluteCenter = css`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate3d(-50%, -50%, 0);
`;

export {
  flexbox, 
  responsive, 
  vhFontSizeLimits, 
  positionAbsoluteCenter, 
};