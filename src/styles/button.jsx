/**
 * Styling for Button
 * @author Johnson Zhou  <johnson@simplyuseful.io>
 * 
 * @export OpenButton
 * @export FilledButton
*/
import styled , { css } from 'styled-components';

/**
 * Common styles to both Filled and Open style buttons
 * @var  {css='sans-serif'}  --theme-font-sans-serif
 * @var  {css='lightgrey'}  --button-hover-background
 */
const commonStyles = css`
  font-family: var(--theme-font-sans-serif, sans-serif);
  font-weight: bold;
  font-size: 0.9rem;
  letter-spacing: 0;
  text-transform: capitalize;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;

  position: relative;
  box-sizing: content-box;
  padding: 0 0.9rem;
  width: auto;
  height: 2.5rem;
  line-height: 2.5rem;

  cursor: pointer;

  border: 0;
  outline: 0;
  border-radius: 6px;

  &:focus {
    outline: 0;
  }

  &:before {
    display: block;
    content: '';
    width: 100%;
    height: 100%;
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%) scale(0, 0);
    transition: transform 0.2s ease;
    background-color: var(--button-hover-background, lightgrey);
    opacity: 0.1;
    outline: 0;
    border: 0;
    border-radius: 6px;
  }

  &:hover:before, &:focus:before {
    transform: translate(-50%, -50%) scale(1, 1);
  }

  &:disabled {
    filter: saturate(0.1) opacity(0.2);
    cursor: not-allowed;

    &:hover:before, &:focus:before {
      transform: translate(-50%, -50%) scale(0, 0);
    }
  }
`;

/**
 * Filled design: a button with coloured background and white text
 * eg: [  Button  ]
 * @var  {css='white'}  --theme-color-button-filled-foreground
 * @var  {css='black'}  --theme-color-button-filled-background
 * @var  {css='dimgrey'}  --theme-color-shadow
 * @var  {css='white'}  --theme-color-white
 */
const FilledButton = styled.button`
  ${commonStyles}

  --button-hover-background: var(--theme-color-white, white);

  color: var(--theme-color-button-filled-foreground, white);
  background: var(--theme-color-button-filled-background, black);
  letter-spacing: 0.1ch;
  box-shadow: 0px 2px 8px -2px var(--theme-color-shadow, dimgrey);

  &:before {
    opacity: 0.3;
  }

  &:disabled {
    box-shadow: none;
  }
`;

/**
 * Open design: a button with transparent background and grey text
 * eg:   Button  
 * @var  {css='grey'}  --theme-color-button-open-foreground
 * @var  {css='grey'}  --theme-color-button-open-background
 * @var  {css='black'}  --theme-color-foreground
 */
const OpenButton = styled.button`
  ${commonStyles}

  --button-hover-background: var(--theme-color-button-open-background, grey);

  color: var(--theme-color-button-open-foreground, grey);
  letter-spacing: 0.1ch;
  background-color: transparent;
  box-shadow: none;

  &:hover, &:focus {
    color: var(--theme-color-foreground, black);
  }
`;

export { FilledButton, OpenButton };
