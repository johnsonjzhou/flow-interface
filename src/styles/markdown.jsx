/**
 * Styles for markdown content as parsed by react-markdown
 * @see  https://github.com/rexxars/react-markdown
 * @author  Johnson Zhou  <johnson@simplyuseful.io>
 * 
 * @export  md  
 * { wrapper }
 */
import styled from 'styled-components';
import { text } from './text';

export const md = {};

md.wrapper = styled.div`
  position: relative;
  color: black;
  color: var(--theme-color-markdown-foreground);
  background-color: inherit;
  background-color: var(--theme-color-markdown-background);

  font-family: sans-serif;
  font-family: var(--theme-font-markdown);
  font-weight: 500;
  letter-spacing: 0.05ch;
  font-size: 1rem;
  line-height: 1.5em;

  --theme-color-text-link-visited: var(--theme-color-markdown-foreground);

  ${text.codeblock}
  ${text.ul}
  ${text.ol}

  img { 
    max-width: 100%;
    border-radius: 6px;
  }
`;