/**
 * Styling for InputfilePicker
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @export  filepicker
 * Wrapper, Filelist, File, Name, Action
 */
import styled from 'styled-components';
import { flexbox } from './mixins';

export const filepicker = {};

/* filepicker.Wrapper at bottom */ 

filepicker.Filelist = styled.div`
  box-sizing: content-box;
  color: var(--theme-color-filepicker-foreground);
  background-color: var(--theme-color-filepicker-background);
  border: solid 2px transparent;
  border-radius: 0.5em;
  height: auto;
  width: 100%;
  padding: 0.6em 0 1.8em 0;
  transition: border 0.3s ease;

  &:empty {
    padding: 0;
    height: 4px;
  }

  &:hover, &:focus {
    outline: 0;
    border-color: var(--theme-color-filepicker-accent);
  }
`;

filepicker.File = styled.div`
  font-family: var(--theme-font-filepicker);
  font-size: 0.8em;
  letter-spacing: 0;
  text-transform: none;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;

  box-sizing: content-box;
  background-color: transparent;
  transition: background-color 0.3s ease;
  padding: 0 0.75em;
  width: calc(100% - 1.5em);
  height: 2.1rem;
  cursor: default;
  ${flexbox({direction: 'row', xAlign: 'spaceBetween', yAlign: 'center'})};

  &:hover {
    background-color: var(--theme-color-filepicker-hover-background);
  }

  &.placeholder {
    color: var(--theme-color-filepicker-foreground);
    cursor: pointer;
  }

  &.invalid {
    color: var(--theme-color-filepicker-error); 
    &:hover {
      background-color: var(--theme-color-filepicker-hover-background-error);
    }
  }
`;

filepicker.Name = styled.div`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  flex-grow: 2;
  width: initial;
`;

filepicker.Action = styled.div`
  padding-left: 0.5em;
  flex-grow: 0;
  width: initial;
`;

filepicker.Wrapper = styled.div`
  position: relative;
	box-sizing: border-box;
	padding: 1.5em 0 0 0;
  margin: 0 0 4em 0;
	width: 100%;
  height: auto;
	overflow: visible;
  font-size: 1em;

  & input[type=file] {
    display: none;
  }

  & label {
    position: absolute;
    display: block;
    box-sizing: content-box;
    height: 3em;
    line-height: 3em;
    text-align: center;
    width: 3em;
    border-radius: 2em;
    border: 0; 
    bottom: 0;
    left: 1em;
    transform: translateY(50%);
    color: var(--theme-color-filepicker-button-foreground, white);
    background-color: var(--theme-color-filepicker-button-background, black);
    box-shadow: 2px 2px 6px 0px var(--theme-color-shadow, dimgrey);
    transition: filter 0.3s ease;
    cursor: pointer;

    & > i {
      font-size: 1.6em;
    }

    &:focus {
      outline: 0;
    }
  }

  &.error {
    ${filepicker.Filelist} {
      border-color: var(--theme-color-filepicker-error) !important;
      background-color: var(--theme-color-filepicker-background-error);
    }
  }
`;