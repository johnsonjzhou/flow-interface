<?php
    header( $_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
?>
<!doctype html>
  <!-- Copyright 2020, Johnson Zhou (johnson@simplepharmacy.io), all rights reserved. -->

  <head>

    <meta charset="utf-8" />
    <meta 
      name="viewport" 
      content="width=device-width, height=device-height, initial-scale=1.0"
    />
    <link rel="shortcut icon" href="favicon.png" type="image/png" />

    <!-- About this page -->
    <title>404 - Page not found!</title>
    <meta 
      name="description" 
      content="Page not found!" 
    />

    <style>
      body {
        width: 100%;
        height: 100%;
        background-color: #ECEFF1;
        color: #37474F;
        font-family: monospace;
      }
      main {
        display: block;
        position: absolute;
        top: 30%;
        transform: translateY(-50%);
        left: 5vw;
      }
      div {
        display: inline-block;
        background-color: transparent;
      }
      div.status {
        color: #FBC02D;
        font-size: 16vh;
        line-height: 16vh;
        padding: 0;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
      }
      div.message {
        background-color: #FFFFFF;
        color: inherit;
        font-size: 4vh;
        line-height: 5vh;
        padding: 0 2vh;
        box-shadow: 0.5vh 0.5vh 0px 0px #CFD8DC;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        margin-top: -2.4vh;
      }
      @media only screen and (min-height: 1000px) {
        div.status {
          font-size: 160px;
          line-height: 160px;
        }
        div.message {
          font-size: 40px;
          line-height: 50px;
          margin-top: -24px;
          padding: 0 20px;
          box-shadow: 5px 5px 0px 0px #CFD8DC;
        }
      }
    </style>

  </head>

  <body>
    <main>
      <div class='status'>404</div></br>
      <div class='message'>PAGE NOT FOUND!</div>
    </main>
  </body>

</html>