/**
 * Demonstration of form and data input elements
 * @author  Johnson Zhou  <johnson@simplyuseful.io>  
 * 
 * @export  DemoForm  as default 
 */

import React, { memo, useState, useReducer, useEffect, useRef } from 'react';
import { nonMatchingFields } from 'flow-utils';
import Form from '../components/form';
import TextField from '../components/textfield';
import InputFilePicker from '../components/filepicker';
import PaymentCard from '../components/paymentcard';
import Codeblock from '../components/codeblock';
import Checkbox from '../components/checkbox';
import RadioGroup from '../components/radiogroup';
import SelectBox from '../components/selectbox';
import Toggle from '../components/toggle';
import ReCaptchaV2 from '../components/recaptchav2.jsx';
import { demoContentMap } from '../content/demo_content_map';
import { OpenButton } from '../components/buttons';
import { wrapper } from '../styles/layouts';  

import { RiDatabase2Line as IconData } from 'react-icons/ri';

const { 
  Grid, 
 } = wrapper;

const DemoForm = (props) => {
  const {
    openStack, 
    setQuickActions, 
    setChanged, 
    setLoading, 
  } = props;

  const [ rejectPWC, setRejectPWC ] = useState(false);

  const formData = useRef();

  const handleFormData = (nextFormData) => {
    // window.Console.log(nextFormData, 'Form Data');

    const {
      changed = false, 
    } = nextFormData; 

    // handle changed status 
    setChanged(changed); 

    // handle matching passwords
    const nonMatch = nonMatchingFields(
      nextFormData.data, 'password', 'password_confirm', 'Does not match Password'
    );
    (rejectPWC !== nonMatch) && setRejectPWC(nonMatch);

    // save the new formData
    formData.current = nextFormData;
  };

  useEffect(() => { 
    setQuickActions && setQuickActions([
      {icon: <IconData/>, action: () => openStack(
        <Codeblock 
          language={'json'} 
          data={JSON.stringify(formData.current, null, 2)}
        />
      )},
    ]);
  }, []);

  return (
    <Form callback={handleFormData} >
      <h1>Form and data input.</h1>

      <p>
        A demonstration of the various input fields handled by Flow Interface. All input fields are wrapped in the Form component to manage data state. Click on the data quick action to view the form data.
      </p>

      <OpenButton onClick={() => openStack(demoContentMap.guide_form)}>
        View Form Guide
      </OpenButton>

      <h2>TextField</h2>

      <TextField 
        required 
        name='text' 
        type='text' 
        label='Text' 
        defaultValue='Text field with default value'
      />
      <TextField 
        name='number' 
        type='number' 
        label='Number' 
      />
      <TextField 
        name='tel' 
        type='tel' 
        label='Tel' 
      />
      <TextField 
        name='email' 
        type='email' 
        label='Email' 
      />

      <TextField 
        name='textarea' 
        label='Textarea' 
        multiline 
        defaultValue={`Lorem ipsum dolor sit amet, consectetur adipiscing elit. \nQuisque dictum vitae est non tristique.`}
      />

      <Grid min='300px'>
        <TextField 
          name='password' 
          type='password' 
          label='Password' 
          small 
          required 
          autocomplete='new-password' 
        />
        <TextField 
          name='password_confirm' 
          type='password' 
          label='Confirm Password' 
          small 
          required 
          autocomplete='new-password' 
          rejected={rejectPWC}
        />
      </Grid>

      <OpenButton onClick={() => openStack(demoContentMap.guide_textfield)}>
        View TextField Guide
      </OpenButton>

      <hr/>
      <h2>File Picker</h2>

      <InputFilePicker 
        name='filepicker' 
        accept='image/*,application/pdf' 
        maxQty={3} 
        maxSize={1} 
        multiple 
        placeholder='Add up to 3 PDFs or images'
      />

      <OpenButton onClick={() => openStack(demoContentMap.guide_filepicker)}>
        View FilePicker Guide
      </OpenButton>

      <hr/>
      <h2>Payment Card</h2>

      <PaymentCard
        name='paymentcard'
        acceptOnly={['visa', 'mastercard', 'amex']}
        center 
      />

      <OpenButton onClick={() => openStack(demoContentMap.guide_paymentcard)}>
        View PaymentCard Guide
      </OpenButton>

      <hr/>
      <h2>Checkbox</h2>

      <Checkbox 
        name='checkbox_unchecked' 
        label='Checkbox that starts unchecked.' 
      />
      <Checkbox 
        name='checkbox_checked' 
        label='Checkbox that starts checked.' 
        defaultValue={true}
      />
      <Checkbox 
        name='checkbox_indeterminate' 
        label='Checkbox that starts indeterminate.' 
        indeterminate 
      />
      <Checkbox 
        name='checkbox_long' 
        label='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque dictum vitae est non tristique. Nulla interdum dignissim dictum. Nam efficitur eros at mi suscipit, quis porta nunc vehicula. Duis tincidunt, metus a consequat lacinia, ex eros pellentesque risus, ut commodo tortor massa eget nulla. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed ultrices varius sodales. Cras a risus malesuada, egestas velit eget, commodo dolor. Maecenas magna dolor, tincidunt id luctus eget, placerat eu nibh. Pellentesque mattis dapibus orci, eget luctus arcu convallis eget. Aliquam nec elit varius, porta nisl ac, bibendum nibh. Cras in efficitur eros. '  
      />

      <OpenButton onClick={() => openStack(demoContentMap.guide_checkbox)}>
        View Checkbox Guide
      </OpenButton>

      <hr/>
      <h2>Radio Group</h2>

      <RadioGroup 
        name='radiogroup' 
        options={[
          { label: 'Radio option 1', value: 'option_1' }, 
          { label: 'Default option', value: 'option_2' }, 
          { label: 'Radio option 3', value: 'option_3' }
        ]} 
        defaultValue={'option_2'}
        minWidth='150px'
      />

      <OpenButton onClick={() => openStack(demoContentMap.guide_radiogroup)}>
        View RadioGroup Guide
      </OpenButton>

      <hr/>
      <h2>Toggles</h2>

      <Grid min='200px'>
        <Toggle name='toggle' label='Toggle option 1'/> 
        <Toggle name='toggle_2' label='Toggle option 2'/> 
        <Toggle name='toggle_3' label='Toggle default on' defaultValue={true} /> 
      </Grid>

      <OpenButton onClick={() => openStack(demoContentMap.guide_toggle)}>
        View Toggle Guide
      </OpenButton>

      <hr/>
      <h2>Select box</h2>

      <Grid min='300px'>

        <SelectBox
          name={'selectbox'} 
          label={'Select a fruit'}
          options={[
            {label: '< Clear >', value: ''},
            {group: {
              label: 'Fruits',
              options: [
                {label: 'Banana', value: 'banana'},
                {label: 'Apple', value: 'apple'},
                {label: 'Orange', value: 'orange'},
              ]
            }}
          ]}
        />

        <SelectBox
          name={'selectbox_2'} 
          label={'Preselected fruit'}
          options={[
            {label: '< Clear >', value: ''},
            {group: {
              label: 'Fruits',
              options: [
                {label: 'Banana', value: 'banana'},
                {label: 'Apple', value: 'apple'},
                {label: 'Orange', value: 'orange'},
              ]
            }}
          ]}
          defaultValue='orange'
        />

      </Grid>

      <OpenButton onClick={() => openStack(demoContentMap.guide_selectbox)}>
        View SelectBox Guide
      </OpenButton>

      <hr/>
      <h2>Google ReCaptcha v2</h2>

      <ReCaptchaV2 
        name='recaptcha' 
        apiKey='6LcJAfcZAAAAAC2RiJQ3Zrx2WDZVsUoAuOQ-eUxm'
        required 
      />

      <OpenButton onClick={() => openStack(demoContentMap.guide_recaptchav2)}>
        View ReCaptchaV2 Guide
      </OpenButton>

    </Form>
  );
};

export default memo(DemoForm);