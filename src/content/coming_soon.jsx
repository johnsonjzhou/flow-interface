/**
 * Fills a block level element with a 'Coming Soon' message
 * @author  Johnson Zhou  <johnson@simplyuseful.io>
 * 
 * @param  {function}  props.openStack
 * @param  {function}  props.setQuickActions
 */

import React, { memo, useState, useEffect } from 'react';
import { comingSoon } from '../styles/coming_soon';
import { 
  RiExternalLinkLine as IconOpenNew, 
} from 'react-icons/ri';
import {
  IoMdHourglass as IconLoading, 
} from 'react-icons/io';
import {
  FiEdit3 as IconChanged, 
} from 'react-icons/fi';
import {
  GoBeaker as LogoBeaker, 
  GoMegaphone as LogoMegaphone, 
  GoTools as LogoTools, 
  GoTelescope as LogoTelescope, 
  GoRocket as LogoRocket, 
} from 'react-icons/go';

const ComingSoon = memo((props) => {
  const {
    openStack, 
    setQuickActions, 
    setChanged, 
    setLoading, 
  } = props;

  const randomLogo = () => {
    const icons = [
      <LogoBeaker/>, 
      <LogoMegaphone/>, 
      <LogoTools/>, 
      <LogoTelescope/>, 
      <LogoRocket/>, 
    ];
    return icons[Math.floor(Math.random() * icons.length)];
  }

  const [ logo, setLogo ] = useState(randomLogo());

  useEffect(() => {
    setQuickActions && setQuickActions([
      {icon: <IconOpenNew/>, action: ()=> openStack(<ComingSoon/>)},
      {icon: <IconChanged/>, action: ()=> setChanged(true)}, 
      {icon: <IconLoading/>, action: ()=> setLoading(true)}
    ]);
  })

  return (
    <comingSoon.wrapper>
      <comingSoon.logo>
        { logo }
      </comingSoon.logo>
      <comingSoon.message>
        This feature is coming soon.
      </comingSoon.message>
    </comingSoon.wrapper>
  );
});

export default ComingSoon;