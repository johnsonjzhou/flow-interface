/**
 * An object describing all the content available to be rendered
 * @author  Johnson Zhou  <johnson@simplyuseful.io>
 * 
 * @export  defaultContentMap
 * 
 * @see  default_menu.js
 */
import React from 'react';
import ComingSoon from './coming_soon';

export const defaultContentMap = {
  soon: <ComingSoon/>, 
};