/**
 * An object describing all the content available to be rendered, 
 * for the demo website 
 * @author  Johnson Zhou  <johnson@simplyuseful.io>
 * 
 * @export  demoContentMap
 * 
 * @see  demo_menu.js
 */
import React from 'react';
import ComingSoon from './coming_soon';
import FormData from './demo_form';
import Settings from './settings'
import Markdown from '../components/markdown';

// guide content 
const form_md = 'https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/components/form.md';
const textfield_md = 'https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/components/textfield.md';
const selectbox_md = 'https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/components/selectbox.md';
const inputfilepicker_md = 'https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/components/inputfilepicker.md';
const paymentcard_md = 'https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/components/paymentcard.md';
const codeblock_md = 'https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/components/codeblock.md';
const checkbox_md = 'https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/components/checkbox.md';
const radiogroup_md = 'https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/components/radiogroup.md';
const toggle_md = 'https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/components/toggle.md';
const recaptchav2_md = 'https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/components/recaptchav2.md';
const readme_md = 'https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/readme.md';
const content_md = 'https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/content_guide.md';
const sw_md = 'https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/sw_guide.md';
const events_md = 'https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/interface_events.md';
const buttons_md = 'https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/components/buttons.md';
const conditionalwrapper_md = 'https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/components/conditionalwrapper.md';
const markdown_md = 'https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/components/markdown.md';
const async_load_md = 'https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/components/async_load.md';

export const demoContentMap = {
  readme: <Markdown source={readme_md}/>, 
  formdata: <FormData/>, 
  settings: <Settings/>, 
  soon: <ComingSoon/>, 
  guide_form: <Markdown source={form_md}/>, 
  guide_textfield: <Markdown source={textfield_md}/>, 
  guide_selectbox: <Markdown source={selectbox_md}/>, 
  guide_filepicker: <Markdown source={inputfilepicker_md}/>, 
  guide_paymentcard: <Markdown source={paymentcard_md}/>, 
  guide_checkbox: <Markdown source={checkbox_md}/>, 
  guide_radiogroup: <Markdown source={radiogroup_md}/>, 
  guide_toggle: <Markdown source={toggle_md}/>, 
  guide_codeblock: <Markdown source={codeblock_md}/>, 
  guide_recaptchav2: <Markdown source={recaptchav2_md}/>, 
  guide_content: <Markdown source={content_md}/>, 
  guide_sw: <Markdown source={sw_md}/>, 
  guide_events: <Markdown source={events_md}/>, 
  guide_buttons: <Markdown source={buttons_md}/>, 
  guide_conditionalwrapper: <Markdown source={conditionalwrapper_md}/>, 
  guide_markdown: <Markdown source={markdown_md}/>, 
  guide_asyncload: <Markdown source={async_load_md}/>, 
};