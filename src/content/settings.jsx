/**
 * Settings page for Flow Interface 
 * @author  Johnson Zhou  <johnson@simplyuseful.io>
 */

import React, { memo, useState, useEffect } from 'react';
import RadioGroup from '../components/radiogroup';

const Settings = memo((props) => {
  const {
    openStack, 
    setQuickActions, 
    setChanged, 
    setLoading, 
  } = props;

  const [ defaultColorScheme, setDefaultColorScheme ] = useState();
  const [ defaultStyle, setDefaultStyle ] = useState();

  const onChangeColorScheme = ({ value }) => {
    const setScheme = new CustomEvent('flow-set-color-scheme', {
      detail: { scheme: value }
    });
    window.dispatchEvent(setScheme);
  };

  const onChangeStyle = ({ value }) => {
    const setStyle = new CustomEvent('flow-set-interface-style', {
      detail: { style: value }
    });
    window.dispatchEvent(setStyle);
  };

  useEffect(() => {
    const getScheme = new CustomEvent('flow-get-preferred-color-scheme', {
      detail: { callback: setDefaultColorScheme }
    });

    const getStyle = new CustomEvent('flow-get-interface-style', {
      detail: { callback: setDefaultStyle }
    });

    window.dispatchEvent(getScheme);
    window.dispatchEvent(getStyle);

  }, []);

  return (
    <>
      <h1>Settings.</h1>

      <h2>Color Scheme</h2>

      <RadioGroup
        name='color-scheme' 
        options={[
          { label: 'Auto', value: 'auto' },
          { label: 'Light', value: 'light' },
          { label: 'Dark', value: 'dark' }
        ]}
        minWidth='200px'
        callback={onChangeColorScheme}
        defaultValue={defaultColorScheme}
      />

      <hr/>
      <h2>Interface Style</h2>

      <RadioGroup
        name='interface-style' 
        options={[
          { label: 'Mono (Default)', value: 'mono' },
          { label: 'Stacks', value: 'stacks' },
          { label: 'Stacks-Perspective', value: 'stacks-perspective' },
          { label: 'Stacks-Performance', value: 'stacks-performance' },
        ]}
        minWidth='200px'
        callback={onChangeStyle}
        defaultValue={defaultStyle}
      />
    </>
  );
});

export default Settings;