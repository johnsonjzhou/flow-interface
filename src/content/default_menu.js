/**
 * Default menu options in case one isn't defined
 * @author  Johnson Zhou  <johnson@simplyuseful.io>
 * 
 * @export  defaultMenuOptions
 * 
 * @see  default_content_map.jsx
 */

const defaultMenuOptions = [
  {
    label: 'Hidden', 
    content: 'firstrun',
    hide: true,
  },
  {
    label: 'First option', 
    content: 'soon'
  },
  {
    label: 'Second option', 
    content: 'soon'
  },
  {
    label: 'Third option', 
    content: 'soon',
  },
  {
    label: 'Next group',
    setGroup: 'newGroup',
    background: 'linear-gradient(120deg, #FF7139 0%, #FF505F 100%)'
  },
  {
    label: 'Fourth option', 
    content: 'fourth',
    group: 'newGroup',
  },
  {
    label: 'Fifth option', 
    content: 'fifth',
    group: 'newGroup',
  },
];

export {
  defaultMenuOptions
};