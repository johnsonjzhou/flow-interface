/**
 * Default menu options in case one isn't defined
 * @author  Johnson Zhou  <johnson@simplyuseful.io>
 * 
 * @export  demoMenuOptions
 * 
 * @see  demo_content_map.jsx
 */

const demoMenuOptions = [
  {
    label: 'Hidden', 
    content: 'firstrun',
    hide: true,
  },
  {
    label: 'Readme', 
    content: 'readme'
  },
  {
    label: 'Demonstration',
    setGroup: 'demo',
    background: 'linear-gradient(120deg, #ffa3b2 0%, #ffd95c 100%)'
  },
  {
    label: 'Form and Data', 
    content: 'formdata', 
    group: 'demo'
  },
  {
    label: 'Guides: General',
    setGroup: 'guide_general',
    background: 'linear-gradient(120deg, #f5855b 0%, #9966e8 100%)'
  },
  {
    label: 'Content', 
    content: 'guide_content', 
    group: 'guide_general'
  },
  {
    label: 'Service Worker', 
    content: 'guide_sw', 
    group: 'guide_general'
  },
  {
    label: 'AsyncLoad', 
    content: 'guide_asyncload', 
    group: 'guide_general'
  },
  {
    label: 'Events', 
    content: 'guide_events', 
    group: 'guide_general'
  },
  {
    label: 'Buttons', 
    content: 'guide_buttons', 
    group: 'guide_general'
  },
  {
    label: 'Markdown', 
    content: 'guide_markdown', 
    group: 'guide_general'
  },
  {
    label: 'Codeblock', 
    content: 'guide_codeblock', 
    group: 'guide_general'
  },
  {
    label: 'ConditionalWrapper', 
    content: 'guide_conditionalwrapper', 
    group: 'guide_general'
  },
  {
    label: 'Guides: Form & Data',
    setGroup: 'guide_formdata',
    background: 'linear-gradient(120deg, #f5855b 0%, #9966e8 100%)'
  },
  {
    label: 'Form', 
    content: 'guide_form', 
    group: 'guide_formdata'
  },
  {
    label: 'TextField', 
    content: 'guide_textfield', 
    group: 'guide_formdata'
  },
  {
    label: 'SelectBox', 
    content: 'guide_selectbox', 
    group: 'guide_formdata'
  },
  {
    label: 'InputFilePicker', 
    content: 'guide_filepicker', 
    group: 'guide_formdata'
  },
  {
    label: 'PaymentCard', 
    content: 'guide_paymentcard', 
    group: 'guide_formdata'
  },
  {
    label: 'Checkbox', 
    content: 'guide_checkbox', 
    group: 'guide_formdata'
  },
  {
    label: 'RadioGroup', 
    content: 'guide_radiogroup', 
    group: 'guide_formdata'
  },
  {
    label: 'Toggle', 
    content: 'guide_toggle', 
    group: 'guide_formdata'
  },
  {
    label: 'ReCaptchaV2', 
    content: 'guide_recaptchav2', 
    group: 'guide_formdata'
  },
  {
    label: 'Settings', 
    content: 'settings',
  },
];

export {
  demoMenuOptions
};