/**
 * A quick way to render flow-interface on the <main> tag with prop options
 * @author  Johnson Zhou <johnson@simplyuseful.io>  
 */
import React from 'react';
import ReactDOM from 'react-dom';

import FlowInterface, { ErrorBoundary } from 'flow-interface';

/**
 * Initiate or update FlowInterface on <main> with given menu and contents 
 * @param  {object}  object 
 * @param  {array}  object.menu 
 * @param  {object}  object.contents 
 * @param  {styled.css}  object.theme 
 * @param  {object}  object.logo 
 * @param  {function}  object.onLoad 
 * @param  {React.Component}  object.append 
 * 
 * @see flow-interface 
 */
const renderInterface = ({ menu, contents, theme, logo, onLoad, append }) => {
  ReactDOM.render(
    <ErrorBoundary message='Web App failed to load'>
      <FlowInterface
        serviceworker={{
          scriptURL: '/sw.js', 
          options: { scope: '/' }
        }}
        logo={logo} 
        menu={menu} 
        contents={contents} 
        theme={theme} 
        onLoad={onLoad} 
        appendComponent={append} 
      />
    </ErrorBoundary>
  , document.getElementsByTagName("main")[0]);
};

export { 
  renderInterface, 
};