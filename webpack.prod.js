'use strict';
const path = require('path');
const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');

/**
 * ! uglifyjs-webpack-plugin has dependency problems
 * ! switching to terser-webpack-plugin
 * todo  test functionality
 */
// const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = merge(common, {
  output: {
    path: path.resolve('./public/'),
    filename: '[name].[contenthash].js'
  },
  mode: 'production',
  // devtool: '',
  optimization: {
    /* minimizer: [
      new UglifyJsPlugin({
        uglifyOptions: {
          output: {
            comments: false,
          },
        },
      }),
    ], */
    minimize: true,
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          output: {
            comments: false,
          },
        },
        extractComments: false,
      }),
    ],
    splitChunks: {
      chunks: 'all',
    }
  }
});