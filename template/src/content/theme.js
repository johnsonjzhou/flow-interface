import { css } from 'styled-components';

const theme = css`
  #flow-interface {
    --theme-color-accent: #00a651;
    --theme-color-accent-dark: #008b44;
    --theme-color-blue: #26489c;
    --theme-color-cyan: #63c7f2;
    --theme-color-teal: #4cbc9d;
    --theme-color-green: #7ec529;
    --theme-color-yellow: #fcdd04;
    --theme-color-red: #ec124e;
    --theme-color-orange: #f39411;
    --theme-color-pink: #edaddb;
    --theme-color-purple: #d848a9;
    --theme-color-midnight: #1a1a1a;
    --theme-color-darkgrey: #666666;
    --theme-color-grey: #a6a6a6;
    --theme-color-lightgrey: #e6e6e6;
    --theme-color-white: #ffffff;
    --theme-color-black: #000000;

    /* interface */
    --theme-interface-background: 
      linear-gradient(120deg, #00a651 0%, #7ec529 100%); 
    --menu-group-background:
      linear-gradient(120deg, #26489c 0%, #63c7f2 100%); 

      &.dark-theme {
        /* interface */
      --theme-interface-background: 
        linear-gradient(120deg, #008b44 0%, #4b9200 100%); 
      --menu-group-background:
        linear-gradient(120deg, #0D2F83 0%, #3094BF 100%); 
      }
  }
`;

export {
  theme as default
};