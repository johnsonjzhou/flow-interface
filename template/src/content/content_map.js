import React from 'react';

// settings
import { Settings } from 'flow-interface';

const contents = {

  // settings 
  settings_interface: <Settings/>, 
};

export default contents;