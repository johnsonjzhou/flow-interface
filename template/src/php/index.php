<?php  
/**
 * Base site template page 
 * @author  Johnson Zhou  <johnson@simplyuseful.io>  
 */

// site meta info
$metaTitle = ''; 
$metaDescription = ''; 
$metaUrl = '';

// colors
$themeColor = '#00A651';
$dotOneColor = '#63c7f2';
$dotTwoColor = '#fcdd04';
$dotThreeColor = '#edaddb';

//todo  remember to set icons
?>
<!doctype html>
<html lang='en'>
  <head>

    <base href="/"/>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="/assets/icons/icon-1024.png" type="image/png" />

    <!-- About this page -->
    <title><?php echo $metaTitle; ?></title>
    <meta name="description" content="<?php echo $metaDescription; ?>" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php echo $metaUrl; ?>" />
    <meta property="og:site_name" content="<?php echo $metaTitle; ?>" />
    <meta property="og:title" content="<?php echo $metaTitle; ?>" />
    <meta property="og:description" content="<?php echo $metaDescription; ?>" />
    <meta property="og:image" content="" />

    <!-- Twitter support -->
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="<?php echo $metaTitle; ?>" />
    <meta name="twitter:description" content="<?php echo $metaDescription; ?>" />

    <!-- Apple web app support -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="<?php echo $metaTitle; ?>">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no">
    <link rel="apple-touch-icon" href="/assets/icons/icon-1024.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/assets/icons/icon-120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/assets/icons/icon-152.png">
    <link rel="apple-touch-icon" sizes="167x167" href="/assets/icons/icon-167.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/icons/icon-180.png">
    <link rel="apple-touch-icon" sizes="1024x1024" href="/assets/icons/icon-1024.png">
    
    <!-- Android web app support -->
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="theme-color" content="<?php echo $themeColor; ?>">

    <!-- PWA support -->
    <link rel="manifest" href="/app.webmanifest">

  </head>

  <body>
    <main>
      <!-- Loading indicators -->
      <style type='text/css'>
        body {
          background-color: <?php echo $themeColor; ?>; 
          height: 100vh; 
          width: 100vw;
          margin: 0;
        }
        div.wrapper {
          font-size: 16px;
          display: block;
          position: absolute;
          left: 50%;
          top: 50%;
          transform: translate3d(-50%, -50%, 0);
          height: 1.6em;
          width: 100%;
          overflow: visible;
        }
        div.dot {
          position: absolute;
          width: 1em;
          height: 1em;
          border-radius: 1em;
          background-color: #FFFFFF;
        }
        div.dots {
          animation: two 2s infinite;
          display: block;
          position: absolute;
          left: 50%;
          top: 50%;
          transform: translate3d(-50%, -50%, 0);
          width: 1em;
          height: 1em;
          border-radius: 1em;
          background-color: #FFFFFF;
        }
        div.dots:before {
          content: '';
          left: -1.5em;
          animation: one 2s infinite;
          display: block;
          position: absolute;
          width: 1em;
          height: 1em;
          border-radius: 1em;
          background-color: #FFFFFF;
        }
        div.dots:after {
          content: '';
          right: -1.5em;
          animation: three 2s infinite;
          display: block;
          position: absolute;
          width: 1em;
          height: 1em;
          border-radius: 1em;
          background-color: #FFFFFF;
        }
        @keyframes one {
          0%    {background-color: #F5F5F6;}
          10%   {background-color: transparent;}
          20%   {background-color: <?php echo $dotOneColor; ?>;}
          50%   {background-color: <?php echo $dotOneColor; ?>;}
          60%   {background-color: transparent;}
          70%   {background-color: #F5F5F6;}
          100%  {background-color: #F5F5F6;}
        }
        @keyframes two {
          0%    {background-color: #F5F5F6;}
          3%    {background-color: #F5F5F6;}
          13%   {background-color: transparent;}
          23%   {background-color: <?php echo $dotTwoColor; ?>;}
          53%   {background-color: <?php echo $dotTwoColor; ?>;}
          63%   {background-color: transparent;}
          73%   {background-color: #F5F5F6;}
          100%  {background-color: #F5F5F6;}
        }
        @keyframes three {
          0%    {background-color: #F5F5F6;}
          9%    {background-color: #F5F5F6;}
          19%   {background-color: transparent;}
          29%   {background-color: <?php echo $dotThreeColor; ?>;}
          59%   {background-color: <?php echo $dotThreeColor; ?>;}
          69%   {background-color: transparent;}
          79%   {background-color: #F5F5F6;}
          100%  {background-color: #F5F5F6;}
        }
      </style>
      <div class='wrapper'>
        <div class='dots'></div>
      </div>
    </main>
  </body>

  <footer></footer>

</html>