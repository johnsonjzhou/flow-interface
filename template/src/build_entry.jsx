import { renderInterface } from 'flow-interface';
//todo import menu from './content/menu';
//todo import contents from './content/content_map';
//todo import theme from './content/theme';
//todo import logo_image from ' ';

/**
 * Show identifier in the console
 */
window.console.log(
  '--------------------------------------------------- \n'+
  'App Name \n'+
  `Version ${___WEBPACK_VERSION___} ${process.env.NODE_ENV} \n`+
  'Webmaster:  \n'+
  'Powered by Project Flow (https://bitbucket.org/johnsonjzhou/workspace/projects/FLOW) \n'+
  '---------------------------------------------------'
);

renderInterface({ menu, contents, theme, logo: {
  name: 'App Name', 
  src: logo_image
} });
