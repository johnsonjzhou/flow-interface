# SelectBox component  

- A styled `<select>` box.  
- Does not actually use the `<select>` tag due to challenges with styling.  
- Can be used with the `<Form>` component.  

---  
## Usage  

````javascript
import { SelectBox } from 'flow-interface';
````

---  
## Required Props  

Prop | Type | Description  
-- | -- | --  
`name` | String | Field name, similar to the html `name` attribute.  
`options` | Array | Array of `OptionObjects` and/or `GroupObjects`, to render.  

---  
## Optional Props  

Prop | Type | Description  
-- | -- | --  
`label` | String | Label description.  
`defaultValue` | String | The default value to apply on load. Match this to `props.options`.  
`callback` | Function | Invoked when field value changes, applying the `DataObject`.  

---  
## Custom Objects  

**OptionObject**  

Describes an option and its associated value.  

Property | Type | Description  
-- | -- | --  
`label` | String | Descriptive label to display.   
`value` | String | Value of the option.  

**GroupObject**  

Describes an option group which contains a nested array of `OptionObjects`.  

Property | Type | Description  
-- | -- | --  
`label` | String | Descriptive label to display.  
`options` | Array | Nested array of `OptionObjects`.  

**DataObject**  

Property | Type  
-- | --  
`name` | String  
`valid` | Bool  
`value` | String  
