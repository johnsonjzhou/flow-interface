# Flow Interface

A minimalist user interface that promotes focus whilst maintaining fluid 
workflows. [Live Example](https://flow-interface.simplyuseful.io)  

![Flow Interface 1.2.0](https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/flow-interface-1-2-0-mono.gif)  
*"Mono" interface style (default)*  

![Flow Interface 1.0.0](https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/flow-interface-1-0-0.gif)  
*"Stacks" interface style*  

---
## Version  
1.2.0  (
  [changelog](https://bitbucket.org/johnsonjzhou/flow-interface/src/master/doc/changelog.md) | 
  [issues and todo](https://bitbucket.org/johnsonjzhou/flow-interface/src/master/doc/issues.md) 
)  

---  
## Features  
- [Responsive styling](##responsive)  
- Dark and Light [color scheme](##color-scheme) support  
- Customisable theme  
- Full PWA support using Servie Workers for offline caching  
- Plug and play your content!  

---  
## Install  
````bash
npm install https://bitbucket.org/johnsonjzhou/flow-interface.git
````

---  
## Browser support

Browser features required:  
- Local Storage  
- Indexed DB  
- Web Workers  
- Service Workers  
- Fetch  
- Promise  

---
## Responsive  

![Responsive example](https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/flow-interface-responsive.png)  

---
## Color Scheme  

Supports both light and dark color schemes that switches automatically based 
on global OS setting or can be manually set as required.  

Local Storage Key | Value  
-- | --  
`prefers-color-scheme` | `auto` (default), `light`, `dark`  

![Color Scheme example mono](https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/flow-interface-color-scheme-mono.png)  
*"Mono" interface style (default)*  

![Color Scheme example](https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/flow-interface-color-scheme.png)  
*"Stacks" interface style*  

---
## Interface style and performance Modes  

Local Storage Key | Value  
-- | --  
`flow-interface-style` | `mono` (default), `stacks`, `stacks-performance`, `stacks-perspective`   

**Mono**  
The default interface style, inspired from the popular TV series *3%*.  
![Mono Style](https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/flow-interface-1-2-0-mono.gif)  

**Stacks**  
Each stack is represented by it's own window, with all transitions enabled.  
![Flat mode](https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/flow-interface-perf-flat.gif)  

**Stacks Performance**  
Stacks for older devices with weaker GPUs, all transitions are disabled.  
![Performance mode](https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/flow-interface-perf-performance.gif)  

**Stacks Perspective**  
Will change the stack perspective based on mouse position or device orientation. 
This does not work well with Firefox and as such can only be manually applied.  
![Perspective mode](https://bitbucket.org/johnsonjzhou/flow-interface/raw/master/doc/flow-interface-perf-perspective.gif)  

---  
## Syntax  

> **Example:** [build_entry.jsx](https://bitbucket.org/johnsonjzhou/flow-interface/src/master/src/build_entry.jsx)  

````javascript
import FlowInterface { ErrorBoundary } from 'flow-interface'; 
// ErrorBoundary is optional

// mount on <main>
ReactDOM.render(
  <ErrorBoundary message='Flow Interface failed to load'>
    <FlowInterface 
      theme={themeOverride} 
      menu={menuOverride} 
      content={contentOverride} 
      logo={logoOverride} 
    />
  </ErrorBoundary>
, document.getElementsByTagName("main")[0]);
````

--- 
## Props  

- [props.theme](#props.theme)  
- [props.menu](#props.menu)  
- [props.content](#props.content)  
- [props.logo](#props.logo)  
- [props.serviceworker](#props.serviceworker)  

--- 
## Props.theme `object`  

Theme overrides declared using the `css` helper function of `styled-components`, 
and is merged within `global_styles.jsx`. Refer to 
[`global_styles.jsx`](https://bitbucket.org/johnsonjzhou/flow-interface/src/master/src/styles/global_styles.jsx) for settable properties. 

````javascript
import { css } from 'styled-components';

const theme = css`
  :root {
    --theme-color-accent: #00a651;
    --theme-color-accent-dark: #008b44;
    --theme-color-blue: #26489c;
    --theme-color-cyan: #63c7f2;
    --theme-color-teal: #4cbc9d;
    --theme-color-green: #7ec529;
    --theme-color-yellow: #fcdd04;
    --theme-color-red: #ec124e;
    --theme-color-orange: #f39411;
    --theme-color-pink: #edaddb;
    --theme-color-purple: #d848a9;
    --theme-color-midnight: #1a1a1a;
    --theme-color-darkgrey: #666666;
    --theme-color-grey: #a6a6a6;
    --theme-color-lightgrey: #e6e6e6;
    --theme-color-white: #ffffff;
    --theme-color-black: #000000;

    --theme-color-background: #f5f5f6;
    --theme-color-background-dark: #263238;
    --theme-color-foreground: #121212;
    --theme-color-foreground-dark: #ffffff;
  }
`;
````

--- 
## Props.menu `array`  

An `array` of `objects` describing the options available on the main menu. 
There are two types of menu options:  

- *Standard*, an option that opens content, or  
- *Group*, an option that changes the menu group.  

> **Example:** [default_menu.js](https://bitbucket.org/johnsonjzhou/flow-interface/src/master/src/content/default_menu.js)  

### Standard menu object  

Property | Type | Description  
-- | -- | --  
`label` | `string` | *required* The text that will be rendered in the menu.  
`content` | `string` | *required* The content **key** as mapped by the content object. See [props.content](##props.content).  
`hidden` | `bool` | *optional* If `true`, the option will not be rendered.  
`group` | `string` | *optional* The menu group this option falls under.  

### Group menu object  

Property | Type | Description  
-- | -- | --  
`label` | `string` | *required* The text that will be rendered in the menu.  
`setGroup` | `string` | *required* Set the menu group.  
`background` | `string` | Any property that would fulfill the `background` css property.  

--- 
## Props.content `object`  

An `object` that maps content components as defined by the object property 
as the **key**.  

> **Example:** [default_content_map.jsx](https://bitbucket.org/johnsonjzhou/flow-interface/src/master/src/content/default_content_map.jsx)  

````javascript
// content
const content = {
  products: <Products/>,
  customers: <Customers/>
  ...
}

// menu
const menu = [
  {
    label: 'View Products', 
    content: 'products', 
  },
  {
    label: 'View Customers', 
    content: 'customers',
  }
  ...
]
````

--- 
## Props.logo `object`  

An `object` that describes the logo to display on the menu background.  

Property | Type | Description  
-- | -- | --  
`name` | `string` | *required* App name or `alt` tag if src not specified.  
`src` | `string` | *required* `src` of the logo image.  

---  
## Props.serviceworker `object`  

An `object` that encapsulates parameters for `ServiceWorkerContainer.register`. 
If this is not specified, then a Service Worker will not be used.  

> **Important**  
> Flow Interface is designed around 
> [`flow-serviceworker`](https://bitbucket.org/johnsonjzhou/flow-serviceworker/). 
> Using your custom Service Worker script may yield unpredictable results.  

Property | Type | Description  
-- | -- | --  
`scriptURL` | `string` | The URL of the service worker script.  
`options` | `object` | An object containing registration options.  
`options.scope` | `string` | URL that defines a service worker's registration scope.  

---  
## Content  

See [Content Guide](https://bitbucket.org/johnsonjzhou/flow-interface/src/master/doc/content_guide.md).  

---  
## Service Worker  

See [Service Worker Guide](https://bitbucket.org/johnsonjzhou/flow-interface/src/master/doc/sw_guide.md).  

---  
## Events  
See [Flow Interface Events Guide](https://bitbucket.org/johnsonjzhou/flow-interface/src/master/doc/interface_events.md).  

---  
## Component Exports  
See [Component Exports Guide List](https://bitbucket.org/johnsonjzhou/flow-interface/src/master/doc/component_exports.md).  

---
## License  
MIT.  
Johnson Zhou [johnson@simplyuseful.io](mailto://johnson@simplyuseful.io).

---
## Credits and inspirations  

- [The League of Moveable Type](https://www.theleagueofmoveabletype.com/)  
- [Material Design](https://material.io/)  
- [Mozilla Dot Design](https://mozilla.design/)  
