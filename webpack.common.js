'use strict';
const webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

// @see https://developers.google.com/web/tools/workbox/modules/workbox-webpack-plugin
const { InjectManifest } = require('workbox-webpack-plugin');

// @see https://github.com/jantimon/html-webpack-plugin
const HtmlWebpackPlugin = require('html-webpack-plugin');

// @see https://github.com/GoogleChromeLabs/preload-webpack-plugin
// need preload-webpack-plugin@next if using with HtmlWebpackPlugin v4+
const PreloadWebpackPlugin = require('preload-webpack-plugin');

// remove these paths at the start of each build
const pathsToClean = [
  '.*',
  '*.js',
  '*.php',
  'assets/fonts',
  'assets/img',
  'assets/icons',
];

// these files are copied directly from src using copy-webpack-plugin
// mainly used for pwa support
const copyFiles = [
  {
    from: 'src/apache/.htaccess', 
    to: '.htaccess',
    toType: 'file'
  }, 
  {
    from: 'src/php/403.php', 
    to: '403.php'
  }, 
  {
    from: 'src/php/404.php', 
    to: '404.php'
  }, 
  {
    from: 'src/php/500.php', 
    to: '500.php'
  }, 
  {
    from: 'src/pwa/app.webmanifest', 
    to: 'app.webmanifest'
  }, 
  {
    from: 'src/icons/icon*.png',
    to: 'assets/icons/[name].[ext]'
  }
];

const npmConfig = require('./package.json');

// append a random six character identifier to the version 
// so that each build will result in a different service worker version 
// this is needed so that an update is triggered on the client 
const serviceWorkerVersion = (max = 6) => {
  let key = '';
  let version = npmConfig.version;
  var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  for (var i = 0; i < max; i++)
  key += chars.charAt(Math.floor(Math.random() * chars.length));
  return version.concat('.', key);
};

// manually defined list of assets to pre-cache
const firstLoadAssets = [
  '/', 
];

// url paths containing these strings will be handled by cache-then-network policy
const staticFileTypes = [
  'app.webmanifest',
  '/assets',
];

// do not inject these files with these names
const avoidInjection = [
  'sw.js', 
  '.htaccess', 
  '403.php', '404.php', '500.php', 'index.php' 
];

const bugsnagSettings = require('./bugsnag.json');

module.exports = {
  entry: {
    flow_interface: './src/build_entry.jsx',
  },
  resolve: {
    alias: {},
    extensions: ['.js', '.jsx']
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules\/(?!flow\-).*/,
        use: "babel-loader"
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules\/(?!flow\-).*/,
        use: "babel-loader"
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          { 
            loader: 'file-loader',
            options: {
              outputPath: 'assets/fonts/',
              publicPath: 'assets/fonts/'
            }
          }
        ]
      },
      {
        test: /\.(jpg|jpeg|png|svg)$/,
        use: [
          { 
            loader: 'file-loader',
            options: {
              outputPath: 'assets/img/',
              publicPath: 'assets/img/'
            }
          }
        ]
      },
      {
        test: /\.md$/,
        use: [
          { 
            loader: 'file-loader',
            options: {
              outputPath: 'assets/markdown/',
              publicPath: 'assets/markdown/'
            }
          }
        ]
      },
    ]
  },
  plugins: [
    // delete existing packed assets
    new CleanWebpackPlugin({
      cleanOnceBeforeBuildPatterns: pathsToClean
    }),

    // we are dynamically generating the package file
    // [name].[contenthash].js
    // we are using this plugin to generate a list of <script> tags
    // then this is included within the php render process
    new HtmlWebpackPlugin({
      filename: 'index.php',
      template: './src/php/index.php'
    }),

    // static copy of files
    new CopyPlugin({
      patterns: copyFiles
    }), 

    // to handle precache within service worker
    // using workbox-webpack-plugin.InjectManifest
    // to inject a list of built assets to ServiceWorker.js
    // the standard manifest includes array of {revision, url}
    // we are transforming this to just array of url (string)
    // the asset path is also relative to the sw.js
    // so we need to remove the 'assets/' prefix
    // @see https://developers.google.com/web/tools/workbox/reference-docs/latest/module-workbox-webpack-plugin.InjectManifest#InjectManifest
    new InjectManifest({
      swSrc: './node_modules/flow-serviceworker/src/sw.js',
      swDest: './sw.js',
      injectionPoint: '___WEBPACK_INJECT_MANIFEST___',
      maximumFileSizeToCacheInBytes: 10000000,
      manifestTransforms: [
        async (manifestEntries) => {
          const manifest = manifestEntries
          .map(entry => {
            // avoid injecting these files 
            if (avoidInjection.includes(entry.url)) return;

            // add / to url if doesn't exist 
            if (!entry.url.startsWith('/')) 
            entry.url = '/' + entry.url;
            
            return entry.url;
          })
          .filter(url => url);
          return {manifest, warnings: []};
        },
      ]
    }),

    // preloading built assets to improve performance
    new PreloadWebpackPlugin({
      rel: 'preload',
      include: 'allAssets',
      fileWhitelist: [
        /\.woff/, /\.woff2/, /\.eot/, /\.ttf/, /\.otf/, 
        // /\.jpg/, /\.jpeg/, /\.png/, /\.svg/, 
      ],
      as(entry) {
        if (/\.(woff|woff2|eot|ttf|otf)$/.test(entry)) return 'font';
        // if (/\.(jpg|jpeg|png|svg)$/.test(entry)) return 'image';
        return 'script';
      }
    }),

    new webpack.DefinePlugin({
      ___WEBPACK_VERSION___: JSON.stringify(npmConfig.version),
      ___WEBPACK_BUGSNAG___: JSON.stringify(bugsnagSettings.apiKey),
      ___WEBPACK_SW_VERSION___: JSON.stringify(serviceWorkerVersion()),
      ___WEBPACK_SW_ASSETS___: JSON.stringify(firstLoadAssets),
      ___WEBPACK_SW_STATIC_FILE_TYPES___: JSON.stringify(staticFileTypes),
    })
  ],
  
};